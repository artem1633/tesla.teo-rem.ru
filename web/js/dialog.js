/**
 * Создание редактирование и удаление
 * записей справочников
 */

function getDialog(id){
    if ($('#' + id).length == 0){
        $('<div id="' + id + '" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
            '<h4 class="modal-title">Заголовок</h4></div><div class="modal-body"></div><div class="modal-footer">' +
            '<button type="button" class="btn btn-save btn-primary" data-loading-text="Сохранение ...">Сохранить</button>' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button></div>' +
            '</div></div></div>').appendTo('body');
    }
    return $('#' + id);
}

function dialog(url, after){
    var $dialog = getDialog('crud-dialog');
    $.getJSON(url, function(data){
        if (data.action == 'render'){
            $dialog.find('h4.modal-title').html(data.title);
            $dialog.find('div.modal-body').html(data.html);
            $dialog.find('.btn-save').unbind('click');
            $dialog.find('.btn-save').click(function(){
                $(this).button('loading');
                $.post($dialog.find('form').attr('action'), $dialog.find('form').serialize(), function(data){
                    $dialog.find('.btn-save').button('reset');
                    if (data.action == 'close'){
                      //  $.notify(data.flash.options, data.flash.settings);
                        after.call();
                        $dialog.modal('hide');
                    } else{
                        $dialog.find('h4.modal-title').html(data.title);
                        $dialog.find('div.modal-body').html(data.html);
                    }
                });
            });
            $dialog.modal();
        }
    });
}