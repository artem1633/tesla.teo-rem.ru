<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AboutCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-company-form">
 <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site_name')->textInput() ?>

    <?php if(!$model->isNewRecord) { ?>

        <?= $form->field($model, 'login')->textInput() ?>

        <?= $form->field($model, 'parol')->textInput() ?>
        
    <?php } ?>
    
    <div style="display: none;">
    <?= $form->field($model, 'smslog')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smspas')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'user_by_cr')->textInput() ?>

    <?= $form->field($model, 'user_by_up')->textInput() ?>

    <?= $form->field($model, 'date_cr')->textInput() ?>

    <?= $form->field($model, 'date_up')->textInput() ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
