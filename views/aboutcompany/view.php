<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AboutCompany */

$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'About Companies', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-company-view">
 <div class="box box-default">  
        <div class="box-body"> 
   <!--  <h1><?= Html::encode($this->title) ?></h1>

    <p>
        
       <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?> 
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
    //        'id',
            'name',
            'adress',
            'bank',
            'director',
            'email:email',
            'phone',
            'site_name',
            'smslog',
            //'smspas',
            //'user_by_cr',
            //'user_by_up',
            //'date_cr',
            //'date_up',
        ],
    ]) ?>
<?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
</div>
</div>
</div>