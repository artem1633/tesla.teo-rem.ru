<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\AboutCompany */

//$this->title = 'Создание About Company';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'About Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-company-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
