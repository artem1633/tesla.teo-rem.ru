<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cash */

$this->title = $title. $model->id;
$this->params['breadcrumbs'][] = ['label' => $label,'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-view">
        <div class="box box-default">   
        <div class="box-body">
    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Печатать', ['print', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type_cash_by',
            'summa',
			 'type',
			 
            'comment',
            'user_by_cr',
            'user_by_up',
            [
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
           
        ],
    ]) ?>

</div>
</div>
</div>
