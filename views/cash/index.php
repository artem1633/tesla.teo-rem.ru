<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$gridColumn = [
	[
		'attribute' => 'type_cash_by',				
		'label' => 'Статья',	
		'value'=>'typecash.name',
	],
	'summa',
	[
	'attribute'=>'type',
	'content'=>function($data){
		if ($data->type == "income")
		{return '<span class="label label-success">Приход</span>'; }
		else{return '<span class="label label-danger">Расход</span>';}			 
		},
	],
	[
		'attribute' => 'user_by_cr',				
		'label' => 'Создал',	
		'value'=>'usercr.name',
	],
	[
		'attribute' => 'date_cr',
		'format' => ['datetime', 'php:d.m.Y H:i:s']
	],
	'comment',      
];
?>
<div class="cash-index">

<div class="box box-default">	
	<div class="box-body">

			<div class="row">

				<div class="col-md-4">
			    
			      <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
					<?= Html::a('Печатать', ['cash/printlist','searchId' => $searchId,'linkTime' => $linkTime,'statusSearch'=> $statusSearch], ['class' =>'btn btn-info','target'=>"_blank"]) ?>
					<?php 

						echo ExportMenu::widget([
						    'dataProvider' => $dataProvider,
						    'columns' => $gridColumn
						]);
						?>
					
				  </div>
				 
			</div>

				<br/>
			<div class="row">
				<div class="col-md-8">
					<div class="btn-group btn-group-sm" role="group" aria-label="...">
					<?php echo ButtonDropdown::widget([
										'label' => $labelId,
										'options' => [
											'class' => 'btn btn-primary',
											'style' => 'font-size:13px',
											],
											'dropdown' => [
											'items' => [
																[
																	'label' => 'За все время',
																	'url' => ['','statusSearch'=> $statusSearch],
																],
																[
																	'label' => 'Сегодня',
																	'url' => ['index', 'searchId' => 'today', 'statusSearch'=> $statusSearch],
																],
																[
																	'label' => 'Вчера',
																	'url' =>  ['index', 'searchId' => 'yesterday', 'statusSearch'=> $statusSearch],
																],
																[
																	'label' => 'С начала недели',
																	'url' => ['index', 'searchId' => 'week', 'statusSearch'=> $statusSearch],
																],
																[
																	'label' => 'За прошлую неделю',
																	'url' => ['index', 'searchId' => 'lastWeek', 'statusSearch'=> $statusSearch],
																],
																[
																	'label' => 'С начала месяца',
																	'url' => ['index', 'searchId' => 'Month', 'statusSearch'=> $statusSearch],
																],
																[
																	'label' => 'За прошлый месяц',
																	'url' => ['index', 'searchId' => 'lastMonth', 'statusSearch'=> $statusSearch],
																],
															]
										]	
								]);?>
								<?= Html::dropDownList('status', $statusSearch, $typeCash, ['style'=> 'font-size:16px','prompt' => 'Установите статус','class' => 'btn btn-primary',
											'onchange' =>'window.location.href = "/cash/index?linkTime='.$linkTime.'&searchId='.$searchId.'&statusSearch="+$(this).val();']);?>
								<?= Html::input('date','',$linkTime,['style'=> 'font-size:12.5px','class' => 'btn btn-primary','onchange' =>'window.location.href = "/cash/index?statusSearch='.$statusSearch.'&searchId=dinamick&linkTime="+$(this).val();']) ?>
					
						</div>
				</div>
			</div>
	</div>
</div>

			<br/>
			
			<br/>
<div class="box box-default">	
	<div class="box-body">
		<?php Pjax::begin(); ?>    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
				'summary' => 'Итого: '.$sum,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn',
				//	'style'=>'font-size:12px',
					
					],
					

		           // 'id',
					[
						'attribute' => 'type_cash_by',				
						'label' => 'Статья',	
						'value'=>'typecash.name',
					 ],
		            'summa',
		            [
						'attribute'=>'type',
						'content'=>function($data){
							if ($data->type == "income")
							{return '<span class="label label-success">Приход</span>'; }
							else{return '<span class="label label-danger">Расход</span>';}			 
						},
					],
					 [
						'attribute' => 'user_by_cr',				
						'label' => 'Создал',	
						'value'=>'usercr.name',
					 ],
					
		            // 'user_by_up',
		            
		             [
						'attribute' => 'date_cr',
						'format' => ['datetime', 'php:d.m.Y H:i:s']
		            ],
					 
					
		            'comment',
		          //  [
				//		'attribute' => 'date_cr',
				//		'format' => ['datetime', 'php:d.m.Y H:i:s']
		       //     ],

		            ['class' => 'yii\grid\ActionColumn'
					
					],
		        ],
		    ]); ?>
		<?php Pjax::end(); ?>
			
		</div>
	</div>
</div>
