<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cash */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cash-form">
	<div class="box box-default">	
		<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>


	<?= $form->field($model, 'type_cash_by')
		->dropDownList($typeCash,
		['prompt' => 'Выберите один вариант']);	?>


    <?= $form->field($model, 'summa')->textInput() ?>

    <?= $form->field($model, 'comment')->textInput() ?>
	
	<?= $form->field($model, 'type')
		->dropDownList([
		'income'=>'Приход',
		'outlay'=>'Расход',
		],
		['prompt' => 'Выберите один вариант']);	?>
		
		
	
	<div style="display:none">
    <?= $form->field($model, 'user_by_cr')->textInput(['value'=>$user_by_cr_v]) ?>

    <?= $form->field($model, 'user_by_up')->textInput(['value'=>$user_by_up_v]) ?>

     
    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
	</div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
