<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Shablon */

$this->title = 'Create Shablon';
$this->params['breadcrumbs'][] = ['label' => 'Shablons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shablon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
