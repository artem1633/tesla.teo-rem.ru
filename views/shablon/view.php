<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Shablon */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Шаблоны', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shablon-view">   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'komp_name',
            'komp_address',
            'komp_tel',
            'rejim_raboti',
            'tekst:html',
        ],
    ]) ?>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>        
    </p>
</div>
