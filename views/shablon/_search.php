<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ShablonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shablon-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'komp_name') ?>

    <?= $form->field($model, 'komp_address') ?>

    <?= $form->field($model, 'komp_tel') ?>

    <?= $form->field($model, 'rejim_raboti') ?>

    <?php // echo $form->field($model, 'tekst') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
