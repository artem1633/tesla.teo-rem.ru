<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;


?>

<div class="shablon-form">

    <?php $form = ActiveForm::begin(); ?>
    
        <div class="col-md-6">
            
            <?= $form->field($model, 'komp_name')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'komp_tel')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="row">
            <div class="col-md-6">

                <?= $form->field($model, 'komp_address')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'rejim_raboti')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="col-md-12">
        <?php if($model->id == 2){
            
            echo $form->field($model, 'tekst')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                ],
            ]);
            

             } 
        ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
