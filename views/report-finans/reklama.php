<?php

use app\models\JobList;
use app\models\OrderSearch;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use app\models\Order;
use yii\helpers\ArrayHelper;
use app\models\manual\Marketing;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по рекламы';
//$this->params['breadcrumbs'][] = $this->title;

if($reklama == null) {
    $marks = Marketing::find()->all();
    $i=0; $marketing="";
        foreach ($marks as $value) {
            if($i == 0) $marketing .= $value->id;
            else $marketing .= ",".$value->id;
            $i=1;
        }
}
else {
    $marketing = $reklama;
}
?>
<div class="order-index">

    <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <div class="box box-default">  
        <div class="box-body"> 
            <br/>
            <div class="row">

                        <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
                        <div class="col-md-3">
                                      
                        <?= Html::dropDownList('reklama', $reklama, ArrayHelper::map(Marketing::find()->all(), 'id','name'), ['prompt' => 'Выберите рекламу','class' => 'btn btn-primary']);?>
                        </div>
                        <div class="col-md-3">
                            <?= Html::input('date','dateStart',$dateStart,['class' => 'form-control',]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= Html::input('date','dateEnd',$dateEnd,['class' => 'form-control']) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton('Поиск',['class' => 'btn btn-default']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
        </div>
    </div>
    <br/>

    <br/>

    <div class="order-index">
        <div class="box box-default">
            <div class="box-body">

                <table class="table table-striped table-bordered"><thead>
                    <tr>

                        <th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Названия рекламы<!-- </a> -->
                        <th><!-- <a href="/rdt2/rank/index?sort=order_id" data-sort="order_id"> -->
                                Всего<!-- </a> --></th>
                       

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $dataStartSearch = strtotime($dateStart);
                    $dataEndSearch = strtotime($dateEnd);                    
                    if($dataStartSearch == null && $dataEndSearch == null) $sql = "SELECT `marketing_by`, COUNT(id) as price FROM `order` WHERE `marketing_by` IN (".$marketing.") GROUP BY `marketing_by`";
                    else $sql = "SELECT `marketing_by`, COUNT(id) as price FROM `order` WHERE (`date_up` BETWEEN '".$dataStartSearch."' AND '".$dataEndSearch."' ) AND `marketing_by` IN (".$marketing.") GROUP BY `marketing_by`";
                    
                    $query = Order::findBySql($sql);                    
                    $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => false,
                    ]);
                    $work = 0;
                    $sumWork = 0;
                    $finish = 0;
                    $sumFinish = 0;
                    $sumAll = 0;
                    /** @var Order $item */
                    foreach ($dataProvider->getModels() as $item) {
                       echo '<tr data-key="100">
                        <td width = 200px>'.$item->marketing->name.'</td>
                        <td width = 200px>'.$item->price.'</td>
                        </tr>';
                    }
                                       

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
