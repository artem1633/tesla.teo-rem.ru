<?php

use app\models\Available;
use app\models\JobList;
use app\models\OrderSearch;
use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по прибыли';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <div class="box box-default">  
        <div class="box-body"> 
            <br/>
            <div class="row">

                        <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
                        <div class="col-md-4">
                            <?= Html::input('date','dateStart',$dateStart,['class' => 'form-control',]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= Html::input('date','dateEnd',$dateEnd,['class' => 'form-control']) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton('Поиск',['class' => 'btn btn-default']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
            <br/>
        </div>
    </div>

    <br/>

    <div class="order-index">
        <div class="box box-default">
            <div class="box-body">

                <table class="table table-striped table-bordered"><thead>
                    <tr>

                        <th><!-- <a href="/rdt2/rank/index?sort=order_id" data-sort="order_id"> -->
                                Всего заказов<!-- </a> --></th>
                        <th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Сумма услуг<!-- </a> -->
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Сумма товара<!-- </a> -->
                        </th></th>
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Цена закупа<!-- </a> -->
                        </th></th>
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                ЗП Мастера<!-- </a> -->
                        </th></th>
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Итого:<!-- </a> -->
                        </th></th></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $dataStartSearch = strtotime($dateStart);
                    $dataEndSearch = strtotime($dateEnd);
                    $query = OrderSearch::find();
                    if ($dataStartSearch) {$query->andFilterWhere(['>=','date_up',$dataStartSearch]);}
                    if ($dataEndSearch) {$query->andFilterWhere(['<=','date_up',$dataEndSearch]);}
                    $query->andFilterWhere(['=','status_order','7']);

                    $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => false,
                    ]);
                    $sumService = 0;//Cумма за услуги
                    $sumParts = 0;//Cумма за товар
                    $sumPartAvalid = 0;//Сумма закупа
                    $sumZp = 0; //Сумма зарплаты
                    $sumFinish = 0;//Сумма итого
                    /** @var Order $item */
                    foreach ($dataProvider->getModels() as $item) {
                        $listAllService = JobList::find()->where(['order_by' => $item->id,'type' => 'service'])->all();
                        /** @var JobList $job */
                        foreach ( $listAllService as $job) {
                            $sumService += $job->count * $job->price;
                            /** @var User $infoUser */
                            $infoUser = User::find()->where(['id'=>$item->user_by])->one();
                            $price = $job->price/100;
                            $sumZp += $price * $infoUser->percent_finish;
                        }
                        $listAllParts = JobList::find()->where(['order_by' => $item->id,'type' => 'parts'])->all();
                        /** @var JobList $job */
                        foreach ( $listAllParts as $job) {
                            $sumParts += $job->count * $job->price;
                            /** @var Available $avalid */
                            $avalid = Available::find()->where(['parts_by' => $job->job_by])->one();
                            $sumPartAvalid += $avalid->price_shop;
                        }
                    }
                    $sumFinish = $sumService + $sumParts - $sumPartAvalid - $sumZp;
                    $count = $dataProvider->getTotalCount();
                    echo '<tr data-key="100">
                        <td>'.$count.'</td>
                        <td>'.$sumService.'</td>
                        <td>'.$sumParts.'</td>
                        <td>'.$sumPartAvalid.'</td>
                        <td>'.$sumZp.'</td>
                        <td>'.$sumFinish.'</td></tr>';
                    //}
                    //	print_r($users);

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
