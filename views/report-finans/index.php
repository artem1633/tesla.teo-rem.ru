<?php

use app\models\JobList;
use app\models\OrderSearch;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по заказам';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="box box-default">  
    <div class="box-body"> 
        <br/>
        <div class="row">

                <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
                <div class="col-md-4">
                    <?= Html::input('date','dateStart',$dateStart,['class' => 'form-control',]) ?>
                </div>
                <div class="col-md-4">
                    <?= Html::input('date','dateEnd',$dateEnd,['class' => 'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Поиск',['class' => 'btn btn-default']) ?>
                </div>

                <?php ActiveForm::end(); ?>

        </div>
    <br/>
    </div>
</div>

    <br/>

    <div class="order-index">
        <div class="box box-default">
            <div class="box-body">

                <table class="table table-striped table-bordered"><thead>
                    <tr>

                        <th><!-- <a href="/rdt2/rank/index?sort=order_id" data-sort="order_id"> -->
                                Всего<!-- </a> --></th>
                        <th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                               В работе<!-- </a> -->
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Завершено Успешно<!-- </a> -->
                        </th></th>
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Сумма всего<!-- </a> -->
                        </th></th>
                        </th><th><!-- <a href="/rdt2/rank/index?sort=status" data-sort="status"> -->
                                Сумма выполнено<!-- </a> -->
                        </th></th></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $dataStartSearch = strtotime($dateStart);
                    $dataEndSearch = strtotime($dateEnd);
                    $query = OrderSearch::find();
                    $query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
                        ->andWhere('order.user_by_cr = user.id');

                    if ($dataStartSearch) {$query->andFilterWhere(['>=','order.date_up',$dataStartSearch]);}
                    if ($dataEndSearch) {$query->andFilterWhere(['<=','order.date_up',$dataEndSearch]);}
                    $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => false,
                    ]);
                    $work = 0;
                    $sumWork = 0;
                    $finish = 0;
                    $sumFinish = 0;
                    $sumAll = 0;
                    /** @var Order $item */
                    foreach ($dataProvider->getModels() as $item) {
                        $sumAll = $sumAll + $item->price;
                        if ($item->status_order == '2') {
                            $work ++;
                        }
                        if ($item->status_order == '7') {
                            $finish++;
                            $sumFinish = $sumFinish + $item->price;
                        }
                    }
                    $count = $dataProvider->getTotalCount();
                    echo '<tr data-key="100">
                        <td>'.$count.'</td>
                        <td>'.$work.'</td>
                        <td>'.$finish.'</td><td>'.$sumAll.'</td><td>'.$sumFinish.'</td></tr>';
                    //}
                    //	print_r($users);

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
