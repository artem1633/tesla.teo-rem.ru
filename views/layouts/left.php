<?php

use app\models\AboutCompany;

$id = Yii::$app->user->identity->id;
$permission = Yii::$app->user->identity->permission;
$company_id = Yii::$app->user->identity->company_id;
$about_company = AboutCompany::find()->where(['company_id' => $company_id])->one();

?>
<aside class="main-sidebar">
    <section class="sidebar">      
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    //['label' => 'Меню g-service', 'options' => ['class' => 'header']],
					
					['label' => 'Показатели', 'icon' => 'dashboard', 'url' => ['/indicators']],
					['label' => 'Заказы', 'icon' => 'archive', 'url' => ['/order']],
					['label' => 'Магазин', 'icon' => 'shopping-cart', 'url' => ['/shop']],
					['label' => 'Касса', 'icon' => 'rub', 'url' => ['/cash']],
					['label' => 'Склад', 'icon' => 'cubes', 'url' => ['/available']],
                    ['label' => 'СМС', 'icon' => 'send', 'url' => ['/sending-sms']],
                    ['label' => 'Склады', 'icon' => 'cube', 'url' => ['/storage'],'visible' => ($permission == 'admin' || $permission == 'super_admin')],
                    
                    ['label' => 'Отчет', 'icon' => 'bar-chart', 'url' => ['/report/finance'],
                        'items' => [                            
                                    ['label' => 'Зарплаты', 'icon' => 'area-chart', 'url' => ['/report/finance'],],
                                    ['label' => 'По продажам', 'icon' => 'area-chart', 'url' => ['/report-finans'],],
                                    ['label' => 'По рекламе', 'icon' => 'area-chart', 'url' => ['/report-finans/reklama'],],
                                    ['label' => 'Прибыль по заказов','icon' =>'area-chart', 'url' => ['/report-finans/profit'],],
                        ],
                    ],
                    
                    ['label' => 'Справочники', 'icon' => 'book', 'url' => ['/nothealth'],
                        'items' => [
                            ['label' => 'Не исправности', 'icon' => 'pencil', 'url' => ['/nothealth'],],
                            ['label' => 'Реклама', 'icon' => 'pencil', 'url' => ['/marketing'],],
                            ['label' => 'Запчасти', 'icon' => 'pencil', 'url' => ['/parts'],],
                            ['label' => 'Денежные движения', 'icon' => 'pencil', 'url' => ['/type-cash'],],
                            //['label' => 'Группы запчастей', 'icon' => 'fa fa-pencil', 'url' => ['/group-parts'],],
                            ['label' => 'Группы Клиентов', 'icon' => 'pencil', 'url' => ['/group-client'],],                            
                            ['label' => 'Статусы запчастей', 'icon' => 'pencil', 'url' => ['/status-parts'],],
                            ['label' => 'Статусы заказов', 'icon' => 'pencil', 'url' => ['/status-order'],],
                            ['label' => 'Тип товара', 'icon' => 'pencil', 'url' => ['/type-parts'],],
                            ['label' => 'Типы заказов', 'icon' => 'pencil', 'url' => ['/type-order'],],                         
                            ['label' => 'Типы устройств', 'icon' => 'pencil', 'url' => ['/type-device'],],
                            ['label' => 'Производители', 'icon' => 'pencil', 'url' => ['/made'],],
                            ['label' => 'Модели', 'icon' => 'pencil', 'url' => ['/model-device'],],
                            ['label' => 'Услуги', 'icon' => 'pencil', 'url' => ['/service'],],
                        ],
                    ],
                    ['label' => 'Настройки', 'icon' => 'wrench', 'url' => ['/about-company'],
                        'visible' => ($permission == 'admin' || $permission == 'super_admin'),
                        'items' => [
                            ['label' => 'Клиенты', 'icon' => 'briefcase', 'url' => ['/client'],],
                            ['label' => 'Сотрудники', 'icon' => 'briefcase', 'url' => ['/user'],],
                            ['label' => 'О компании', 'icon' => 'briefcase', 'url' => ['/about-company/view'],],
                           // ['label' => 'Документы', 'icon' => 'file-pdf-o', 'url' => ['/'],],
                            ['label' => 'Шаблон выполненных работ', 'icon' => 'briefcase', 'url' => ['/template/update?key=done'],],
                            ['label' => 'Шаблон приемки на ремонт', 'icon' => 'briefcase', 'url' => ['/template/update?key=acceptance'],],
                            ['label' => 'Шаблон форма заказа', 'icon' => 'briefcase', 'url' => ['/template/update?key=order_form'],],
                            //['label' => 'Смс шаблон', 'icon' => 'briefcase', 'url' => ['/template/update?id=3'],],
                            //['label' => 'Общие настройки', 'icon' => 'briefcase', 'url' => ['/nastroyka/changesms'],],
                        ],		
					],
                    [
                        'label' => 'Администратор',
                        'icon' => 'book',
                        'url' => '#',
                        'visible' => \Yii::$app->user->identity->isSuperAdmin(),
                        'items' => [
                            ['label' => 'Компании', 'icon' => 'list-alt', 'url' => ['/companies'],],
                            ['label' => 'Статистика', 'icon' => 'list-alt', 'url' => ['/statistic'],],
                            ['label' => 'Логи', 'icon' => 'list-alt', 'url' => ['/logs'],],
                            ['label' => 'Тарифы', 'icon' => 'list-alt', 'url' => ['/rates'],],
                            ['label' => 'Шаблоны сообщений', 'icon' => 'list-alt', 'url' => ['/email-templates'],],
                        ],
                    ],
                    ['label' => 'Инструкция', 'icon' => 'book', 'url' => ['/site/instruksiya']],
                ],
            ]
        ) ?>

    </section>

</aside>
