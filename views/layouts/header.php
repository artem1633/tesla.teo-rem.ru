<?php
use yii\helpers\Html;
use app\models\Order;
use app\models\AboutCompany;
use yii\helpers\Url;

\Yii::$app->db->createCommand()->update('user', ['last_activity_datetime' => date('Y-m-d H:i:s') ], [ 'id' => \Yii::$app->user->identity->id ])->execute(); 
\Yii::$app->db->createCommand()->update('companies', ['last_activity_datetime' => date('Y-m-d H:i:s') ], [ 'id' => \Yii::$app->user->identity->company_id ])->execute(); 

$sum 		= 0;
$finish		=	0;
$notFinish=	0;
$jobList 	=Order::find()->all();

$about = AboutCompany::find()->where(['id'=>1])->one();
$url_get = 'http://smsc.ru/sys/balance.php?login='.$about->smslog.'&psw='.$about->smspas;                
$ch = curl_init();  
curl_setopt($ch,CURLOPT_URL,$url_get);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
$result=curl_exec($ch);
$r=json_decode($result,TRUE);
$sms_balans = $r;
curl_close($ch);


foreach($jobList as $job)
{
	
	if ($job->status_order == 7){$finish++; $sum += $job->price;}else{$notFinish++;}
}	//считаем сумму по заказу	
?>
<?php if($sms_balans=="") {$sms =  'Смс сервис не установлено или не правильный логин и пароль';} 
else $sms = 'Смс баланс: '.$sms_balans .' р.'; ?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' ."TEO-CRM" /*Yii::$app->name*/ . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span> </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Информация
                </a>
                <ul class="dropdown-menu" id="ajaxCrudDatatable" >
                    <li>
                        <ul class="menu">
                          <li>
                            <?php if(Yii::$app->user->isGuest === false): ?>
                                <?= Html::a('Лицензия истечет '.Yii::$app->formatter->asDate(Yii::$app->user->identity->getCompanyInstance()->access_end_datetime, 'php:d.m.Y'), ['#'])?>
                            <?php endif; ?>
                          </li>
                            <li>
                                <?= Html::a('<i class="fa fa-rub"></i>На сумму выполнено: '.$sum .' р.', ['#'], [] ) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-hourglass"></i>Не выполнено заказов:'.$notFinish .' р.', ['#'], [] ) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-send"></i>'.$sms, ['#'], [] ) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-flag"></i>Выполнено заказов : '.$finish, ['#'], [] ) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-cog fa-lg"></i>Настройки', ['/settings'], [] ) ?>
                            </li>
                        </ul>
                    </li>
                </ul>
              </li>


                <!-- <li>
                    <?php //if(Yii::$app->user->isGuest === false): ?>
                        <?php //Html::a('Лицензия истечет '.Yii::$app->formatter->asDate(Yii::$app->user->identity->getCompanyInstance()->access_end_datetime, 'php:d.m.Y'), ['#'])?>
                    <?php //endif; ?>
                </li>
                <?php //if(Yii::$app->user->isGuest === false && Yii::$app->user->identity->isSuperAdmin()): ?>
                  <li>
                      <?php /*Html::a('<i class="fa fa-cog fa-lg"></i>',
                          ['/settings'],
                          ['title' => 'Настройки']
                      )*/ ?>
                  </li>
                              <?php //endif; ?> -->

                <!-- Messages: style can be found in dropdown.less-->
<!--                 <li class="dropdown messages-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-rub"></i>
          <span class="label label-success"><?php //$sum ?></span>
      </a>
      <ul class="dropdown-menu">
          <li class="header">Заказов на сумму выполнено:  <?php //$sum ?> р.</li>
      </ul>
  </li>
  
  <li class="dropdown notifications-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-hourglass"></i>
          <span class="label label-warning"><?php //$notFinish ?></span>
      </a>
      <ul class="dropdown-menu">
          <li class="header">Не выполнено заказов: <?php //$notFinish ?> шт.</li>
         
  </li>
      </ul>
  </li>
  <li class="dropdown notifications-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-send"></i>
          <span class="label label-warning"><?php //$sms_balans ?></span>
      </a>
      <ul class="dropdown-menu">
         
         </li>
      </ul>
  </li>
  Tasks: style can be found in dropdown.less
  <li class="dropdown tasks-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-flag-o"></i>
          <span class="label label-danger"><?php //$finish ?></span>
      </a>
      <ul class="dropdown-menu">
           <li class="header">Выполнено заказов: <?php //$finish ?> шт.</li>
        </li>
      </ul>
  </li>  -->  
                    
                <li class="dropdown user user-menu">
                    <?= Html::a('Выйти',['/site/logout'],['data-method' => 'post']) ?>
                </li>                
            </ul>
        </div>
    </nav>
</header>
