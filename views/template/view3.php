<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Template */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="template-view">
    <div class="box box-default">  
        <div class="box-body"> 

            <table class="table table-bordered table-condensed">
                <tr>
                    <td>{order_id}</td>
                    <td>Номер заказа</td>
                </tr>
                <tr>
                    <td>{status_name}</td>
                    <td>Статус заказа</td>
                </tr>
                <tr>
                    <td>{status_tekst}</td>
                    <td>Текст статуса</td>
                </tr>
                <tr>
                    <td>{summa}</td>
                    <td>Сумма заказа</td>
                </tr>
            </table>
        </div>
    </div>
</div>
