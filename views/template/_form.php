<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(); ?>
<div class="template-form">
  <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-3 center">
                 <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>            
            </div>
       
            <div class="col-md-5 vcenter"  style="margin-top: 25px;">
                <?php if($model->id !=4) echo $form->field($model, "dostup")->checkbox([]); ?>
            </div>
       

            <div class="col-md-1 vcenter">
                <div class="form-group"  style="margin-top: 25px;">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1 vcenter">
                    <div class="form-group"  style="margin-top: 25px; margin-left: 10px;">
                        <?php if($model->id == 1) echo Html::button('Обозначения',  ['value'=>Url::to('view1?id=1'), 'class'=>'btn btn-success', 'id' => 'modalButton']) ;?>
                        <?php if($model->id == 2) echo Html::button('Обозначения',  ['value'=>Url::to('view2?id=2'), 'class'=>'btn btn-success', 'id' => 'modalButton']) ;?>
                        <?php if($model->id == 3) echo Html::button('Обозначения',  ['value'=>Url::to('view3?id=3'), 'class'=>'btn btn-success', 'id' => 'modalButton']) ;?>
                        <?php if($model->id == 4) echo Html::button('Обозначения',  ['value'=>Url::to('view4?id=4'), 'class'=>'btn btn-success', 'id' => 'modalButton']) ;?>

                        
                    </div>
                </div>
            </div>
</div>
</div>  
<br>
<div class="box box-default">  
        <div class="box-body"> 
        <?php 
        Modal::begin([
            'header'=>'<h4>Обозначения</h4>',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";
        Modal::end();
        ?>
       
   

<div class="row">
     <div class="col-md-12 center">
    <?php 
        echo $form->field($model, 'tekst')->widget(CKEditor::className(),[
            'editorOptions' => [
             'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false                    
                    ],
            ]); 
        ?>

    </div>
</div>

    <?php ActiveForm::end(); ?>
</div>
</div>
</div>
<?php Pjax::end(); ?>