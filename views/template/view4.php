<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Template */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="template-view">
    <div class="box box-default">  
        <div class="box-body"> 

            <table class="table table-bordered table-condensed">
                <tr>
                    <td>{akt_date}</td>
                    <td>Выдать</td>
                </tr>
                <tr>
                    <td>{tipzakaza}</td>
                    <td>Тип заказа</td>
                </tr>
                <tr>
                    <td>{akt_date}</td>
                    <td>Время акта</td>
                </tr>
                <tr>
                    <td>{client_name}</td>
                    <td>Ф.И.О клиента</td>
                </tr>
                <tr>
                    <td>{client_phone}</td>
                    <td>Телефонный номер клиента</td>
                </tr>
                <tr>
                    <td>{device}</td>
                    <td>Устройство</td>
                </tr>
                 <tr>
                    <td>{configuration}</td>
                    <td>Комплектация</td>
                </tr>
                 <tr>
                    <td>{appearance} </td>
                    <td>Внешний вид</td>
                </tr>
                 <tr>
                    <td>{prepay}</td>
                    <td>Предоплата</td>
                </tr>
                <tr>
                    <td>{order_usermas_name}</td>
                    <td>Мастер</td>
                </tr>
                <tr>
                    <td>{comment}</td>
                    <td>Коментарий</td>
                </tr>
                <tr>
                    <td>{reklama}</td>
                    <td>Реклама</td>
                </tr>
                <tr>
                    <td>{nothealth_name}</td>
                    <td>Не исправность</td>
                </tr>
                <tr>
                    <td>{discount_order}</td>
                    <td>Скидка</td>
                </tr>
                <tr>
                    <td>{device_made_name}</td>
                    <td>Производитель</td>
                </tr>
                 <tr>
                    <td>{model}</td>
                    <td>Модель</td>
                </tr>
                <tr>
                    <td>{price}</td>
                    <td>Цена</td>
                </tr>
                <tr>
                    <td>{barcode}</td>
                    <td>Штрих код</td>
                </tr>
            </table>
        </div>
    </div>
</div>
