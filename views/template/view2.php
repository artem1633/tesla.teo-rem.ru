<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Template */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="template-view">
    <div class="box box-default">  
        <div class="box-body"> 

            <table class="table table-bordered table-condensed">
                <tr>
                    <td>{company_name}</td>
                    <td>Имя компании</td>
                </tr>
                <tr>
                    <td>{company_adress}</td>
                    <td>Адрес компании</td>
                </tr>
                <tr>
                    <td>{company_phone}</td>
                    <td>Телефонный номер компании</td>
                </tr>
                <tr>
                    <td>{mode_of_operation}</td>
                    <td>Режим работы</td>
                </tr>
                <tr>
                    <td>{template_name}</td>
                    <td>Имя шаблона</td>
                </tr>
                <tr>
                    <td>{akt_number}</td>
                    <td>Номер акта</td>
                </tr>
                <tr>
                    <td>{akt_date}</td>
                    <td>Время акта</td>
                </tr>
                <tr>
                    <td>{client_name}</td>
                    <td>Ф.И.О клиента</td>
                </tr>
                <tr>
                    <td>{client_phone}</td>
                    <td>Телефонный номер клиента</td>
                </tr>
                <tr>
                    <td>{device}</td>
                    <td>Устройство</td>
                </tr>
                 <tr>
                    <td>{device_made_name}</td>
                    <td>Производитель устройство</td>
                </tr>
                 <tr>
                    <td>{device_modeldevice_name} </td>
                    <td>Модель устройство</td>
                </tr>
                 <tr>
                    <td>{nothealth_name}</td>
                    <td>Не исправность</td>
                </tr>
                 <tr>
                    <td>{discount_order}</td>
                    <td>Скидка</td>
                </tr>
                 <tr>
                    <td>{prepay}</td>
                    <td>Предоплата</td>
                </tr>
                 <tr>
                    <td>{warranty_period}</td>
                    <td>Гарантийный период</td>
                </tr>
                 <tr>
                    <td>{table}</td>
                    <td>Таблица акта</td>
                </tr>
                 <tr>
                    <td>{client_signature}</td>
                    <td>Подпись клиента</td>
                </tr>
                 <tr>
                    <td>{employee_signature}</td>
                    <td>Подпись сотрудника</td>
                </tr>
                 <tr>
                    <td>{order_usermas_name}</td>
                    <td>Имя мастера</td>
                </tr>
                <tr>
                    <td>{barcode}</td>
                    <td>Штрих код</td>
                </tr>
            </table>
        </div>
    </div>
</div>
