<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Template */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="template-view">
    <div class="box box-default">  
        <div class="box-body"> 

            <table class="table table-bordered table-condensed">
                <tr>
                    <td>{company_name}</td>
                    <td>Имя компании</td>
                </tr>
                <tr>
                    <td>{company_adress}</td>
                    <td>Адрес компании</td>
                </tr>
                <tr>
                    <td>{company_phone}</td>
                    <td>Телефонный номер компании</td>
                </tr>
                <tr>
                    <td>{mode_of_operation}</td>
                    <td>Режим работы</td>
                </tr>
                <tr>
                    <td>{template_name}</td>
                    <td>Имя шаблона</td>
                </tr>
                <tr>
                    <td>{akt_number}</td>
                    <td>Номер акта</td>
                </tr>
                <tr>
                    <td>{akt_date}</td>
                    <td>Время акта</td>
                </tr>
                <tr>
                    <td>{client_name}</td>
                    <td>Ф.И.О клиента</td>
                </tr>
                <tr>
                    <td>{client_phone}</td>
                    <td>Телефонный номер клиента</td>
                </tr>
                <tr>
                    <td>{device}</td>
                    <td>Устройство</td>
                </tr>
                 <tr>
                    <td>{configuration}</td>
                    <td>Комплектация</td>
                </tr>
                 <tr>
                    <td>{appearance} </td>
                    <td>Внешний вид</td>
                </tr>
                 <tr>
                    <td>{defect}</td>
                    <td>Дефект</td>
                </tr>
                 <tr>
                    <td>{approx_data}</td>
                    <td>Ориентировочная дата готовности</td>
                </tr>
                 <tr>
                    <td>{approx_cost}</td>
                    <td>Ориентировочная стоимость</td>
                </tr>
                 <tr>
                    <td>{preorder}</td>
                    <td>Предоплата</td>
                </tr>
                 <tr>
                    <td>{notes}</td>
                    <td>Заметки приемщика</td>
                </tr>
                 <tr>
                    <td>{usercr_name}</td>
                    <td>Имя приёмщика</td>
                </tr>
                 <tr>
                    <td>{client_name}</td>
                    <td>Имя клиента</td>
                </tr>
                <tr>
                    <td>{barcode}</td>
                    <td>Штрих код</td>
                </tr>
            </table>
        </div>
    </div>
</div>
