<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Template */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

if($model->dostup == 1)$d='Есть';
else $d = 'Нет';
?>
<div class="template-view">
 <div class="box box-default">  
        <div class="box-body"> 

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            //'dostup',
            [
                'attribute'=>'dostup',
                'value' => $d,
            ],
            'tekst:html',
        ],
    ]) ?>

</div>
</div>
</div>
