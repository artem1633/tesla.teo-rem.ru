<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AktRemont */


?>
<div class="akt-remont-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'top:html',
            'shablon_name',
            'klient',
            'telephone',
            'device',
            'equipment',
            'appearance',
            'defect',
            'orient_data',
            'orient_cost',
            'prepay',
            'zametka',
            'tekst:html',
            'priyomshik',
            'inform_master',
        ],
    ]) ?>

       <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>       
       </p>
</div>
