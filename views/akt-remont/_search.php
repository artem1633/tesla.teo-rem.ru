<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AktRemontSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akt-remont-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'top') ?>

    <?= $form->field($model, 'shablon_name') ?>

    <?= $form->field($model, 'klient') ?>

    <?= $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'device') ?>

    <?php // echo $form->field($model, 'equipment') ?>

    <?php // echo $form->field($model, 'appearance') ?>

    <?php // echo $form->field($model, 'defect') ?>

    <?php // echo $form->field($model, 'orient_data') ?>

    <?php // echo $form->field($model, 'orient_cost') ?>

    <?php // echo $form->field($model, 'prepay') ?>

    <?php // echo $form->field($model, 'zametka') ?>

    <?php // echo $form->field($model, 'tekst') ?>

    <?php // echo $form->field($model, 'priyomshik') ?>

    <?php // echo $form->field($model, 'inform_master') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
