<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AktRemont */

$this->title = 'Create Akt Remont';
$this->params['breadcrumbs'][] = ['label' => 'Akt Remonts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akt-remont-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
