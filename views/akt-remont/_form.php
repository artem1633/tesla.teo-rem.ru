<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\AktRemont */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akt-remont-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?php 
        echo $form->field($model, 'top')->widget(CKEditor::className(),[
            'editorOptions' => [
             'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false                    
                    ],
            ]); 
        ?>
    <?php 
        echo $form->field($model, 'tekst')->widget(CKEditor::className(),[
            'editorOptions' => [
             'preset' => 'advanced', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false                    
                    ],
            ]); 
        ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'shablon_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'klient')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'device')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'equipment')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'appearance')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'defect')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
         <div class="col-md-6">

            <?= $form->field($model, 'orient_data')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'orient_cost')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'prepay')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'zametka')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'priyomshik')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'inform_master')->textInput(['maxlength' => true]) ?>
            <br>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
  </div>

    <?php ActiveForm::end(); ?>

</div>
