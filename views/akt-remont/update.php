<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AktRemont */

/*$this->title = 'Update Akt Remont: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Akt Remonts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';*/
?>
<div class="akt-remont-update">

   
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
