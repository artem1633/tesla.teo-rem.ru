<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AktRemontSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Akt Remonts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akt-remont-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Akt Remont', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'top:ntext',
            'shablon_name',
            'klient',
            'telephone',
            // 'device',
            // 'equipment',
            // 'appearance',
            // 'defect',
            // 'orient_data',
            // 'orient_cost',
            // 'prepay',
            // 'zametka',
            // 'tekst:ntext',
            // 'priyomshik',
            // 'inform_master',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
