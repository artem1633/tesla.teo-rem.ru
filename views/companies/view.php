<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->company_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'company_name',
                'created_date',
                [
                    'attribute' => 'admin_id',
                    'value' => function($data){
                        return $data->admin->name;
                    }
                ],
                [
                    'attribute' => 'rate_id',
                    'value' => function($data){
                        return $data->rate->name;
                    }
                ],
                'last_activity_datetime',
                'access_end_datetime',
                [
                    'attribute' => 'is_super',
                    'value' => function($data){
                        if($data->is_super) return 'Да';
                        else return 'Нет';
                    }
                ],
            ],
        ]) ?>
    </div>
</div>

           