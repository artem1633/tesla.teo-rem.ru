<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="companies-form">
    <div class="user-form box box-primary">
        <div class="box-body table-responsive">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'admin_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name')) ?>

            <?= $form->field($model, 'rate_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Rates::find()->all(), 'id', 'name')) ?>

            <?= $form->field($model, 'access_end_datetime')->widget(\kartik\date\DatePicker::class, [
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>

            <div style="display: none;">
                <?= $form->field($model, 'last_activity_datetime')->textInput() ?>
                <?= $form->field($model, 'is_super')->textInput(['value' => 0]) ?>
                <?= $form->field($model, 'created_date')->textInput(['value' => date('Y-m-d H:i:s')]) ?>                
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
