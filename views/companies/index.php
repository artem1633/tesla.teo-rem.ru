<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
/* @var $this yii\web\View */

/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Компании');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Создать'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div style="width: 100%; overflow: auto"> 
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'company_name',
                'created_date',
                [
                    'attribute' => 'admin_id',
                    'value' => function($model){
                        return $model->admin->name;
                    },
                ],
                'last_activity_datetime',
                'access_end_datetime',
                [
                    'attribute' => 'rate_id',
                    'value' => function($model){
                        if($model->rate_id != null) return $model->rate->name;
                        else return null;
                    },
                ],
                [
                    'attribute' => 'is_super',
                    'value' => function($model){
                        if($model->is_super == 1) return 'Да';
                        else 'Нет';
                    },
                ],
                [
                  'class' => 'yii\grid\ActionColumn',
                ],
            ],
        ]); ?>
    </div>
</div>



