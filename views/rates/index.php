<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Rates;
/* @var $this yii\web\View */

/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Тарифы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Создать'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div >
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                
                'id',
                'name',
                'price',
                [
                    'attribute' => 'time',
                    'value' => function($model){
                        $timeList = Rates::getTimeList();
                        if(isset($timeList[$model->time])) {
                            return $timeList[$model->time];
                        }
                        return Yii::$app->formatter->asRaw($model->time);
                    },
                ],
                'sort',
                [
                  'class' => 'yii\grid\ActionColumn',
                ],
            ],
        ]); ?>
    </div>
</div>




