<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Rates;

/* @var $this yii\web\View */
/* @var $model app\models\Rates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rates-form">
	<div class="user-form box box-primary">
        <div class="box-body table-responsive">

		    <?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>

		    <?= $form->field($model, 'time')->dropDownList(Rates::getTimeList(), ['prompt' => 'Выберите срок']) ?>

		    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>

		    <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		    
		</div>
	</div>
</div>
