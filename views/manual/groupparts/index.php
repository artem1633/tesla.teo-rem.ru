<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\manual\GroupPartsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Group Parts';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-parts-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <!-- <?= Html::a('Create Group Parts', ['create'], ['class' => 'btn btn-success']) ?>
        --><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            //'id',
            'name',
            /* [
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{update}  {link}'
			],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
