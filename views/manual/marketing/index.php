<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\manual\MarketingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->registerJsFile(Yii::$app->request->baseUrl.DIRECTORY_SEPARATOR.'js/dialog.js', [
    'depends' => [
        \yii\web\JqueryAsset::className(),
    ],
    'position' => \yii\web\View::POS_HEAD
]);
?>
<div class="marketing-index">
 <div class="box box-default">  
        <div class="box-body"> 

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
</div>
<br>
<br>
 <div class="box box-default">  
        <div class="box-body"> 	

<?php Pjax::begin(); ?> <div style="width: 100%; overflow: scroll">    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
			'plan_month',
            [
                'attribute'=>'show_in_indicators',
                'filter'=>array("1"=>"Да","0"=>"Нет"),
                'content'=>function($data){
                    return $data->show_in_indicators?'Да':'Нет';
                },
            ],

			/*[
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{update}  {link}'
			],
			
        ],
    ]); ?></div>
<?php Pjax::end(); ?></div>
</div>
</div>