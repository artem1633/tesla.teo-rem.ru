<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Marketing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marketing-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'show_in_indicators')->checkbox() ?>

            <?= $form->field($model, 'plan_month')->textInput() ?>

            <div style="display:none">
                <?= $form->field($model, 'date_cr')->textInput(['value' => $date_cr_v]) ?>

                <?= $form->field($model, 'date_up')->textInput(['value' => $date_up_v]) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
