<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\Marketing */


$this->params['breadcrumbs'][] = ['label' => 'Реклама', 'url' => ['index']];

$this->params['breadcrumbs'][] = $title;
?>
<div class="marketing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
