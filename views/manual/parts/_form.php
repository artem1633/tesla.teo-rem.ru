<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Parts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parts-form">
 <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'group_by')
		->dropDownList($groupParts ,
		['prompt' => 'Выберите один вариант']);	?>
		
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'supplier')->widget(Select2::classname(), [
    'data' => $client,
    'options' => ['placeholder' => 'Выберите ...','id' =>'supplier'],	 
    'pluginOptions' => [
		'tags' => true,
        'allowClear' => true,],
		
	]);?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_shop')->textInput(['maxlength' => true]) ?>

	
	
    <div style="display:none">
    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>