<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\manual\Parts */

$this->params['breadcrumbs'][] = ['label' => 'Запчасти', 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
<div class="parts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
        'groupParts' => $groupParts,
        'client' => $client,
    ]) ?>

</div>
