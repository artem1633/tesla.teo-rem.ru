<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\manual\TypeParts */

//$this->title = 'Создание Type Parts';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Type Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-parts-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->


    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
