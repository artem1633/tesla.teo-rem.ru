<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\GroupClient */

//$this->title = 'Изменение Group Client: ' . $model->name;
$this->title = $title . $model->name;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Group Clients', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="group-client-update">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->


    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
