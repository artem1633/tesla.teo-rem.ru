<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\manual\ModelDeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Model Devices';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-device-index">
 <div class="box box-default">  
        <div class="box-body"> 
 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <!-- <?= Html::a('Create Model Device', ['create'], ['class' => 'btn btn-success']) ?>
        --><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
</div>
<br>
<br>
 <div class="box box-default">  
        <div class="box-body"> 
<?php Pjax::begin(); ?> <div style="width: 100%; overflow: scroll">    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            //'id',
            'name',
             /*[
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{update}  {link}'
			],
        ],
    ]); ?></div>
<?php Pjax::end(); ?></div>
</div>
</div>
