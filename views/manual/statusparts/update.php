<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\manual\StatusParts */

//$this->title = 'Изменение Status Parts: ' . $model->name;
$this->title = $title . $model->name;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Status Parts', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="status-parts-update">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->


    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
