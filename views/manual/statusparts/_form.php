<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\manual\StatusParts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="status-parts-form">
 <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

	<div style="display:none">
    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
	</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
