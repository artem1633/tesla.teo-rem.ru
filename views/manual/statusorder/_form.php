<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\manual\StatusOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="status-order-form">
 <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-1 center" style="margin-left: 10px;">            
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 center">
                    <?= Html::button('Обозначения',  ['value'=>Url::to('view1'), 'class'=>'btn btn-success', 'id' => 'modalButton'])?>
                </div>
                
                <div class="col-md-2 center">
                    <?= $form->field($model, 'push')->checkBox() ?>
                </div>

                <br>

                <div class="col-md-11 vcenter" style="margin-left: 20px;">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?php 
                    /*echo $form->field($model, 'tekst')->widget(CKEditor::className(),[
                        'editorOptions' => [
                         'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false                    
                                ],
                        ]);*/ 
                    ?>

                    <?= $form->field($model, 'tekst')->textarea(['rows' => 4 ]) ?>


                     <div style="display:none">
                    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

                    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
                    </div>
                </div>
        </div>
    <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
        <?php 
        Modal::begin([
            'header'=>'<h4>Обозначения</h4>',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";
        Modal::end();
        ?>