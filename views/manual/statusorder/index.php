<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\manual\StatusOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Status Orders';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-order-index">
 <div class="box box-default">  
        <div class="box-body"> 
 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <!-- <?= Html::a('Create Status Order', ['create'], ['class' => 'btn btn-success']) ?>
        --><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
</div>

 <div class="box box-default">  
        <div class="box-body"> 
<?php Pjax::begin(); ?>  <div style="width: 100%; overflow: scroll">   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            //'id',
            'name',
            'tekst:ntext',
            //'push',
            [
                'attribute'=>'push',
                'filter'=>array("1"=>"Есть","0"=>"Нет"),
                'content'=>function($data){
                    return $data->getStatus();
                },
            ],
            /* [
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{update}'
			],
            ['class' => 'yii\grid\ActionColumn',
			'template' => '{delete}'
			],
        ],
    ]); ?></div>
<?php Pjax::end(); ?></div>
</div>
</div>


