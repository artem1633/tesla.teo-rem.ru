<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\manual\Made */

//$this->title = 'Создание Made';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Mades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="made-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
