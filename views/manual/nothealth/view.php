<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\manual\Nothealth */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Nothealths', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nothealth-view">
 <div class="box box-default">  
        <div class="box-body"> 
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext',
            'date_cr',
            'date_up',
        ],
    ]) ?>

</div>
</div>
</div>
