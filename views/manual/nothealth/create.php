<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\manual\Nothealth */

//$this->title = 'Создание Nothealth';
//$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Nothealths', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nothealth-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
