<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\manual\NothealthSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Nothealths';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nothealth-index">
    <div class="box box-default">  
        <div class="box-body"> 
 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <!-- <?= Html::a('Create Nothealth', ['create'], ['class' => 'btn btn-success']) ?>
        --><?= Html::a('Добавить', ['#'], ['class' => 'btn btn-success','onClick' => '
			
			var modal = $(".modal");
			$.get("/nothealth/create", function(data) {
			modal.html(data).modal("show");
			});
			return false;
		
		']) ?>
    </p>
	
	<div class="modal fade"></div>
</div>
</div>
<br>
 <div class="box box-default">  
        <div class="box-body"> 	
 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            //'id',
            'name:ntext',
			/*[
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/

			

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{update}  {link}',
			'buttons' => [
				'update' => function ($url, $model, $key){            
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>',  ['#'],
							['onClick' => '		
								var modal = $(".modal");
								$.get("/nothealth/update?id='.$key.'", function(data) {
								modal.html(data).modal("show");
								});
								return false;
							
							']);
					
                }
            ],
			],
        ],
    ]); ?>
	
</div>
</div>
</div>