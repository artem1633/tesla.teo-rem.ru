<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
/* @var $this yii\web\View */

/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Логи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <div >
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute'=>'user_id',
                        'value' => function($data){
                            if($data->user_id != null) return $data->user->name;
                            else return null;
                        },
                    ],
                    'event_datetime',
                    'description',
                [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{delete}',
                ],
            ],
        ]); ?>
    </div>
</div>



