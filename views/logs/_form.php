<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Logs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logs-form">
	<div class="user-form box box-primary">
        <div class="box-body table-responsive">

		    <?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'user_id')->textInput() ?>

		    <?= $form->field($model, 'event_datetime')->textInput() ?>

		    <?= $form->field($model, 'event')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

		    <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		    
		</div>
	</div>
</div>
