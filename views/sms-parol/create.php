<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmsParol */

$this->title = 'Create Sms Parol';
$this->params['breadcrumbs'][] = ['label' => 'Sms Parols', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-parol-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
