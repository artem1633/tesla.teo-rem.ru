<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\ShopList;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$gridColumn = [
	'id',
	[
	'attribute' => 'client_name',				
	'label' => 'Клиент',	
	'value'=>'client.name',
	 ],
	 [
		'label' => 'Товар',	
		'content'=>function($data){
			$shopList = ShopList::find()->where(['shop_by'=>$data->id])->all();
	 },
	],
	'price',
	[
		'attribute' => 'user_name',				
		'label' => 'Создал',	
		'value'=>'user.name',
	 ],
	[
		'attribute' => 'date_cr',
		'format' => ['datetime', 'php:d.m.Y H:i:s']
	],	      
];
?>
<div class="shop-index">

<div class="box box-default">	
	<div class="box-body">

		<div class="row">
			<div class="col-md-4">			    
			   <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
			<?= Html::a('Печатать', ['shop/printlist','searchId' => $searchId,'linkTime' => $linkTime], ['class' =>'btn btn-info','target'=>"_blank"]) ?>
			<?= ExportMenu::widget([ 'dataProvider' => $dataProvider, 'columns' => $gridColumn]);?>
					
			</div>
				 
		</div>
	
	<br/>
	<div class="row">
		<div class="col-md-8">
			<div class="btn-group btn-group-sm" role="group" aria-label="...">
			<?php echo ButtonDropdown::widget([
								'label' => $labelId,
								'options' => [
									'class' => 'btn btn-primary',
									'style' => 'font-size:13px',
									],
									'dropdown' => [
									'items' => [
														[
															'label' => 'За все время',
															'url' => [''],
														],
														[
															'label' => 'Сегодня',
															'url' => ['index', 'searchId' => 'today'],
														],
														[
															'label' => 'Вчера',
															'url' =>  ['index', 'searchId' => 'yesterday'],
														],
														[
															'label' => 'С начала недели',
															'url' => ['index', 'searchId' => 'week'],
														],
														[
															'label' => 'За прошлую неделю',
															'url' => ['index', 'searchId' => 'lastWeek'],
														],
														[
															'label' => 'С начала месяца',
															'url' => ['index', 'searchId' => 'Month'],
														],
														[
															'label' => 'За прошлый месяц',
															'url' => ['index', 'searchId' => 'lastMonth'],
														],
													]
								]	
						]);?>
						
						<?= Html::input('date','',$linkTime,['style'=> 'font-size:12.5px','class' => 'btn btn-primary','onchange' =>'window.location.href = "/shop/index?searchId=dinamick&linkTime="+$(this).val();']) ?>
			
				</div>
		</div>
	</div>
	</div>
</div>

	
<div class="box box-default">	
	<div class="box-body">

		<?php Pjax::begin(); ?>    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
				'summary' => 'Итого: '.$sum,
		        'columns' => [ 
		            ['class' => 'yii\grid\SerialColumn'],
					

		            'id',
					//'store_by',
					
					[
						'attribute' => 'client_name',				
						'label' => 'Клиент',	
						'value'=>'client.name',
					 ],
					[
						'label' => 'Товар',	
						'content'=>function($data){
							$shopList = ShopList::find()->where(['shop_by'=>$data->id])->all(); 
							$names = '';
							foreach ($shopList as $value) {
								$names .= $value->parts->name . '<br>';
							}
							return $names;
					 	},
					],
		            'price',
		            //'count',

		            // 'komment',
		           //  'user_by_cr',
					 [
						'attribute' => 'user_name',				
						'label' => 'Создал',	
						'value'=>'user.name',
					 ],
		            // 'user_by_up',
		            	[
						'attribute' => 'date_cr',
					//	'filter' =>	Html::input('date'),
						'format' => ['datetime', 'php:d.m.Y H:i:s']
		            ],
		            // 'date_up',

		            ['class' => 'yii\grid\ActionColumn',
					'template' => '{view} {update}{delete} {link}',
					'buttons' => [
		                'update' => function ($url, $model, $key){ 
							if (trim($model->status) <> 'finish'){
								return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/shop/create?id='.$key);
							}	
		                },
		                'delete' => function ($url, $model, $key){ 
							if (trim($model->status) <> 'finish'){
								return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
								['delete','id'=>$key],
								[
									'title'=>'Удалить',
									'aria-label'=>'Удалить',
									'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
									'data-method'=>'post',
								]);
							}	
		                }
		            ],
					],
		        ],
		    ]); ?>
		<?php Pjax::end(); ?>

		</div>
	</div>
</div>
