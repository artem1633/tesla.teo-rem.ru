

<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>

<?=$aboutCompany->name ?>
&quot;

<o:p></o:p></span></p>


<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>
<?=$aboutCompany->adress ?> 
<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>
тел.:
<?=$aboutCompany->phone ?> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>Режим
работы: <span class=SpellE>Пн-Пт</span>: 9-18 часов, <span class=SpellE>Сб-Вс</span>:
выходной</span><span style='font-size:6.5pt;font-family:"Tahoma","sans-serif";
color:#333333;background:#ECF0F5'><br style='mso-special-character:line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]></span><span style='font-size:6.5pt;font-family:"Tahoma","sans-serif";
color:#333333;background:#F9F9F9'><o:p></o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'><span style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
color:black;background:white'><o:p>&nbsp;</o:p></span></b></p>


<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'><span style='font-size:12.0pt;line-height:115%;font-family:"Arial","sans-serif";
color:black;background:white'>Товарный чек документу № <?=$shop->id ?> от  <?=date('Y.m.d H:i', time()); ?><o:p></o:p></span></b></p>



<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='margin-left:.25pt;border-collapse:collapse;border:none'>
 
<tr style='height:19.85pt'>
  <td width=26 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>№</span></p>
  </td>
  <td width=365 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Наименование </span></p>
  </td>
  <td width=57 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Ед.</span></p>
  </td>
  <td width=76 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Кол-во</span></p>
  </td>
  <td width=57 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Цена</span></p>
  </td>
  <td width=119 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Сумма</span></p>
  </td>  
  <td width=119 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Гарантия
	</span></p>
  </td>
  
 </tr>
 
 <?
 $table = '';
 $number = 0;
 $summa = 0;
 foreach($shopList as $shopL)
 {
	 $number++;
	 
	 $summa += $shopL->count * $shopL->price;

	$table .= "
	
	 <tr >
		<td width=26 >
		<p class=MsoNormal align=center ><span lang=EN-US
		style='font-size:9.0pt'>".$number."</span></p>
		</td>
 
		<td width=365 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>".$shopL->parts->name."</span></p>
		</td>

		<td width=57 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>шт.</span></p>
		</td>
		
	  
		<td width=76 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$shopL->count."</span></p>
		</td>	
		
		<td width=57 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$shopL->price."</span></p>
		</td>

		<td width=119 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$shopL->price * $shopL->count."</span></p>
		</td>

		<td width=119 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>30 дней</span></p>
		</td>

	 </tr>
		
	";
 
 
 
 
 
 
 } 
 echo $table;
 
 
 ?>
 
 <tr >
  <td width=580 colspan=5 style='width:435.25pt;border:none;border-right:solid windowtext 1.0pt;
  padding:0cm 0cm 0cm 0cm;height:17.0pt'>
  <p class=MsoNormal align=right style='margin-right:5.65pt;text-align:right'><b><span
  style='font-size:9.0pt'><span
  style='color:windowtext;text-decoration:none'>Итого</span></a>:</span></b></p>
  
  </td>
  <td width=119 >
  
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>
  
  <?
				
  echo $summa;
  
  ?>
  
  </span></p>
  </td>
  
  
 </tr>
 
 
</table>

<p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
"Arial","sans-serif";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:7.0pt;line-height:115%;font-family:
"Arial","sans-serif";color:black;background:white'>Покупатель проинформирован о
всех свойствах приобретаемых товаров в полном объеме. Товар осмотрен и получен.
Покупатель претензий не имеет.<o:p></o:p></span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none;mso-border-alt:solid black .5pt;
 mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
  <td width=685 valign=top style='width:513.95pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  style='font-size:8.0pt;font-family:"Arial","sans-serif";color:black;
  background:white'><o:p>&nbsp;</o:p></span></b></p>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  style='font-size:12.0pt;font-family:"Arial","sans-serif";color:black;
  background:white'>УСЛОВИЯ ГАРАНТИЙНОГО ОБСЛУЖИВАНИЯ<o:p></o:p></span></b></p>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b style='mso-bidi-font-weight:normal'><span
  style='font-size:8.0pt;font-family:"Arial","sans-serif";color:black;
  background:white'><o:p>&nbsp;</o:p></span></b></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>1. Гарантийное
  обслуживание предоставляется при соблюдении следующих условий:<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>- наличие правильно
  (без исправлений) заполненного гарантийного талона с указанием кода изделия и<b>&nbsp;</b><span
  style='mso-bidi-font-weight:bold'>печати торгующей организации</span>;<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>-<span
  style='mso-spacerun:yes'>  </span>наличие документа подтверждающего дату
  покупки данного изделия (<span style='mso-bidi-font-weight:bold'>товарный чек</span>);<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>2. Не подлежат
  гарантийному ремонту изделия и запасные части с дефектами, возникшими
  вследствие:<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>- несоблюдения условий
  эксплуатации, неправильной установки, механических повреждений или ошибочных
  действий владельца;<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>- стихийных бедствий
  (молния, пожар, наводнение и т.п.), а также других причин, находящихся вне
  контроля сервисного центра и изготовителя;<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>- попадания внутрь
  изделия посторонних предметов, жидкостей, насекомых, и <span class=SpellE>т.п</span>;<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>- ремонта или внесения
  конструктивных изменений неуполномоченными лицами;<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>3. Гарантия не
  включает в себя периодическое обслуживание, установку, настройку изделия на
  дому у владельца.<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>5. Сервисный центр
  оставляет за собой право на отказ в гарантийном обслуживании в случае
  непредставления вышеуказанных документов, если информация в них будет
  неполной, неразборчивой, противоречивой.<o:p></o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'><o:p>&nbsp;</o:p></span></p>
  <p class=MsoNormal ><span style='font-size:8.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'>Проданный товар
  подлежит возврату (обмену) только в 14-ти <span class=SpellE>дневный</span>
  срок <span class=SpellE>c</span> момента продажи, при наличии фирменной
  (заводской) упаковки.</span><span style='font-size:9.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:"Times New Roman";color:black'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
color:black;background:white'><br>
С условиями гарантии ознакомлен, товар получил полностью, оборудование в моем
присутствии проверено, внешних механических повреждений нет, комплект
документов, инструкцию на русском языке получил, претензий не имею.<o:p></o:p></span></b></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
color:black;background:white'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:8.0pt;line-height:115%;font-family:"Arial","sans-serif";
color:black;background:white'>Подпись клиента: ____________________
(________________)<span style='mso-spacerun:yes'>     </span>Подпись
сотрудника: __________________(<?=$shop->user->name ?>)</span></b><b style='mso-bidi-font-weight:
normal'><span style='font-size:8.0pt;line-height:115%'><o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
"Arial","sans-serif";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
"Arial","sans-serif";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

</div>
