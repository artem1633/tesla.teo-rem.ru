<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */

$this->title = $model->story_by;
$this->params['breadcrumbs'][] = ['label' => 'Магазин', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-view">
        <div class="box box-default">   
        <div class="box-body">
    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Печатать', ['print', 'id' => $model->id], ['class' => 'btn btn-primary','target'=>"_blank"]) ?> 
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
 //           'store_by',
            'client_by',
            'price',
 //           'count',

            'komment',
            'user_by_cr',
            'user_by_up',
			[
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
        ],
    ]) ?>
	
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

           // 'id',
			[
				'attribute' => 'parts_by',				
				'label' => 'Товар',	
				'value'=>'parts.name',
			 ],
            'price',
            'count',
           // 'shop_by',

            ['class' => 'yii\grid\ActionColumn',
			'template' => ' {link}',
			],
        ],
    ]); ?>

</div>
</div>
</div>
