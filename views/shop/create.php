<?php

use yii\helpers\Html;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */

//$this->title = 'Создание Shop';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Shops', 'url' => ['index']]; 
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*echo "user_by_cr_v = ".$user_by_cr_v;
echo "<br>user_by_up_v = ".$user_by_up_v;
echo "<br>date_cr_v = ".$date_cr_v;
echo "<br>date_up_v = ".$date_up_v;
die;*/

?>
<div class="shop-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'user_by_cr_v' => $user_by_cr_v,
		'user_by_up_v' => $user_by_up_v,
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
        'dataProvider' => $dataProvider,
        'client' => $client, 
        'id' => $id, 
        'sum' => $sum, 
    ]) ?>
	
	
	
	

</div>
