<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal fade"></div>


<div class="shop-form">
		<div class="box box-default">	
		<div class="box-body">

    <?php $form = ActiveForm::begin(['action' => '/shop/update?id='.$id]); ?>

	
	
	
	
<div style="display:none">  
	
	<?= $form->field($model, 'price')->textInput(['value' => $sum]) ?>
	
	<?= $form->field($model, 'status')->textInput(['value' => "finish"]) ?>
	
    <?= $form->field($model, 'user_by_cr')->textInput(['value'=>$user_by_cr_v]) ?>

    <?= $form->field($model, 'user_by_up')->textInput(['value'=>$user_by_up_v]) ?>

     
    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
	
</div>


	<p>
      
		<div class="form-group">
		
			<?= Html::a('Добавить', ['#'], ['class' => 'btn btn-success','onClick' => '
			
			var modal = $(".modal");
			$.get("/shop-list/create?id='.$id.'", function(data) {
			modal.html(data).modal("show");
			});
			return false;
		
		']) ?>
		
			<?php if ($dataProvider->getCount() > 0){echo Html::submitButton($model->isNewRecord ? 'Создать ' : 'Создать документ', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);} ?>
		</div>
		
		<div class="row">
				<div class="col-md-3">
					<div class="input-group date">
						<?= $form->field($model, 'client_by')
						->dropDownList($client,
						['prompt' => 'Выберите один вариант',
						'onChange'=>
						'$.get("clientchange",{"id":' . $id . ', "client":$(this).val()});											
						']);	?>
					</div>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'komment')->textInput() ?>
			
			</div>
		</div>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

           // 'id',
			[
				'attribute' => 'parts_by',				
				'label' => 'Товар',	
				'value'=>'parts.name',
			 ],
            'price',
            'count',
           // 'shop_by',

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{delete} {link}',
			'buttons' => [
                'delete' => function ($url, $model, $key){					
                   return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['shop-list/delete','id' => $key]);
                }
            ],
			],
        ],
    ]); ?>
	
	<h3><?php if(isset($id)){ echo Html::encode('Итого : '.$sum  . "   (Сумма  с учетом скидки и предоплаты)"); }?></h3>
	
	<br/>
	<br/>
	
	
	

	

    <?php ActiveForm::end(); ?>

</div>

</div>
</div>