<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>TEO - CRM</b>	</a>
    </div>
        <?php if(Yii::$app->session->hasFlash('register_success')): ?>
            <p>
                <div class="alert alert-success show m-b-0">
                    <span class="close" data-dismiss="alert">×</span>
                    <strong>Успех!</strong>
                    <?=Yii::$app->session->getFlash('register_success')?>
                </div>
            </p>
        <?php endif; ?>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Введите данные для входа</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="col-xs-12">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block btn-flat',  'style' => 'width:100%', 'name' => 'login-button']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

        
        <!-- /.social-auth-links -->

       

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
