<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Store */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-form">
 <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adress')->textInput(['maxlength' => true]) ?>

    <div style="display: none;">

        <?= $form->field($model, 'status')->textInput(['value'=>1]) ?>

        <?= $form->field($model, 'user_by_cr')->textInput(['value'=>$user_by_cr_v]) ?>

        <?= $form->field($model, 'user_by_up')->textInput(['value'=>$user_by_cr_v]) ?>

        <?= $form->field($model, 'date_cr')->textInput() ?>

        <?= $form->field($model, 'date_up')->textInput() ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div>
