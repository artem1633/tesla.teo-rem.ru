<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$gridColumn = [
    'id',
     'name',
     'adress',
     [
         'attribute' => 'user_by_cr',
         'value' => 'user.name',
     ], 
];
?>
<div class="store-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="box box-default">   
    <div class="box-body">
        
       <!-- <?= Html::a('Create Store', ['create'], ['class' => 'btn btn-success']) ?>
        --><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-print']).' Печатать', ['store/printlist'], ['class' =>'btn btn-default','target'=>"_blank"]) ?>
        <?php 
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumn
                ]);
            ?>
    
    </div>
</div>
<div class="box box-default">   
    <div class="box-body">
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            'id',
            'name',
            'adress',
            //'status',
            //'user_by_cr',
            [
                'attribute' => 'user_by_cr',
                'value' => 'user.name',
            ],
            // 'user_by_up',
            // 'date_cr',
            // 'date_up',

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{update} {delete} {link}'
			],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</div>
</div>
