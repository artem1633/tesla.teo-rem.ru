<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\Store */

//$this->title = 'Создание Store';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'user_by_cr_v' => $user_by_cr_v,
    ]) ?>

</div>
