<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Nastroyka */

/*$this->title = 'Create Nastroyka';
$this->params['breadcrumbs'][] = ['label' => 'Nastroykas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="nastroyka-create">

    <?= $this->render('change_sms_form', [
        'model' => $model,
    ]) ?>

</div>
