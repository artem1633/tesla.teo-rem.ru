<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Nastroyka */

$this->title = 'Create Nastroyka';
$this->params['breadcrumbs'][] = ['label' => 'Nastroykas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nastroyka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
