<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Nastroyka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nastroyka-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stariy_sms_parol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sms_parol_retry')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
