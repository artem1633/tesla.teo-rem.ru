<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Nastroyka */

$this->title = 'Настройка';
/*$this->params['breadcrumbs'][] = ['label' => 'Нвстройка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="nastroyka-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'password',
            'sms_parol',
        ],
    ]) ?>

</div>
