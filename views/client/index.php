<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$gridColumn = [
   'id',
    'name',
    
    [
    'attribute'=>'type',
    'filter'=>array("person"=>"Физ. лицо","company"=>"Юр. лицо"),
    'content'=>function($data){
         if ($data->type == 'person'){
            return 'Физ. лицо'; 
         }
          if ($data->type == 'company'){
            return 'Юр. лицо'; 
         }
     },
    ],
    'iin',
    'ogrn',
    'expense',          
     [
        'attribute' => 'group_name',                
        'label' => 'Группа',    
        'value'=>'group.name',
     ],
    //'adress',
    'phone',  
];
?>
<div class="client-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
  <div class="box box-default">  
        <div class="box-body"> 
        
           <!-- <?= Html::a('Create Client', ['create'], ['class' => 'btn btn-success']) ?>
            --><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-print']).' Печатать', ['client/printlist'], ['class' =>'btn btn-default','target'=>"_blank"]) ?>
            <?php 
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumn
                ]);
            ?>
        
    </div>
</div>
  <div class="box box-default">  
        <div class="box-body"> 
        <?php Pjax::begin(); ?>    <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
        			

                    'id',
                    'name',
                    
        			[
                    'attribute'=>'type',
                    'filter'=>array("person"=>"Физ. лицо","company"=>"Юр. лицо"),
        			'content'=>function($data){
        				 if ($data->type == 'person'){
        					return 'Физ. лицо'; 
        				 }
        				  if ($data->type == 'company'){
        					return 'Юр. лицо'; 
        				 }
        			 },
        			],
                    'iin',
                    'ogrn',
                    'expense',          
        			 [
        				'attribute' => 'group_name',				
        				'label' => 'Группа',	
        				'value'=>'group.name',
        			 ],
                    //'adress',
                    'phone',
                    //'email:email',
                    //'discount_order',
                    //'discount_parts',
                    // 'user_by_cr',
                    // 'user_by_up',
                    // 'date_cr',
                    // 'date_up',

                    ['class' => 'yii\grid\ActionColumn',
        			'template' =>' {view}{update} {delete} {link}'
        			],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
            
        </div>
    </div>
</div>


