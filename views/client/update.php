<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

//$this->title = 'Изменение Client: ' . $model->name;
$this->title = $title . $model->name;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="client-update">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'user_by_cr_v' => $user_by_cr_v,
		'user_by_up_v' => $user_by_up_v,
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
        'groupClient' => $groupClient,
    ]) ?>

</div>
