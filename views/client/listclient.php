
<div class=Section1>


<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='margin-left:.25pt;border-collapse:collapse;border:none'>
 
 
 
 <tr style='height:19.85pt'>
  <td width=26 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>№</span></p>
  </td>
  <td width=26 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Код</span></p>
  </td>
  <td width=90 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>ФИО</span></p>
  </td>
  <td width=60 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Тип</span></p>
  </td>
  <td width=60 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>ИИН</span></p>
  </td>
  <td width=60 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>ОГРН</span></p>
  </td>
  <td width=60>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>р/с</span></p>
  </td>
  <td width=60>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Группа</span></p>
  </td>
  <td width=180>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Адрес</span></p>
  </td>
  <td width=120>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Телефон</span></p>
  </td>
  <td width=100>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Email</span></p>
  </td>
   <td width=60>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>На заказ(%)</span></p>
  </td>
  <td width=60>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>На работу(%)</span></p>
  </td>
  
 </tr>
 
 
 
 <?php
 $table = '';
 $number = 0;

 foreach($dataProvider as $inf)
 {
	$number++;	
	if($inf->type == 'person')$tip = 'Физ. лицо';
	else $tip = 'Юр. лицо';
	 
	$table .= "
	
	 <tr >
		<td width=26 >
		<p class=MsoNormal align=center ><span lang=EN-US
		style='font-size:9.0pt'>".$number."</span></p>
		</td>
 
		<td width=26 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>".$inf->id."</span></p>
		</td>

		<td width=90 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>".$inf->name."</span></p>
		</td>
		
	  
		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$tip."</span></p>
		</td>	
		
		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->iin."</span></p>
		</td>

		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->ogrn."</span></p>
		</td>

		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->expense."</span></p>
		</td>

		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->group->name."</span></p>
		</td>

		<td width=179 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->adress."</span></p>
		</td>

		<td width=120 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->phone."</span></p>
		</td>

		<td width=100 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->email."</span></p>
		</td>

		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->discount_order."</span></p>
		</td>

		<td width=60 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$inf->discount_parts."</span></p>
		</td>

	 </tr>
		
	";
 
 } 
 echo $table; 
 
 ?>
 
</table>


</div>
