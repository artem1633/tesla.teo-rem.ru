<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\Client */

//$this->title = 'Создание Client';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'user_by_cr_v' => $user_by_cr_v,
		'user_by_up_v' => $user_by_up_v,
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
        'groupClient' => $groupClient,
    ]) ?>

</div>
