<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AktRabot */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Akt Rabots', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akt-rabot-view">

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'top:html',
            'shablon_name',
            'klient',
            'device',
            'repair',
            'discount',
            'prepay',
            'warranty_period',
            'client_signature',
            'employee_signature',
        ],
    ]) ?>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       
    </p>

</div>
