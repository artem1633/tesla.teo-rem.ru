<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AktRabotSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akt-rabot-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'top') ?>

    <?= $form->field($model, 'shablon_name') ?>

    <?= $form->field($model, 'klient') ?>

    <?= $form->field($model, 'device') ?>

    <?php // echo $form->field($model, 'repair') ?>

    <?php // echo $form->field($model, 'discount') ?>

    <?php // echo $form->field($model, 'prepay') ?>

    <?php // echo $form->field($model, 'warranty_period') ?>

    <?php // echo $form->field($model, 'client_signature') ?>

    <?php // echo $form->field($model, 'employee_signature') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
