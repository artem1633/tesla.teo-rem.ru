<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AktRabot */

/*$this->title = 'Update Akt Rabot: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Akt Rabots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';*/
?>
<div class="akt-rabot-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
