<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AktRabotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Akt Rabots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akt-rabot-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Akt Rabot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'top:ntext',
            'shablon_name',
            'klient',
            'device',
            // 'repair',
            // 'discount',
            // 'prepay',
            // 'warranty_period',
            // 'client_signature',
            // 'employee_signature',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
