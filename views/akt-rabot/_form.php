<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\AktRabot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akt-rabot-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="col-md-12">
        <?php 
        echo $form->field($model, 'top')->widget(CKEditor::className(),[
            'editorOptions' => [
             'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false                    
                    ],
            ]); 
        ?>
    </div>

            <div class="col-md-6">

    <?= $form->field($model, 'shablon_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'klient')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'device')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'repair')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

</div>
    <div class="row">
         <div class="col-md-6">
    <?= $form->field($model, 'prepay')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warranty_period')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_signature')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'employee_signature')->textInput(['maxlength' => true]) ?>
    <br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
      </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
