<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SmsObject */

$this->title = 'Create Sms Object';
$this->params['breadcrumbs'][] = ['label' => 'Sms Objects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-object-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
