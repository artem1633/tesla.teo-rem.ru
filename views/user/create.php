<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\User */

//$this->title = 'Создание User';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->


    <?= $this->render('_form', [
		'user_by_cr_v' => $user_by_cr_v,
		'user_by_up_v' => $user_by_up_v,
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
        'model' => $model,
    ]) ?>

</div>
