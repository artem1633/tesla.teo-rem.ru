<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use app\models\User;

$gridColumn = [
    'name:ntext',
    'phone',
    [
      'attribute'=>'permission',
      'filter'=>array("admin"=>"Администратор","maneger"=>"Менеджер","master"=>"Мастер"),
      'content'=>function($data){
        if ($data->permission == 'admin'){
          return 'Администратор'; 
        }
        if ($data->permission == 'maneger'){
          return 'Менеджер'; 
        }
        if ($data->permission == 'master'){
        return 'Мастер'; 
        }
      },
    ],       
    'percent_sale',
    'percent_service',
    'percent_finish',
    [
     'attribute'=>'status',
     'filter'=>array("1"=>"Доступ","0"=>"Без доступа"),
     'format'=>'boolean',
    ],
];
?>
<div class="user-index">
  <div class="box box-default">  
    <div class="box-body"> 
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?php 
          echo ExportMenu::widget([
              'dataProvider' => $dataProvider,
              'columns' => $gridColumn
          ]);
        ?>           
    </div>
  </div>
  <div class="box box-default">  
    <div class="box-body">
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
    		'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'booleanFormat' => [ 'Без доступа','Доступ'],
    		],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'name:ntext',
            'phone',             
      			[
              'attribute'=>'permission',
              'filter'=>array("admin"=>"Администратор","maneger"=>"Менеджер","master"=>"Мастер"),
      			  'content'=>function($data){
                if ($data->permission == 'admin'){
                  return 'Администратор'; 
                }
                if ($data->permission == 'maneger'){
                  return 'Менеджер'; 
                }
                if ($data->permission == 'master'){
                return 'Мастер'; 
                }
              },
      			],			 
             'percent_sale',
             'percent_service',
             'percent_finish',
      			 [
      				'attribute'=>'status',
      				'filter'=>array("1"=>"Доступ","0"=>"Без доступа"),
      				'format'=>'boolean',
      			 ],

            [
              'class' => 'yii\grid\ActionColumn',
        			'template' => '{leadView} {leadUpdate} {leadDelete}',
              'buttons'  => [

                  'leadView' => function ($url, $model) {
                      $url = Url::to(['/user/view', 'id' => $model->id]);
                      return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                  },
                  'leadUpdate' => function ($url, $model) {
                      $url = Url::to(['/user/update', 'id' => $model->id]);
                      return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                  },
                  'leadDelete' => function ($url, $model) {
                    if($model->permission != User::USER_TYPE_ADMIN && $model->permission != User::USER_TYPE_SUPER_ADMIN){
                      $url = Url::to(['/user/delete', 'id' => $model->id]);
                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                          'role'=>'modal-remote','title'=>'', 
                                'data-confirm'=>false, 'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-toggle'=>'tooltip',
                                'data-confirm-title'=>'Подтвердите действие',
                                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                      ]);
                    }
                  },
              ]
        		],
        ],
    ]); ?>
</div>
</div>
</div>
