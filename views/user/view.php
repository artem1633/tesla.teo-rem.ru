<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $title . $model->name;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
 <div class="box box-default">  
        <div class="box-body"> 
    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       
    </p>

	<?
		
		
	?>
    <?= DetailView::widget([
        'model' => $model,	
        'attributes' => [
            'id',
            'username',
            'name:ntext',
            'phone',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
			'permission',
            'percent_sale',
            'percent_service',
            'percent_finish',
            'status',
            'user_by_cr',
            'user_by_up',
            [
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
        ],
    ]) ?>

</div>
</div>
</div>
