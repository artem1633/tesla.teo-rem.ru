<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
 <div class="box box-default">  
        <div class="box-body"> 
    <?php $form = ActiveForm::begin(); ?>

    
	<div class="row">
        <div class="col-md-4">
		    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-4">
		    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-4">
        	<?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>
        </div>
	</div>

	<div class="row">
		<div class="col-md-3">
        	<?= $form->field($model, 'move')->dropDownList(
        		[
					'1'=>'Есть',
					'0'=>'Нет'
				] ,
				['prompt' => 'Выберите один вариант']);
			?>
        </div>
        <div class="col-md-3">
        	<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        	<?= $form->field($model, 'permission')->dropDownList(
        		[
					'admin'=>'Администратор',
					'maneger'=>'Менеджер',
					'master'=>'Мастер'
				] ,
				['prompt' => 'Выберите один вариант']);	?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
        	<?= $form->field($model, 'status')->dropDownList(
        		[
					'1'=>'Доступ',
					'0'=>'Без доступа'
				] ,
				['prompt' => 'Выберите один вариант']);
			?>
        </div>
        <div class="col-md-3">
        	<?= $form->field($model, 'percent_sale')->textInput() ?>
        </div>
        <div class="col-md-3">
        	<?= $form->field($model, 'percent_service')->textInput() ?>
        </div>
        <div class="col-md-3">
        	<?= $form->field($model, 'percent_finish')->textInput() ?>
        </div>
    </div>	
	
	<div style="display:none">
    <?= $form->field($model, 'user_by_cr')->textInput(['value'=>$user_by_cr_v]) ?>

    <?= $form->field($model, 'user_by_up')->textInput(['value'=>$user_by_up_v]) ?>

     
    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>

