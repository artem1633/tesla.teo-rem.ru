<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Storage */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="storage-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
        <div class="col-md-7">
	    	<?= $form->field($available, 'tovar_name')->textInput(['value' => $available->parts->name, 'maxlength' => true, 'disabled' => true])->label('Названия товара') ?>
	    	<?php // echo Html::activeLabel($model,'name'); ?>
	    </div>
        <div class="col-md-5">
	    	<?= $form->field($available, 'move_count')->textInput(['type' => 'number', 'min' => 1, 'value' => 1 ,'max' => $available->count]) ?>
	    	
	    </div>
	     <div style="display: none;">
	    	<?= $form->field($available, 'parts_by')->textInput(['maxlength' => true,]) ?>
	    	
	    </div>
		
	</div>

	<div class="row">
	    <div class="col-md-12">
		    <?= $form->field($available, 'storage_id')->dropDownList(
                $model->Masters($available->storage_id),
                [
                    //'prompt' => 'Выберите Мастера',
                ]
                ) ?> 
		</div>
		
	</div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
