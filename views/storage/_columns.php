<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'content' => function ($data) {
            return $data->user->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_main',
        'content' => function ($data) {
            if($data->is_main == 0) return ('<span style="color:blue;font-weight:bold;">Склад мастера</span>');
            if($data->is_main == 1) return ('<span style="color:red;font-weight:bold;">Основной склад</span>');
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        //'template' => '{update}{delete}',
        'template' => '{importdetail} ',
        'dropdown' => false,
        'vAlign'=>'middle',
        'buttons' => [
               'importdetail' => function ($url, $model) {                
                if($model->is_main != 1){
                    return 
                    Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', 
                        ['/storage/update', 'id' => $model->id], [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ]).
                    Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['/storage/delete','id'=>$model->id], ['role'=>'modal-remote', 'title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                    ]);
                }
            },
            ],
    ],

];   