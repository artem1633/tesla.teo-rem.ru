<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список складов';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

    <!-- <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Поиск</h3>
    
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        /.box-header
        <div class="box-body">
    
                <?php  //echo $this->render('_search', ['model' => $searchModel, 'post'=>$post, 'to' => $to, 'from' => $from, 'search_surname' => $search_surname, 'search_name' => $search_name, 'search_middle_name' => $search_middle_name]); ?>
        /.box-body
    
        /.footer
    </div>
    
    </div> -->
<div class="box box-default">   
        <div class="box-body">
<div class="users-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('Добавить', ['create'],
                    ['role'=>'modal-remote'/*,'data-pjax'=>0*/,'title'=> 'Создать','class'=>'btn btn-success']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                    '{toggleData}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'headingOptions' => ['style' => 'background-color: #ecf0f5; color:black;'],
                //'heading' => '<i class="glyphicon glyphicon-list"></i> Users listing',
                //'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
</div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size"=>"modal-lg",
    //"size" => "modal-wide",
    "options" => [
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>