<?php

use app\models\Storage;
use app\models\manual\Parts;

//$this->title = 'История перемещений';
?>
<!-- <div class="box box-default ">
    <div class="box-header with-border">
        <h3 class="box-title" data-widget="collapse">Поиск</h3>            
    </div>        
    <div class="box-body">
        <?php //  echo $this->render('_search', ['model' => $model, 'post'=>$post]); ?>        
    </div>
</div> -->

<?php  //echo $this->render('_search', ['model' => $model, 'post'=>$post]); ?> 
<div class="row">
    <div class="box box-default">   
        <div class="box-body">
            <div class="col-md-12" style="margin-left: 5px;">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th>Дата/Время</th>
                        <th>Тип</th>
                        <th>Товар</th>
                        <th>От склада</th>
                        <th>На склад</th>
                        <th>Старое значение</th>
                        <th>Изменение</th>
                    </tr>
                    <?php 
                    foreach ($result as $change) {
                        if($change->table == 'resource') $tip = 'Остатки';
                        if($change->table == 'available1') $tip = 'Ожидает поступления';
                        if($change->table == 'available2') $tip = 'Оприходовано';
                        $storage_from = Storage::find()->where(['id' => $change->storage_form])->one();
                        $storage_to = Storage::find()->where(['id' => $change->storage_to])->one();
                        $tovar = Parts::find()->where(['id' => $change->part_id])->one();
                    ?>
                        <tr>
                            <td><?=\Yii::$app->formatter->asDate($change->data, 'php:H:i, d.m.Y')?></td>
                            <td><?=$tip?></td>
                            <td><?=$tovar->name?></td>
                            <td><?=$storage_from->name?></td>
                            <td><?=$storage_from->name?></td>
                            <td><?=$change->old_count?></td>
                            <td><?=$change->sending_count?></td>
                        </tr>
                    <?php } ?>
                </table>
                
            </div>
        </div>
    </div>
</div>