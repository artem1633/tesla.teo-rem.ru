<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Storage */
?>
<div class="storage-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            //'user_id',
            [
            	'attribute' => 'user_id',
            	'value' => function ($data) {
                    return $data->user->name;
                },
            ],
            //'is_main',
            [
            	'attribute' => 'is_main',
            	'value' => function ($data) {
                   if($data->is_main == 0) return 'Склад мастера';
            	   if($data->is_main == 1) return 'Основной склад';
                },
            ],
        ],
    ]) ?>

</div>
