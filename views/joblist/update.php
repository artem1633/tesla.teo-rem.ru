<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobList */

//$this->title = 'Изменение Job List: ' . $model->id;
$this->title = 'Изменение';
//$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Job Lists', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="job-list-update">


    <?= $this->render('update_form', [
        'model' => $model,
        'order_id' => $order_id,
    ]) ?>

</div>
