<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\manual\Service;
use app\models\manual\Parts;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $model app\models\JobList */
/* @var $form yii\widgets\ActiveForm */
$order =  Order::findOne($order_id);
/*echo "<pre>";
print_r($order);
die;*/
?>

<div class="modal-dialog modal-small">
	<div class="modal-content">
		<div class="nothealth-form">
				<div class="modal-body">

						<?php $form = ActiveForm::begin(); ?>


						<?php
						
						if ($model->type == "service"){
						
						echo $form->field($model, 'job_by')->widget(Select2::classname(), [
						'data' => ArrayHelper::map(Service::find()->all(), 'id', 'name'),
						'options' => ['placeholder' => 'Выберите ...'],
						 'pluginEvents' => [
									"change" => "function() 
									{
										$.get('getprice',
										{'id':$(this).val(),'job':'service'},
										function(data){ $('#price').val(data);}     
										);						
									}",
								],
						'pluginOptions' => [
							'allowClear' => true,],]);
						}else{
							
							echo $form->field($model, 'job_by')->widget(Select2::classname(), [
							'data' => ArrayHelper::map(Parts::find()->where(['group_by'=>$order->type_device_by])->all(), 'id', 'name'),
							'options' => ['placeholder' => 'Выберите ...'],
							 'pluginEvents' => [
										"change" => "function() 
										{
											$.get('getprice',
											{'id':$(this).val(),'job':'parts'},
											function(data){ $('#price').val(data);}     
											);						
										}",
									],
							'pluginOptions' => [
							'allowClear' => true,],]); 
							
						}
						
						
						
						?>
						

						<?= $form->field($model, 'count')->textInput() ?>

						<?= $form->field($model, 'price')->textInput(['id'=>'price']) ?>


						<div style="display:none">.
						
						<?= $form->field($model, 'type')->textInput(['value'=>$job]) ?>
						
						<?= $form->field($model, 'order_by')->textInput(['value'=>$id]) ?>

						<?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

						<?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
						</div>
						
						<div class="form-group">
							<?= Html::a('Закрыть',['#'],['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
							<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>


				</div>
		</div>
	</div>
</div>
