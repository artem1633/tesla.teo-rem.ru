﻿
<div class=Section1>



<p class=MsoNormal align=center style='text-align:center'>

<u><span
style='font-size:12.0pt;font-family:"Arial","sans-serif"'>Гарантийный талон к заказу <b style='mso-bidi-font-weight:
normal'>№ </b></u><b style='mso-bidi-font-weight:normal'><u>
<?=$order->id ?>

</u></b><b
style='mso-bidi-font-weight:normal'><u></u></b><i
style='mso-bidi-font-style:normal'> от <?=date('Y.m.d H:i', time()); ?><o:p></o:p></span></i>

</p> 


<p class=MsoNormal>&nbsp;</p>




<p class=MsoNormal style='margin-top:3.85pt;margin-right:250.95pt;margin-bottom: 
0cm;margin-left:13.35pt;margin-bottom:.0001pt;line-height:101%'>

<span style='font-size:9.0pt;mso-bidi-font-size:11.0pt;line-height:101%;mso-ansi-language:
RU'>

<?=$aboutCompany->name ?>


</span>


<p style='margin-top:3.85pt;margin-right:250.95pt;margin-bottom:
0cm;margin-left:13.35pt;margin-bottom:.0001pt;line-height:101%'>
<span style='font-size:9.0pt;mso-bidi-font-size:11.0pt;line-height:101%;mso-ansi-language:
RU'>

<?=$aboutCompany->adress ?>

</span>
</p>

<p class=MsoNormal style='margin-top:3.85pt;margin-right:250.95pt;margin-bottom:
0cm;margin-left:13.35pt;margin-bottom:.0001pt;line-height:101%'>
<span style='font-size:9.0pt;mso-bidi-font-size:11.0pt;line-height:101%;mso-ansi-language:
RU'>

тел.: 
<?=$aboutCompany->phone ?>

</span>
</p>


<p class=MsoNormal style='margin-top:3.85pt;margin-right:250.95pt;margin-bottom:
0cm;margin-left:13.35pt;margin-bottom:.0001pt;line-height:101%'>
<span style='font-size:9.0pt;mso-bidi-font-size:11.0pt;line-height:101%;mso-ansi-language:
RU'>

режим работы пн­-пт с 9­-18 часов сб­-вс с 9­-16 часов

</span>
</p>



</p>




<br/>
<br/>


<p class=MsoNormal style='margin-top:3.85pt;margin-right:250.95pt;margin-bottom:
0cm;margin-left:13.35pt;margin-bottom:.0001pt;line-height:101%'>

<b
style='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;mso-bidi-font-size:
11.0pt;line-height:101%;mso-ansi-language:RU'>Клиент: </span></b>

<span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
11.0pt;line-height:101%;mso-ansi-language:RU'>

 <?=$order->client->name ?>,  <?=$order->client->phone ?>  
<br/>
<b
style='mso-bidi-font-weight:normal'>Устройство:
</b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
11.0pt;line-height:101%;mso-ansi-language:RU'>фыв</span><o:p></o:p></span>

<br/>
<b
style='mso-bidi-font-weight:normal'>Не исправность:
</b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
11.0pt;line-height:101%;mso-ansi-language:RU'><?=$order->nothealth->name ?> </span><o:p></o:p></span>

<br/>
<b
style='mso-bidi-font-weight:normal'>Скидка: 
</b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
11.0pt;line-height:101%;mso-ansi-language:RU'><? echo $order->discount_order ?> </span><o:p></o:p></span>

<br/>
<b
style='mso-bidi-font-weight:normal'>Предоплата: 
</b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
11.0pt;line-height:101%;mso-ansi-language:RU'><?=$order->prepay?> </span><o:p></o:p></span>

				
				
</p>










<p class=MsoNormal style='margin-bottom:2.0pt'>&nbsp;</p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='margin-left:.25pt;border-collapse:collapse;border:none'>
 
 
 
 
 
 
 
 
 <tr style='height:19.85pt'>
  <td width=26 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>№</span></p>
  </td>
  <td width=365 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Наименование работы (услуги)</span></p>
  </td>
  <td width=57 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Ед. изм.</span></p>
  </td>
  <td width=76 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Количество</span></p>
  </td>
  <td width=57 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Цена</span></p>
  </td>
  <td width=119 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Сумма</span></p>
  </td>
  
 </tr>
 
 
 
 
 <?
 $table = '';
 $number = 0;
 $summa = 0;
 foreach($jobList as $job)
 {
	 $number++;
	 
	 $summa += $job->count * $job->price;

	$table .= "
	
	 <tr >
		<td width=26 >
		<p class=MsoNormal align=center ><span lang=EN-US
		style='font-size:9.0pt'>".$number."</span></p>
		</td>
 
		<td width=365 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>".$job->job->name."</span></p>
		</td>

		<td width=57 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>шт.</span></p>
		</td>
		
	  
		<td width=76 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$job->count."</span></p>
		</td>	
		
		<td width=57 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>$job->price</span></p>
		</td>

		<td width=119 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$job->count * $job->price."</span></p>
		</td>

	 </tr>
		
	";
 
 
 
 
 
 
 } 
 echo $table;
 
 
 ?>
 

 
 
 
 
 
 
 
 
 
 
 
 <tr >
  <td width=580 colspan=5 style='width:435.25pt;border:none;border-right:solid windowtext 1.0pt;
  padding:0cm 0cm 0cm 0cm;height:17.0pt'>
  <p class=MsoNormal align=right style='margin-right:5.65pt;text-align:right'><b><span
  style='font-size:9.0pt'><span
  style='color:windowtext;text-decoration:none'>Итого</span></a>:</span></b></p>
  
  </td>
  <td width=119 >
  
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>
  
  <?
				
				if($order->discount_order > 0)
				{
					$discount = ($summa / 100) * $order->discount_order;
					$summa = $summa - $discount;
				}
				
				if($order->prepay > 0)
				{
					$summa = $summa - $order->prepay;
				}
				
  echo $summa;
  
  ?>
  
  </span></p>
  </td>
  
  
 </tr>
 
 
</table>

<p class=MsoNormal>&nbsp;</p>



<p class=MsoNormal><span style='font-size:12.0pt'>Исполнитель
______________________       Заказчик ___________________________</span></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>                                                М.П.                                                                                       М.П.</p>

</div>