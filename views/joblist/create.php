<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\JobList */

//$this->title = 'Создание Job List';
$this->title = $title . " " .$id;
//$this->params['breadcrumbs'][] = ['label' => 'Job Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index', 'id' => $id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-list-create">


    <?= $this->render('_form', [
		'id' 	=> $id,
		'date_cr_v' =>time(),
		'date_up_v' => time(),
        'model' => $model,
        'service' => $service,
        'parts' => $parts,
        'job' => $job,
    ]) ?>

</div>
