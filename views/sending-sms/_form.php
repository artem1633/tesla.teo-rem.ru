<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SendingSms */
/* @var $form yii\widgets\ActiveForm */

$alreadySelected = \app\models\SmsObject::findAll(['sms_id' => $model->id]);
$selected = [];
//$users = \app\models\Users::find()->all();
foreach ($alreadySelected as $items)
{
    $selected[] = $items->object->id;
}

?>

<div class="sending-sms-form">
        <div class="box box-default">   
        <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>

<div class="col-md-5 center">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tekst')->textarea(['rows' =>8]) ?>

    <div style="display: none;"> 
        <?= $form->field($model, 'count')->textInput() ?>

        <?= $form->field($model, 'created_date')->textInput() ?>

        <?= $form->field($model, 'update_date')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

</div>
    <div class="row">
       <div class="col-md-5 center">
            <?= $form->field($refe, 'Клиенты')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Client::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите Клиенты...', 'multiple' => true,
                    'value' => $selected,],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
            ])->label('Клиенты'); ?>
       </div> 

</div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
