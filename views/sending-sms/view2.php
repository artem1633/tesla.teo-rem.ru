<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Template */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="template-view">
    <div class="box box-default">  
        <div class="box-body"> 

            <table class="table table-bordered table-condensed">
                <tr>
                    <td>2{company_name}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{company_adress}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{company_phone}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{mode_of_operation}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{template_name}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{akt_number}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{akt_date}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{client_name}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{client_phone}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{device}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{device_made_name}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{device_modeldevice_name} </td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{nothealth_name}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{discount_order}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{prepay}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{warranty_period}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{table}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{client_signature}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{employee_signature}</td>
                    <td></td>
                </tr>
                 <tr>
                    <td>{order_usermas_name}</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>
