<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SendingSms */

$this->title = 'Update Sending Sms: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sending Sms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sending-sms-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
