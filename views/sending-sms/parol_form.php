<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmsParol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sms-parol-form">

    <?php $form = ActiveForm::begin(['action'=> '/sending-sms/parol',]); ?>

    <?= $form->field($model, 'sms_parol')->passwordInput(['maxlength' => true]) ?>

	<div style="display: none;">
    <?= $form->field($model, 'sms_id')->textInput(['maxlength' => true]) ?>
	</div>
    <div class="form-group">
        <?= Html::submitButton('OK', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
