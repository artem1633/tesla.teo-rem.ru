<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

$gridColumn = [
    'name',
    'tekst:ntext',
    'count',
    'status',
    'created_date',
];

$this->title = 'СМС';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sending-sms-index">

    
    <div class="box box-default">  
        <div class="box-body"> 

               
                    <?= Html::a('Создать рассылка', ['create'], ['class' => 'btn btn-success']) ?>
                    <?php // Html::a('Печатать', ['sending-sms/printlist'], ['class' =>'btn btn-info','target'=>"_blank"]) ?>
                    <?= Html::button('Изменить пароль',  ['value'=>Url::to('nastroyka/changesms'), 'class'=>'btn btn-primary', 'id' => 'modalButton']) ; ?>
                    <?= ExportMenu::widget([ 'dataProvider' => $dataProvider, 'columns' => $gridColumn ]); ?>
                            <?php 
                            Modal::begin([
                                'header'=>'<h4>Изменить пароль</h4>',
                                'id' => 'modal',
                                'size' => 'modal-lg',
                            ]);

                            echo "<div id='modalContent'></div>";
                            Modal::end();
                            ?>
                
            </div>
        </div>
    <div class="box box-default">  
        <div class="box-body"> 
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    'name',
                    'tekst:ntext',
                    'count',
                    //'status',
                    'created_date',
                    [
                        'attribute'=>'otp',
                        'content'=>function ($data){                 
                            return '<span class="label label-default">'.Html::a('Повторной отправка '.Html::tag('i', '', ['class' => 'fa fa-external-link']), ['parol','id' =>$data->id]).'</span>';                                     
                        },
                    ],
                    // 'update_date',

                    ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}'],
                ],
            ]); ?>
        </div>
    </div>
</div>
