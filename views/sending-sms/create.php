<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SendingSms */
$refe = new \app\models\SmsObject();

$this->title = 'Создать рассылку';
$this->params['breadcrumbs'][] = ['label' => 'СМС', 'url' => ['index']];
$this->params['breadcrumbs'][] ="Создать рассылка";
?>
<div class="sending-sms-create">
<br>
    

    <?= $this->render('_form', [
        'model' => $model,
        'refe' => $refe,
    ]) ?>

</div>
