<?php
/* @var $this yii\web\View */
/* @var array $data */
?>
<div class="content animate-panel">
<div id="middle" class="dashboard ">
    <div class="row row-15">
        <div class="col-lg-12 text-center" style="z-index: 1">
            <h2>
                <a href="https://www.youtube.com/watch?v=xShihXdr738&amp;utm_source=new-install&amp;utm_medium=help&amp;utm_content=dashboard-title" style="color:#696969" target="_blank">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Основные бизнес-показатели компании</a>                    <span class="pull-right">
                    <!-- <div id="daterange" class="btn btn-info">
                        <span>1/11/17 - 6/11/17</span> <b class="caret"></b>
                    </div> -->


                </span>
            </h2>
        </div>
    </div>




    <div class="row row-15">
        <div class="col-lg-3">
            <div class="box box-default">
                <div class="hpanel">
                    <div class="panel-body text-center" style="height:300px">
                        <i class="fa fa-line-chart fa-4x"></i>
                        <h1 class="m-xs"><i class="fa fa-ruble"></i> <?= (int)$data['sumFinish'] ?></h1>
                        <h3 class="font-extra-bold no-margins text-success">
                            Средний чек                        </h3>
                        <small class="m-t-md inline-block">Средний чек рассчитывается делением суммы всех оплаченных клиентами заказов на количество оплаченных заказов</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="box box-default">
                <div class="hpanel stats">
                    <div class="panel-body" style="height:300px">
                        <div class="clearfix">
                            <div class="stats-title pull-left">
                                <h4>Отчет по рекламе</h4>
                            </div>
                            <div class="stats-icon pull-right">
                                <i class="fa fa-cubes fa-2x"></i>
                            </div>
                        </div>
                        <div class="m-t-xs">
                            <?php if(!empty($data['marketing'])):?>
                                <?php foreach ($data['marketing'] as $item): ?>
                                    <span class="font-bold no-margins">
                                        <?= $item['name']?> <span class="pull-right"><?= (int)$item['count']?></span>
                                    </span>
                                    <div class="progress m-t-xs full progress-small">
                                        <div style="width:0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="55"
                                             role="progressbar" class="hidden  progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                За выбранный период нет статистики
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-3">
            <div class="box box-default">
                <div class="hpanel stats">
                    <div class="panel-body" style="height:300px">
                        <div class="clearfix">
                            <div class="stats-title pull-left">
                                <h4>Отчет по зарплатам</h4>
                            </div>
                            <div class="stats-icon pull-right">
                                <i class="fa fa-users fa-2x"></i>
                            </div>
                        </div>
                        <?php if(!empty($data['salaries'])):?>
                            <?php foreach ($data['salaries'] as $item): ?>
                                <span class="font-bold no-margins">
                                        <?= $item['name']?> <span class="pull-right"><?= (int)$item['result']?></span>
                                    </span><br>
                            <?php endforeach; ?>
                        <?php else: ?>
                            За выбранный период нет статистики
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-lg-3">

            <div class="box box-default">
                <div class="hpanel stats">
                    <div class="panel-body" style="height:300px">
                        <div class="stats-title pull-left">
                            <h4>Данные по продажам</h4>
                        </div>
                        <div class="stats-icon pull-right">
                            <i class="fa fa-money fa-2x"></i>
                        </div>
                        <div class="clearfix"></div>
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-cash-chart" style="margin-top: 30px; padding: 0px; position: relative;"><canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 210px; height: 60px;" width="210" height="60"></canvas><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 210px; height: 60px;" width="210" height="60"></canvas></div>
                        </div>
                        <div class="row-fluid m-t-sm">
                            <div class="col-xs-6 no-padding">
                                <small class="stat-label">Всего заказов</small>
                                <h4><i class="fa fa-ruble"></i> <?= (int)$data['countAllOrders']?> </h4>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <small class="stat-label">В работе</small>
                                <h4><i class="fa fa-ruble"></i> <?= (int)$data['countWorkOrders']?> </h4>
                            </div>
                            <div class="col-xs-12 no-padding">
                                <small class="stat-label">Завершено Успешно</small>
                                <h4><i class="fa fa-ruble"></i> <?= (int)$data['countFinishOrders']?> </h4>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <small class="stat-label">Сумма всего</small>
                                <h4><i class="fa fa-ruble"></i> <?= (int)$data['sumAll']?> </h4>
                            </div>
                            <div class="col-xs-6 no-padding">
                                <small class="stat-label">Сумма выполнено</small>
                                <h4><i class="fa fa-ruble"></i> <?= (int)$data['sumFinish']?> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    </div>
    </div>
