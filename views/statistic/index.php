<?php
use yii\helpers\Url;
use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Статистика';
?>



    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <!-- small box -->
          <div class="small-box widget-stats" style="background-color:#00acac;">
            <div class="inner">
              <h3><?=$dataProviderAuthorized->getCount()?></h3>
              <p><b>АВТОРИЗОВАНО</b></p>
            </div>
            <div class="stats-icon">
              <i class="fa fa-key"></i>
            </div>
            <a href="<?=Url::to(['/users/index'])?>" style="background-color: #006767 " class="small-box-footer">Подробнее ... <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <!-- small box -->
          <div class="small-box widget-stats" style="background-color: #348fe2;">
            <div class="inner">
              <h3><?=$dataProviderRegistered->getCount()?></h3>

              <p><b>ЗАРЕГИСТРИРОВАНО</b></p>
            </div>
            <div class="stats-icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="<?=Url::to(['/users/index'])?>" style="background-color: #1f5688" class="small-box-footer">Подробнее ... <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <!-- small box -->
          <div class="small-box widget-stats" style="background-color: #727cb6;">
            <div class="inner">
              <h3><?=$dataProviderOpenedRegistrationPage->getCount()?></h3>

              <p><b>ПЕРЕШЛИ НА РЕГИСТРАЦИЮ</b></p>
            </div>
            <div class="stats-icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="javascript:;" style="background-color: #444a6d;" class="small-box-footer">Подробнее ... <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-md-3 col-sm-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Фильтры</h4>
                </div>
                <div class="panel-body">

                    <div class="store-remains-search">

                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>

                            <div class="row">
                                <div class="col-md-12 vcenter">
                                    <?= $form->field($searchModel, 'period')->dropDownList($searchModel->getPeriodsList(),
                                        ['prompt' => 'Выберите вариант']) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 vcenter">
                                    <?= Html::submitButton('Применить', ['class' => 'btn btn-primary btn-block']) ?>
                                </div>
                            </div>

                        <?php ActiveForm::end(); ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php

$this->registerJsFile('/theme/assets/js/dashboard.min.js', ['position' => yii\web\View::POS_READY]);

?>