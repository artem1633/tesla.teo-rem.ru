<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShopList */

//$this->title = 'Изменение Shop List: ' . $model->id;
$this->title = $title . $model->id;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Shop Lists', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="shop-list-update">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
