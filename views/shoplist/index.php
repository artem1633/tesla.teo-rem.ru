<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Shop Lists';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-list-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->

    <p>
       <!-- <?= Html::a('Create Shop List', ['create'], ['class' => 'btn btn-success']) ?>
        --><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            'id',
            [
				'attribute' => 'parts_by',				
				'label' => 'Товар',	
				'value'=>'parts.name',
			 ],
            'price',
            'count',
            'shop_by',

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{link}'
			],
        ],
    ]); ?>
</div>
