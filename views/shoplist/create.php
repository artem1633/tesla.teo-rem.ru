<?php

use yii\helpers\Html;
 

/* @var $this yii\web\View */
/* @var $model app\models\ShopList */

//$this->title = 'Создание Shop List';
//$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Shop Lists', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-list-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'parts' => $parts,
        'id' => $id,
    ]) ?>

</div>
