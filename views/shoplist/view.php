<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ShopList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Shop Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Печатать', ['print', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> 
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parts_by',
            'price',
            'count',
            'shop_by',
        ],
    ]) ?>

</div>
