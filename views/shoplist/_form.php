<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ShopList */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-dialog modal-small">
	<div class="modal-content">
		<div class="nothealth-form">
				<div class="modal-body">


							<?php $form = ActiveForm::begin(); ?>
								
							<?= $form->field($model, 'parts_by')->widget(Select2::classname(), [
							'data' => $parts,
							'options' => ['placeholder' => 'Выберите ...'],
							'pluginEvents' => [
										"change" => "function() 
										{
											$.get('/g-service/shop-list/price',
											{'id':$(this).val()},
											function(data){ $('#price').val(data);}     
											);
											
											$.get('/g-service/shop-list/count',
											{'id':$(this).val(),'count':'1'},
											function(data)
											{ 
												if (!data)
												{
													alert('На складе нет данного товара!');
													$('#price').val('');
													$(this).val(null);
												}
											}     
											);
											
										}",
									],
							'pluginOptions' => [
								'allowClear' => true,],
								
							]);?>
							

							<?= $form->field($model, 'price')->textInput(['id' => 'price']) ?>

							<?= $form->field($model, 'count')->textInput(['id' => 'count','onChange' =>"
							$.get('/g-service/shop-list/count',
											{'id':$('#shoplist-parts_by').val(),'count':$(this).val()},
											function(data)
											{ 
												if (!data)
												{
													alert('На складе не достаточно товара!');
													$('#count').val('');
												}
											}     
											);
							"]) ?>

							<?= $form->field($model, 'shop_by')->hiddenInput(['value' => $id])->label(false) ?>

							<div class="form-group">
								<?= Html::a('Закрыть',['#'],['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
								<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
							</div>

							<?php ActiveForm::end(); ?>

				</div>
		</div>
	</div>
</div>
