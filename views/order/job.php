<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
/* @var $this yii\web\View */
/* @var $searchModel app\models\JobListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Job Lists';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-list-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <div class="row">
	   <?php if($visualButton){echo '<div class="col-md-2"><h2>'. Html::encode($title) .'</h2></div>'; 
			if($visualButton)
			{echo '<div class="input-group date col-md-2"><h2>';
						echo Html::activeDropDownList($order, 'status_order', $statusOrder, ['prompt' => 'Установите статус','class' => 'form-control',
								/*'onChange'=>
								'$.get("newstatus",{"idOrder":' . $id . ', "newSum": ' . $sum . ', "status_order":$(this).val()});											
								'*/
								'onchange' =>'window.location.href = "/order/newstatus?idOrder='.$id.'&newSum='.$sum.'&status_order="+$(this).val();'
							]);
					
			 echo '</h2></div>';
			}}
			?>
			</div>
			
			
			
			
			<br/>
			<div class="row">
			<?php
			 if($visualButton){
				 
				echo '<div class="col-xs-6 col-sm-2">';
				echo Html::a('Добавить услугу', ['job-list/create', 'id'=>$id, 'job' => 'service'], ['class' => 'btn btn-success','onClick' => '
			
					var modal = $(".modal");
					$.get("/job-list/create?id='.$id.'&job=service", function(data) {
					modal.html(data).modal("show");
					});
					return false;'
					
				]);
				
				echo '</div>';
				echo '<div class="col-xs-6 col-sm-2">';
				echo Html::a('Добавить товар', ['/job-list/create', 'id'=>$id,'job' =>'parts'], ['class' => 'btn btn-success','onClick' => '
			
					var modal = $(".modal");
					$.get("/job-list/create?id='.$id.'&job=parts", function(data) {
					modal.html(data).modal("show");
					});
					return false;'
					
					]);
				
				
				echo '</div>';
			 }
					if(isset($id))
					{	echo '<div class="col-xs-6 col-sm-2">';
						echo ButtonDropdown::widget([
								'label' => 'Печатать',
								'options' => [
									'target'=>'_blank',
									'class' => 'btn-info',
									'style' => 'margin:2px',
									],
									'dropdown' => [
									'items' => [
														[
															'label' => 'Акт выполненных работ',
															'url' => ['print0','id' => $id],
															'linkOptions' => ['target'=>'_blank'],
														],
														[
															'label' => 'Акт приема',
															'url' =>['printcreate0','id' => $id],
															'linkOptions' => ['target'=>'_blank'],
														],
														[
															'label' => 'Форму заказа',
															'url' =>['print-order','id' => $id],
															'linkOptions' => ['target'=>'_blank'],
														],
													]
								]	
						]);
						
						//echo Html::a(Html::tag('i', '', ['class' => 'fa fa-print']).' Печатать', ['print','id' => $id], ['class' => 'btn btn-info','target'=>"_blank"]);
						echo '</div>';
					}
			if($visualButton && $dataProvider->getCount() > 0){		
				echo "</div>";
				echo "<br/>";
				echo '<div class="row">';
				echo '<div class="col-xs-6 col-sm-2">';
				echo Html::a(' Завершить заказ', ['newstatus','idOrder'=>$id, 'status_order' => 7,'newSum' =>$sum], ['class' => 'btn btn-danger',]);
				echo '</div>';
			}
		?>
			
		 </div>								
										 
    </p>
	
	
	
	
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            //'id',
            [
			'attribute'=>'id',
			'content'=>function ($data){                 
				return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['job-list/update','id' =>$data->id, 'order_id' => $data->order_by ], ['style'=>'color:#ffffff;']).'</span>';
						 
			 },
			],
            'order_by',
			[
				'attribute' => 'job_name',				
				'label' => 'Работа',	
				'value'=>'job.name', 
				'content'=>function ($data){   
					if($data->type == 'service')  return $data->job->name;
					else { $resource = \app\models\Resource::findOne($data->job_by);
						$tovar = \app\models\manual\Parts::findOne($resource->parts_by);
						return $tovar->name;
					}
						 
			 },
			 ],
            'count',
            'price',
            // 'date_cr',
            // 'date_up',

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{delete} {link}',
			'buttons' => [
               'delete' => function ($url, $model, $key){ 
				if($model->getStatus_order($model->order_by) != 7){
					return Html::a('<span class="glyphicon glyphicon-trash"></span>', '/job-list/delete?id='.$key);
				}
              }, 
				
            ],
			],
        ],
    ]); ?>
	
	<h3><?php if(isset($id)){ echo Html::encode('Итого : '.$sum  . "   (Сумма  с учетом скидки и предоплаты)"); }?></h3>
										
							
									
<?php Pjax::end(); ?>
</div>
