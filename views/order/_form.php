<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
	<div class="box box-default">	
		<div class="box-body">

			<?php $form = ActiveForm::begin(['action'=>$model->isNewRecord ?  '/order/create' : '/order/update?id='.$model->id,]); ?>
	<div class="row">
		<div class="col-md-3">
			
					<?php //$form->field($model, 'return_waid')->textInput(['type'=>'date']) ?>
					<?php 
					echo $form->field($model, 'return_waid')->widget(DateTimePicker::classname(), [
						'options' => ['placeholder' => 'Выберите дата выполнения'],
						'pluginOptions' => [
							'autoclose' => true
						]
					]);

					?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'type_order_by')
			->dropDownList($typeOrder ,
			['prompt' => 'Выберите один вариант']);	?>			
		
		</div>
		<div class="col-md-3">			
			<?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-4">
		<?= $form->field($model, 'client_by')->widget(Select2::classname(), [
						'data' => $clients,
						'options' => ['placeholder' => 'Выберите ...'],
						'disabled' => $model->isNewRecord ?  false : true,
						 'pluginEvents' => [
									"change" => "function() 
									{
										$.get('mydiscount',
										{'id':$(this).val()},
										function(data){ $('#discount_order').val(data);}     
										);
										
										$.get('myphone',
										{'id':$(this).val()},
										function(data){ $('#phone').val(data);}
										);

										$.get('myaddress',
										{'id':$(this).val()},
										function(data){ $('#phone2').val(data);}
										);

										
									}",
								],
						'pluginOptions' => [
							'tags' => true,
							'allowClear' => true,],
							//
						]);?>
		</div>				
		<div class="col-md-5">	
		<?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
		'mask' => '+7(999) 999-9999','id'=>'phone',
        'options' => ['id'=>'phone','class'=>'form-control','disabled' => $model->isNewRecord ?  false : true,]
		]) ?>

		</div>
		<div style="display: none;">
		<div class="col-md-9">	
		<?= $form->field($model, 'client_address')->textInput([
		'id'=>'phone2',
        'options' => ['id'=>'phone2','class'=>'form-control','disabled' => $model->isNewRecord ?  false : true,]
		]) ?>
		</div>
		</div>
	</div>				
	<div class="row">
		<div class="col-md-4">

								
			<?= $form->field($model, 'user_by')
			->dropDownList($user,
			['prompt' => 'Выберите один вариант']);	?>
		</div>
		
		<div class="col-md-5">
		<?= $form->field($model, 'nothealth_by')->widget(Select2::classname(), [
							'data' => $notHealth,
							'options' => ['placeholder' => 'Выберите ...'],
							'pluginOptions' => [
								'tags' => true,
								'allowClear' => true,],
							]);?>
		</div>					
	</div>							
	<div class="row">
		<div class="col-md-3">	
			<?= $form->field($model, 'type_device_by')->widget(Select2::classname(), [
				'data' => $typeDevice,
				'options' => ['placeholder' => 'Выберите ...'],
				'pluginOptions' => [
					'allowClear' => true,],
			]);?>		
		</div>
		<div class="col-md-3">		
			<?= $form->field($model, 'made_by')->widget(Select2::classname(), [
				'data' => $madeDevice,
				'options' => ['placeholder' => 'Выберите ...'],
				'pluginOptions' => [
					'tags' => true,
					'allowClear' => true,],
			]);?>
		</div>					
		<div class="col-md-3">						
							
							<?= $form->field($model, 'model_device_by')->widget(Select2::classname(), [
							'data' => $modelDevice,
							'options' => ['placeholder' => 'Выберите ...'],
							'pluginOptions' => [
								'tags' => true,
								'allowClear' => true,],
							]);?>
		</div>					
	</div>	
	<div class="row">
		<div class="col-md-4">							
			<?= $form->field($model, 'view')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-5">	
			<?= $form->field($model, 'configuration')->textInput(['maxlength' => true]) ?>
		</div>					
	</div>		
	<div class="row">
		<div class="col-md-3">	
			<?= $form->field($model, 'price')->textInput() ?>
		</div>
		<div class="col-md-3">		
			<?= $form->field($model, 'prepay')->textInput() ?>
		</div>
		<div class="col-md-3">	
			<?= $form->field($model, 'discount_order')->textInput(['id'=>'discount_order']) ?>
		</div>					
	</div>
	<div class="row">
		<div class="col-md-9">
			<?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
		</div>					
	</div>
	
	<div class="row">
		<div class="col-md-9">	
			<?= $form->field($model, 'marketing_by')
				->dropDownList($marketing,
				['prompt' => 'Выберите один вариант']);	?>	
		</div>					
	</div>
	

						

						
						

						<?= $form->field($model, 'quick')->checkbox() ?>

						
						
						<div style="display:none">
						<?= $form->field($model, 'status_order')
							->dropDownList($statusOrder,
							['prompt' => 'Выберите один вариант']);	?>
							
							
						<?= $form->field($model, 'user_by_cr')->textInput(['value'=>$user_by_cr_v]) ?>

						<?= $form->field($model, 'user_by_up')->textInput(['value'=>$user_by_up_v]) ?>

						 
						<?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

						<?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>

						<?= $form->field($model, 'return_date')->textInput(['value'=>time()]) ?>
						</div>
						
						
						<div class="form-group">
							<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>

						<?php ActiveForm::end(); ?>

</div>
</div>
</div>
