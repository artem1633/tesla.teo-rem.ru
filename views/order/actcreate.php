﻿<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:35.45pt 28.3pt 2.0cm 49.65pt;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=RU>

<div class=WordSection1>

<p class=MsoNormal  style='font-size:8.00pt;'>
<?php
use app\models\Order;
use app\models\AktRemont;
/**@var Order $order */
$shablon = AktRemont::findOne(1);

echo $shablon->top ?></p>

<br/>
<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:14.0pt;line-height:115%'><?= $shablon->shablon_name ?> № <?=$order->id ?> от  <?=date('Y.m.d H:i', time()); ?></span></b></p>
<br/>
<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='margin-left:5.4pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=319 valign=top style='width:239.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><?= $shablon->klient ?>: <?=$order->client->name ?><br>
  Тел.: <?=$order->client->phone ?><br>
  <?= $shablon->device ?>: <?=$order->device->name.' '.$order->made->name.' '.$order->modeldevice->name ?> <br>
  <?= $shablon->equipment ?>: <?=$order->configuration ?> <br>
  <?= $shablon->appearance ?>: <?=$order->view ?> <br>
  <?= $shablon->defect ?>: <?=$order->nothealth->name ?></p>
  </td>
  <td width=371 valign=top style='width:278.15pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><?= $shablon->orient_data ?>: <?=$order->return_waid ?> <br>
  <?= $shablon->orient_cost ?>: <?=$order->price ?> руб <br>
  <?= $shablon->prepay ?>: <?=$order->prepay ?> руб <br>
  <?= $shablon->zametka ?>: <?=$order->comment ?></p>
  </td>
 </tr>
</table>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal style='font-size:8.00pt;'> <?=$shablon->tekst ?> </p>
<br>
<br>
<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr style='height:28.05pt'>
  <td width=352 valign=top style='width:264.05pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.05pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><?= $shablon->priyomshik ?>: ________________(<?=$order->usercr->name ?>)</p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>                      М.П.</p>
  </td>
  <td width=352 valign=top style='width:264.1pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.05pt'>
  <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'>_____________________ (<?=$order->client->name ?>)</p>
  <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'>с условиями ремонта ознакомлен и
  согласен</p>
  </td>
 </tr>
</table>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>______________________________________________________________________________________________</p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:14.0pt;line-height:115%'><?= $shablon->inform_master ?> № <?=$order->id ?> от  <?=date('Y.m.d H:i', time()); ?>6</span></b></p>

 <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><?= $shablon->klient ?>: <?=$order->client->name ?>, <?=$order->client->phone ?><br>
  <?= $shablon->device ?>: <?=$order->device->name.' '.$order->made->name.' '.$order->modeldevice->name ?> <br>
  <?= $shablon->equipment ?>: <?=$order->configuration ?> <br>
  <?= $shablon->appearance ?>: <?=$order->view ?> <br>
  <?= $shablon->defect ?>: <?=$order->nothealth->name ?><br>
  <?= $shablon->zametka ?>: <?=$order->comment ?></p>

</div>

</body>

</html>
