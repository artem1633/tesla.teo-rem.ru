<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use app\models\Order;
use yii\helpers\ArrayHelper;
use app\models\User;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use dosamigos\datepicker\DatePicker;

$gridColumn = [
	[
		'attribute'=>'id',
		'content'=>function ($data){                 
			return '<span class="label label-default">'.Html::a($data->id.'   '.Html::tag('i', '', ['class' => 'fa fa-external-link']), ['view','id' =>$data->id]).'</span>';									 
		},
	],
	[
		'attribute' => 'status_order',				
		'label' => 'Статус',	
		'content'=>function($data){
		if ($data->status->id == 7)
			{return '<span class="label label-success">'.$data->status->name.'</span>'; }
				elseif($data->status->id == 2){return '<span class="label label-danger">'.$data->status->name.'</span>';}
				else{return '<span class="label label-info">'.$data->status->name.'</span>';}			 
		},
	],
	[
		'attribute'=>'return_waid',
		'content'=>function($data){
			 if ($data->return_waid > 0 ){
				 $finish = ceil(($data->return_waid - time()) / (24*60*60));
					if ($finish > 0 && $data->status->id != 7 )
					{
						return '<span class="label label-success">'.$finish.' дн.</span>';
					}
						elseif($finish <= 0 && $data->status->id != 7)
						{//если заказ до сих пор не завершен а дней 0
							return '<span class="label label-danger">'.$finish.' дн.</span>';
						}elseif($data->status->id == 7 ){//если дата была указа и заказ был завершен
							return '<span class="label label-success">Заказ завершен</span>'; 
						}					  
			 }else{//Если не указали дату
				 return '<span class="label label-warning">Не указано</span>';
			 }			 
		 },
	],
	[
		'attribute' => 'client_by',				
		'label' => 'Клиент',	
		'content'=>function($data){
			return ''.$data->client->name.'<span class="pull-right text-green">('.Order::find()->where(['client_by'=>$data->client->id])->count().')</span><br/>'.$data->client->phone; 
		},
	],
	'client_address',
	 [
		'attribute' => 'nothealth_by',				
		'label' => 'Неисправность',	
		'value'=>'nothealth.name',
	 ],
	 [
		'attribute' => 'type_device_by',
		
		'content'=>function($data){
			$st1 = '';
			$st2 = '';
			$st3 = '';
				if (isset($data->device->name)) {$st1 = $data->device->name;}
				if (isset($data->made->name)) {$st2 = $data->made->name;}
				if (isset($data->modeldevice->name)) {$st3 = $data->modeldevice->name;}
					return ''.$st1.'<br/>'.$st2.' '.$st3; 				
		},
	 ],
	
	 'price',	
	[
		'attribute' => 'user_by',				
		'label' => 'Мастер',	
		'value'=>'usermas.name',
	 ],
		
	 [
		'attribute' => 'user_by_cr',				
		'label' => 'Принял',	
		'content'=>function($data){
				return ''.$data->usercr->name.'<br/>'.date('d.m.Y H:i',$data->date_cr); 
		},
	 ],
	 [
		'attribute' => 'return_date',
		'content'=>function($data){
				return date('d.m.Y H:i',$data->return_date);
		},
	 ],
];
 

$this->title = 'Заказы';

?>
<style type="text/css">
    .tex-right {
    }
</style>
<div class="order-index">
	    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a(Html::tag('i', '', ['class' => 'fa fa-print']).' Печатать', ['order/printlist','searchId' => $searchId,'linkTime' => $linkTime,'statusSearch'=> $statusSearch], ['class' =>'btn btn-default','target'=>"_blank"]) ?>
		<?php 

		echo ExportMenu::widget([
		    'dataProvider' => $dataProvider,
		    'columns' => $gridColumn
		]);
		?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
		<div class="box box-default">	
			<div class="box-body">
				 <?php echo $this->render('_search', ['model' => $searchModel, 'post' => $post]); ?>
	
			</div>
		</div>
	
	
	
	
	

	
 <div class="app-form">
		<div class="box box-default">	
			<div class="box-body">	
			<div style="width: 100%; overflow: scroll"> 
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'summary' => 'Итого: '.$sum,
				   // 'filterModel' => $searchModel,

					'rowOptions' => 
					function ($model, $key, $index, $grid)
											{
											  if($model->status_order == 7) {
												  return ['class' => 'success'];
											  }
											   if($model->status_order == 3 && !$model->quick ) {								    
												  return ['class' => 'warning'];
											  }
											  if($model->quick && $model->status_order != 7) {								 
												  return ['class' => 'danger'];
											  }
											},
					'columns' => [
					  //  ['class' => 'yii\grid\SerialColumn'],
						

						[
						'attribute'=>'id',
						'content'=>function ($data){                 
							   return '<span class="label label-default">'.Html::a($data->id.'   '.Html::tag('i', '', ['class' => 'fa fa-external-link']), ['view','id' =>$data->id]).'</span>';
									 
						 },
						],
						[
							'attribute' => 'status_order',				
							'label' => 'Статус',	
							'content'=>function($data){
								if ($data->status->id == 7)
								{return '<span class="label label-success">'.$data->status->name.'</span>'; }
								elseif($data->status->id == 2){return '<span class="label label-danger">'.$data->status->name.'</span>';}
								else{return '<span class="label label-info">'.$data->status->name.'</span>';}			 
							},
						 ],
						[
						'attribute'=>'return_waid',
						'content'=>function($data){
							 if ($data->return_waid > 0 ){
							 	$date_time_return_waid = \Yii::$app->formatter->asDatetime($data->return_waid, "php:d-m-Y H:i");
								 $finish = ceil(($data->return_waid - time()) / (24*60*60));
									if ($finish > 0 && $data->status->id != 7 )
									{
										return '<span class="label label-success">'.$date_time_return_waid.'</span>';
									}
										elseif($finish <= 0 && $data->status->id != 7)
										{//если заказ до сих пор не завершен а дней 0
											return '<span class="label label-danger">'.$date_time_return_waid.'</span>';
										}elseif($data->status->id == 7 ){//если дата была указа и заказ был завершен
											return '<span class="label label-success">Заказ завершен <br>  '.$date_time_return_waid.'</span>'; 
										}					  
							 }else{//Если не указали дату
								 return '<span class="label label-warning">Не указано</span>';
							 }			 
						 },
						],
					//	[
				  //      'attribute'=>'return_date',
					//	'content'=>function($data){
					//		 if ($data->return_date > 0 ){
					//			return date('Y.m.d H:i', $data->return_date); 
					//		 }
					//		else{return "Заказ не завершен";}			 
					//	 },
					//	],

					//	[
					//		'attribute' => 'type_order_by',				
					//		'label' => 'Тип',	
					//		'value'=>'typeorder.name',
					//	 ],
						 [
							'attribute' => 'client_by',				
							'label' => 'Клиент',	
							'content'=>function($data){
									return ''.$data->client->name.'<span class="pull-right text-green">('.Order::find()->where(['client_by'=>$data->client->id])->count().')</span><br/>'.$data->client->phone; 
							},
						 ],
						 'client_address',
						 'phone',
						 [
							'attribute' => 'nothealth_by',				
							'label' => 'Неисправность',	
							'value'=>'nothealth.name',
						 ],
						 [
							'attribute' => 'type_device_by',
							//'value'=>'device.name',
							'content'=>function($data){
								$st1 = '';
								$st2 = '';
								$st3 = '';
									if (isset($data->device->name)) {$st1 = $data->device->name;}
									if (isset($data->made->name)) {$st2 = $data->made->name;}
									if (isset($data->modeldevice->name)) {$st3 = $data->modeldevice->name;}
										return ''.$st1.'<br/>'.$st2.' '.$st3; 
									
							},
						 ],
						 'serial_number',
						// 'type_device_by',
						// 'nothealth_by',
						// 'view',
						// 'configuration',
						// 'comment',
						 'price',
						// 'prepay',
						[
							'attribute' => 'user_by',				
							'label' => 'Мастер',	
							'value'=>'usermas.name',
						 ],
						// 'marketing_by',
						// 'discount_order',
						
						// 'quick',
						 [
							'attribute' => 'user_by_cr',				
							'label' => 'Принял',	
							'content'=>function($data){
									return ''.$data->usercr->name.'<br/>'.date('d.m.Y H:i',$data->date_cr); 
							},
						 ],
						 [
							'attribute' => 'return_date',
							'content'=>function($data){
									return date('d.m.Y H:i',$data->return_date);
							},
						 ],
						// 'user_by_up',
						// 'date_cr',
						// 'date_up',

						['class' => 'yii\grid\ActionColumn',
						'template' => '{delete} {update} {link}',
						'buttons' => [
						 //   'update' => function ($url, $model, $key){                 
						 //      return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', '/job-list/index?id='.$key);
						 //   },
							'delete' => function ($url, $model, $key){                 
								if ($model->status_order == 3){
									return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
										['delete','id'=>$key],
										[
											'title'=>'Удалить',
											'aria-label'=>'Удалить',
											'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
											'data-method'=>'post',
										]);
								}
								
							},
							'update' => function ($url, $model, $key){                 
								if ($model->status_order == 3){
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', 
										['update','id'=>$key],
										[
											'title'=>'Редактировать',
											'aria-label'=>'Редактировать',
										]);
								}}
						],
						],
					],
				]); ?></div>
</div>
</div>
</div>

