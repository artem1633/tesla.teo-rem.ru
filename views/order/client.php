<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */


?>
<div class="order-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'return_waid',
            'return_date',
			
			[
				'attribute' => 'type_order_by',		
				'value'=>$model->typeorder->name,
			 ],
			 [
				'attribute' => 'client_by',		
				'value'=>$model->client->name,
			 ],
			 [
				'attribute' => 'phone',		
				'value'=>$model->client->phone,
			 ],
			 [
				'attribute' => 'type_device_by',
				'value'=>$model->device->name,
			 ],
			 [
				'attribute' => 'made_by',		
				'value'=>$model->made->name,
			 ],
			 [
				'attribute' => 'model_device_by',	
				'value'=>$model->modeldevice->name,
			 ],
			 [
				'attribute' => 'nothealth_by',	
				'value'=>$model->nothealth->name,
			 ],
            'view',
            'configuration',
            'comment',
            'price',
            'prepay',
			[
				'attribute' => 'user_by',	
				'value'=>$model->usermas->name,
			 ],
			 [
				'attribute' => 'marketing_by',	
			//	'value'=>$model->marketing->name,
			 ],
            'discount_order',
			[
				'attribute' => 'status_order',	
				'value'=>$model->status->name,
			 ],
            'quick:boolean',
			[
				'attribute' => 'user_by_cr',	
				'value'=>$model->usercr->name,
			 ],
			[
				'attribute' => 'user_by_up',	
				'value'=>$model->userup->name,
			 ],
            [
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
        ],
    ]) ?>

</div>
