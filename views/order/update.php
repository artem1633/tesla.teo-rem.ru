<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

//$this->title = 'Изменение Order: ' . $model->id;
$this->title = $title . $model->id;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="order-update">


    <?= $this->render('_form', [
		'user_by_cr_v' => $user_by_cr_v,
		'user_by_up_v' => $user_by_up_v,
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
		'typeOrder' => $typeOrder,
		'madeDevice' => $madeDevice,
		'modelDevice' => $modelDevice,
		'notHealth' => $notHealth,
		'typeDevice' => $typeDevice,
		'user' => $user,
		'clients' => $clients,
		'marketing' => $marketing,
        'model' => $model,
        'statusOrder' => $statusOrder,
    ]) ?>

</div>
