﻿
<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='margin-left:.25pt;border-collapse:collapse;border:none'>

 <tr style='height:19.85pt'>
  <td width=26 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>№</span></p>
  </td>
  <td width=365 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Наименование работы (услуги)</span></p>
  </td>
  <td width=57 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Ед. изм.</span></p>
  </td>
  <td width=76 >
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Количество</span></p>
  </td>
  <td width=57 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Цена</span></p>
  </td>
  <td width=119 s>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:9.0pt'>Сумма</span></p>
  </td>

 </tr>


 <?php
 $table = '';
 $number = 0;
 $summa = 0;
 foreach($jobList as $job)
 {
 	if($job->type == 'service') { $name= $job->job->name;}
					else { $resource = \app\models\Resource::findOne($job->job_by);
						$tovar = \app\models\manual\Parts::findOne($resource->parts_by);
						$name=$tovar->name;
					}
	 $number++;
	 $summa += $job->count * $job->price;
	$table .= "
	
	 <tr >
		<td width=26 >
		<p class=MsoNormal align=center ><span lang=EN-US
		style='font-size:9.0pt'>".$number."</span></p>
		</td>
 
		<td width=365 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>".$name."</span></p>
		</td>

		<td width=57 >
		<p class=MsoNormal style='margin-left:2.85pt'><span lang=EN-US
		style='font-size:8.0pt;font-family:'Arial Narrow','sans-serif''>шт.</span></p>
		</td>
		
	  
		<td width=76 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$job->count."</span></p>
		</td>	
		
		<td width=57 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>$job->price</span></p>
		</td>

		<td width=119 >
		<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
		style='font-size:9.0pt'>".$job->count * $job->price."</span></p>
		</td>

	 </tr>
		
	";
 }
 echo $table;
 ?>

 <tr >
  <td width=580 colspan=5 style='width:435.25pt;border:none;border-right:solid windowtext 1.0pt;
  padding:0cm 0cm 0cm 0cm;height:17.0pt'>
  <p class=MsoNormal align=right style='margin-right:5.65pt;text-align:right'><b><span
  style='font-size:9.0pt'><span
  style='color:windowtext;text-decoration:none'>Итого</span></a>:</span></b></p>

  </td>
  <td width=119 >

  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:9.0pt'>

  <?php
				if($order->discount_order > 0)
				{
					$discount = ($summa / 100) * $order->discount_order;
					$summa = $summa - $discount;
				}

				if($order->prepay > 0)
				{
					$summa = $summa - $order->prepay;
				}

  echo $summa;
  ?>
  </span></p>
  </td>
 </tr>
</table>