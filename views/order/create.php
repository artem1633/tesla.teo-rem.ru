<?php

use yii\helpers\Html;
 use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

//$this->title = 'Создание Order';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-create">


    <?= $this->render('_form', [
		'user_by_cr_v' => $user_by_cr_v,
		'user_by_up_v' => $user_by_up_v,
		'date_cr_v' => $date_cr_v,
		'date_up_v' => $date_up_v,
		'typeOrder' => $typeOrder,
		'madeDevice' => $madeDevice,
		'modelDevice' => $modelDevice,
		'notHealth' => $notHealth,
		'typeDevice' => $typeDevice,
		'user' => $user,
		'clients' => $clients,
		'marketing' => $marketing,
        'model' => $model,
        'statusOrder' => $statusOrder,
    ]) ?>
	


</div>
