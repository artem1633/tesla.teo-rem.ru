<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\models\manual\StatusOrder;
use kartik\datetime\DateTimePicker;
use kartik\touchspin\TouchSpin;
//use kartik\widgets\DatePicker;
$session = Yii::$app->session;
$search_status = $session['search_status'];

/*echo '<br>'.$session['search_id'];
echo '<br>'.$session['search_master'] ;
echo '<br>'.$session['search_texnika'];
echo '<br>'.$session['search_status'] ;
echo '<br>'.$session['search_client'] ;
echo '<br>'.$session['linkTime'] ;
echo '<br>'.$session['search_address'];
echo '<br>'.$session['search_tel'] ;
echo '<br>'.$session['from'] ;
echo '<br>'.$session['to'] ;
echo '<br>a'.$session['vidat_from'] ;
echo '<br>f'.$session['vidat_to'] ;*/

$array = [];
    foreach ($search_status as $value) {
        $array [] = $value;
    }
?>
<div class="client-form">
    <div class="box box-default">   
        <div class="box-body">  

            <?php $form = ActiveForm::begin(); ?>
         
            <div>
                <div class="col-md-5"  style="margin-top: 20px"> 
                    <?php // echo $form->field($model, 'search_days')->dropDownList($model->getDays(), ['value' => $post['OrderSearch']['search_days'],'prompt'=>'Выберите'])->label('');?>
                    <?php 
$layout = <<< HTML
    <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">С</span>
    {input1}
    <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">По</span>
    {input2}
    <span class="input-group-addon kv-date-remove" style="background-color: #ecf0f5; width: 50px; color:black;">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;
 ?>

         <?php
            echo DatePicker::widget([
                'type' => DatePicker::TYPE_RANGE,
                'name' => 'from',
                'value' => $session['from'],
                'name2' => 'to',
                'value2' => $session['to'],
                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                'options' => [
                    'placeholder' => 'Дата создания', 
                ],
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
        ?>
                </div>
                
                <div class="col-md-1">          
                    <?= $form->field($model, 'search_id')->textInput(['value' => (integer)$session['search_id'], 'placeholder' => '  ID','maxlength' => true])->label(''); ?>         
                </div>

                <div class="col-md-2">          
                    <?= $form->field($model, 'search_master')->textInput(['value' => $session['search_master'],'placeholder' => 'Введите мастеру', 'maxlength' => true])->label(''); ?>       
                </div>

                <div class="col-md-2">          
                    <?= $form->field($model, 'search_texnika')->textInput(['value' => $session['search_texnika'], 'placeholder' => 'Введите технику','maxlength' => true])->label(''); ?>         
                </div>
                <div class="col-md-2">          
                    <?= $form->field($model, 'search_serial_number')->textInput(['value' => $session['search_serial_number'],'placeholder' => 'Серийный номер', 'maxlength' => true])->label(''); ?>       
                </div>
            </div>
            
            <div class="col-md-5" >          
         <?php
            echo DatePicker::widget([
                'type' => DatePicker::TYPE_RANGE,
                'name' => 'search_vidat_from',
                'value' => $session['vidat_from'],
                'name2' => 'search_vidat_to',
                'value2' => $session['vidat_to'],
                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                'layout' => $layout,
                'options' => [
                    'placeholder' => 'Дата выдачи', 
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
        ?>
            </div>
     
    
            
            <div class="col-md-4" style="margin-top: -20px;">          
                <?= $form->field($model, 'search_address')->textInput(['value' => $session['search_address'], 'placeholder' => 'Введите адрес клиента', 'maxlength' => true])->label(''); ?>         
            </div>
            <div class="col-md-3" style="margin-top: -20px;">          
                <?php // $form->field($model, 'search_tel')->textInput(['value' => $session['search_tel'], 'placeholder' => 'Введите тел. номер клиента', 'maxlength' => true])->label(''); ?> 
                <?= $form->field($model, 'search_tel')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9(999) 999-9999',
                    
                    'options' => ['class'=>'form-control','placeholder' => '9(999) 999-9999','value' => $session['search_tel'], ]
                    ])->label(''); ?>        
            </div>
            <div class="col-md-5" style="margin-top: -20px;">          
                <?= $form->field($model, 'search_status')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Установите статус',
                        'multiple' => true,
                        'value' => $array,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(''); ?>
            </div>
        
            <div class="form-group" >
                <?= Html::submitButton('Поиск', [ 'style'=>"margin-left: 15px;", 'class' => 'btn btn-primary']) ?>
                <?php // Html::resetButton('Очистить', ['id' => 'reset', 'id' => 'next_date','class' => 'btn btn-default']) ?>
            </div>  

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php 
$this->registerJs(<<<JS

$('#reset').on('click', function() 
{  
    $("#ordersearch-search_client").val(""); 
    $("#ordersearch-search_texnika").val(""); 
    $("#ordersearch-search_master").val(""); 
    $("#ordersearch-search_id").val(""); 
    $("#ordersearch-search_days").val(""); 
    $("#ordersearch-search_status").val(""); 
}
);
JS
);
?>