<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

	<?php $form = ActiveForm::begin(['action'=> '/order/actcreate',]); ?>

	<?= $form->field($model, 'akt')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]);
    ?>					
						
	<div class="form-group">
        <?= Html::submitButton( 'Создать документ', ['btn btn-success']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
