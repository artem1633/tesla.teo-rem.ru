<?php


use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $title. $id;
$this->params['breadcrumbs'][] = ['label' => $label,'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
	
<div class="modal fade"></div>

	<div class="box box-default">
<!-- /.box-header -->
				<div class="box-body">

<div style="position:relative; left:0px; top:30px"> 
        <?= 
            TabsX::widget([
                'items' => [
                    [
                        'label' => 'Информация',
                        'options' => ['id' => 'tab1'],
						'content' => $contentOrder,
                    ],
                    [
                        'label' => 'Ремонт',
                        'options' => ['id' => 'tab2'],
                        'content' => $contentJob,
						//'content' => $this->redirect(['/g-servise/joblist/index', 'id' => $model->id])
						'active'=>true,
						//'linkOptions'=>['data-url'=>Url::to(['job-list/index?id=7'])]
                    ],
                ],
            ]);
        ?>
</div>
</div>
</div>

</div>
