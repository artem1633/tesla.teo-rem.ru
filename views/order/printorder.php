
<div class=Section1>
 <br><br><br><br><br>
<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 style='margin-left:.25pt;border-collapse:collapse;border:none'> 
 
<tr style='height:19.85pt'>
	<td width=200 height=70 colspan="6">
		<p class=MsoNormal style='font-size:26.0pt'><center><b>Форма заказа</b></center></p>
	</td>
</tr>
<tr style='height:19.85pt'>
		<td width=200 height=70 >
			<p class=MsoNormal style='font-size:16.0pt'><b>Выдать</b></p>
		</td>
	    <td width=300 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?php if ($order->return_waid > 0 ){$waid = date('Y.m.d H:i', $order->return_waid);}else{$waid = "не задано";} echo $waid; ?></center></p>
	    </td>
	    <td width=200 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Тип заказа</b></p>
		</td>
	    <td width=350 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?= $order->typeorder->name ?></center></p>
	    </td>

	    <td width=150 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Мастер</b></p>
		</td>
	    <td width=400 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->user->name ?></center></p>
	    </td>  
</tr>
<tr style='height:19.85pt'>
		<td width=200 height=70 >
			<p class=MsoNormal style='font-size:16.0pt'><b>Клиент</b></p>
		</td>
	    <td width=300 height=70 colspan="3">
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->client->name?></center></p>
	    </td>
	    <td width=200 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Телефон клиента</b></p>
		</td>
	    <td width=350 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?php echo $order->phone; ?></center></p>
	    </td>
</tr>
<tr style='height:19.85pt'>
		<td width=200 height=70 >
			<p class=MsoNormal style='font-size:16.0pt'><b>Не исправность</b></p>
		</td>
	    <td width=300 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->nothealth->name; ?></center></p>
	    </td>
	    <td width=200 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Устройство</b></p>
		</td>
	    <td width=350 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->device->name; ?></center></p>
	    </td>

	    <td width=150 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Производитель</b></p>
		</td>
	    <td width=400 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->made->name; ?></center></p>
	    </td>  
</tr>
<tr style='height:19.85pt'>
		<td width=200 height=70 >
			<p class=MsoNormal style='font-size:16.0pt'><b>Модель</b></p>
		</td>
	    <td width=300 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->modeldevice->name; ?></center></p>
	    </td>
	    <td width=200 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Внешний вид</b></p>
		</td>
	    <td width=350 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->view; ?></center></p>
	    </td>

	    <td width=150 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Комплектация</b></p>
		</td>
	    <td width=400 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->configuration; ?></center></p>
	    </td>  
</tr>
<tr style='height:19.85pt'>
		<td width=200 height=70 >
			<p class=MsoNormal style='font-size:16.0pt'><b>Цена</b></p>
		</td>
	    <td width=300 height=70>
	  		<p class=MsoNormal style='font-size:16.0t'><center><?=$order->price; ?></center></p>
	    </td>
	    <td width=200 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Предоплата</b></p>
		</td>
	    <td width=350 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->prepay; ?></center></p>
	    </td>

	    <td width=150 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Скидка</b></p>
		</td>
	    <td width=400 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->discount_order; ?></center></p>
	    </td>  
</tr>
<tr style='height:19.85pt'>
		<td width=200 height=70 >
			<p class=MsoNormal style='font-size:16.0pt'><b>Коментарий</b></p>
		</td>
	    <td width=300 height=70 colspan="3">
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->comment; ?></center></p>
	    </td>
	    <td width=200 height=70>
			<p class=MsoNormal style='font-size:16.0pt'><b>Реклама</b></p>
		</td>
	    <td width=350 height=70>
	  		<p class=MsoNormal style='font-size:16.0pt'><center><?=$order->marketing->name; ?></center></p>
	    </td>
</tr>
 
</table>
</div>
