<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Зарплаты';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
 <div class="row">
     <?php echo $this->render('_search', ['post' => $post, 'model' => $model, 'user' => $user]); ?>
 </div>

<!--     <div class="box box-default">  
    <div class="box-body"> 
<br/>
			<div class="row">
				<div class="col-md-4">
					<div class="btn-group btn-group-sm" role="group" aria-label="...">
					
								
								<?php // Html::input('date','',date('Y-m-d',$timeStart),['id'=>'timeStart','style'=> 'font-size:12px','class' => 'btn btn-primary','onchange' =>'window.location.href = "/report/finance?timeEnd="+$("#timeEnd").val()+"&timeStart="+$(this).val();']) ?>
								<?php // Html::input('date','',date('Y-m-d',$timeEnd),['id'=>'timeEnd','style'=> 'font-size:12px','class' => 'btn btn-primary','onchange' =>'window.location.href = "/report/finance?timeStart="+$("#timeStart").val()+"&timeEnd="+$(this).val();']) ?>
					
						</div>
				</div>
				<div class="col-md-4">
					<div class="btn-group btn-group-sm" role="group" aria-label="...">
						
						<?php /*Html::dropDownList('status', $userSearch, $user, ['prompt' => 'Выберите пользователя','class' => 'btn btn-primary',
							'onchange' =>'window.location.href = "/report/finance?timeStart='.date('Y-m-d',$timeStart).'&timeEnd='.date('Y-m-d',$timeEnd).'&userSearch="+$(this).val();']);*/ ?>
						<span style="margin-left: 20px;"><?php // Html::a('Печатать', ['sending-sms/printlist'], ['class' =>'btn btn-info', 'target'=>"_blank"]) ?></span>
								
						</div>
				</div>
			</div>

	<br/>
		</div>
	</div> -->
	
    <div class="box box-default">  
        <div class="box-body"> 	
	<br/>
		    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
				'summary' => 'Итого: '.$sum . '; Оплата сотруднику: '.$protect,
		       // 'filterModel' => $searchModel,

				'rowOptions' => 
				function ($model, $key, $index, $grid)
										{
										  if($model->status_order == 7) {
											  return ['class' => 'success'];
										  }
										   if($model->status_order == 3 && !$model->quick ) {								    
											  return ['class' => 'warning'];
										  }
										  if($model->quick && $model->status_order != 7) {								 
											  return ['class' => 'danger'];
										  }
										},
				'columns' => [
					 [
						'attribute' => 'type_device_by',
						//'value'=>'device.name',
						'content'=>function($data){
								if (isset($data->device->name) and isset($data->made->name) and isset($data->modeldevice->name)) {
									return ''.$data->device->name.'<br/>'.$data->made->name.' '.$data->modeldevice->name; 
								}
						},
					 ],
		            // 'type_device_by',
		            // 'nothealth_by',
		            // 'view',
		            // 'configuration',
		            // 'comment',
		             'price',
		            // 'prepay',
					[
						'attribute' => 'user_by',				
						'label' => 'Мастер',	
						'value'=>'usermas.name',
					 ],
		            // 'marketing_by',
		            // 'discount_order',
					
		            // 'quick',
					 [
						'attribute' => 'user_by_cr',				
						'label' => 'Принял',	
						'content'=>function($data){
								return ''.$data->usercr->name.'<br/>'.date('d.m.Y H:i',$data->date_cr); 
						},
					 ],
		            // 'user_by_up',
		            // 'date_cr',
		            // 'date_up',

		           
		        ],
		    ]); ?>
	    </div>
	</div>
</div>
