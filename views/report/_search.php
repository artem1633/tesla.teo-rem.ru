<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\models\manual\StatusOrder;
use kartik\datetime\DateTimePicker;
use kartik\touchspin\TouchSpin;
//use kartik\widgets\DatePicker;
$session = Yii::$app->session;

/*echo '<br>'.$session['search_id'];
echo '<br>'.$session['search_master'] ;
echo '<br>'.$session['search_texnika'];
echo '<br>'.$session['search_status'] ;
echo '<br>'.$session['search_client'] ;
echo '<br>'.$session['linkTime'] ;
echo '<br>'.$session['search_address'];
echo '<br>'.$session['search_tel'] ;
echo '<br>'.$session['from'] ;
echo '<br>'.$session['to'] ;*/

?>
<div class="client-form">
    <div class="box box-default">   
        <div class="box-body">  

            <?php $form = ActiveForm::begin(); ?>
         
            
                <div class="col-md-5" > 
                    
<?php 
$layout = <<< HTML
                <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">С</span>
                {input1}
                <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">По</span>
                {input2}
                <span class="input-group-addon kv-date-remove" style="background-color: #ecf0f5; width: 50px; color:black;">
                    <i class="glyphicon glyphicon-remove"></i>
                </span>
HTML;
 ?>
                <?php
                    echo DatePicker::widget([
                        'type' => DatePicker::TYPE_RANGE,
                        'name' => 'from',
                        'value' => $post['from'],
                        'name2' => 'to',
                        'value2' => $post['to'],
                        'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                        'layout' => $layout,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                ?>

                </div>
                
                
            
            
            <div class="col-md-3" style="margin-top: -20px;">          
                <?= $form->field($model, 'search_user')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $user,//ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите пользователя',
                        //'multiple' => true,
                        'value' => $post['OrderSearch']['search_user'],
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(''); ?>
            </div>
            
            
                
            <div class="form-group" >
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>                
            </div>  

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
