<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Available */

$this->title = $title. $model->id;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="available-view">
		<div class="box box-default">	
		<div class="box-body">
   <!-- <h1><?= Html::encode($this->title) ?></h1>-->

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
				'attribute' => 'group_parts_by',		
				'value'=>$model->group->name,
			 ],
            [
				'attribute' => 'parts_by',		
				'value'=>$model->parts->name,
			 ],
            [
				'attribute' => 'store_by',		
				'value'=>$model->store->name,
			 ],
            [
				'attribute' => 'status_parts_by',		
				'value'=>$model->status->name,
			 ],
            'price',
            'price_shop',
            'count',
            [
				'attribute' => 'type_parts_by',		
				'value'=>$model->type->name,
			 ],
            'comment',
            [
				'attribute' => 'user_by_cr',		
				'value'=>$model->usercr->name,
			 ],
            [
				'attribute' => 'user_by_up',		
				'value'=>$model->userup->name,
			 ],
			[
				'attribute' => 'date_cr',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
            
        ],
    ]) ?>

</div>
</div>
</div>
