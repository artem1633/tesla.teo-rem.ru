<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use kartik\field\FieldRange;
use kartik\form\ActiveForm;
use kartik\touchspin\TouchSpin;


?>

<div class="available-search"  style="margin-left: 30px;">

    <?php $form = ActiveForm::begin(); ?>

         <div class="row">
             <div class="col-md-5" > 
                 <?= $form->field($model, 'tovar')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->PartList(),
                    'options' => [
                        'placeholder' => 'Выберите товара',
                        //'multiple' => true,
                        'value' => $post['Available']['tovar'],
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <?php if(Yii::$app->user->identity->permission == 'admin' || Yii::$app->user->identity->permission == 'super_admin'){ ?>
            <div class="col-md-5">
                <?= $form->field($model, 'storages')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->StorageList(),
                    'options' => [
                        'placeholder' => 'Выберите склада',
                        //'multiple' => true,
                        'value' => $post['Available']['storages'],
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <?php } ?>
            
            <div class="col-md-2" >
                <div class="form-group" style="margin-top: 25px;">
                    <?= Html::submitButton('Поиск' , ['style'=>' ','class' =>  'btn btn-primary']) ?>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
