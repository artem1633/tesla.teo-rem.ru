<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Available */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="available-form">
		<div class="box box-default">	
		<div class="box-body">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
			<?= $form->field($model, 'parts_by')->widget(Select2::classname(), [
			'data' => $parts,
			'options' => ['placeholder' => 'Выберите ...'],	 
			'pluginEvents' => [
					"change" => "function() 
                    {
						$.getJSON('info',
						{'id':$(this).val()},
						function(data){	
                            $('#supplier').val(data.supplier).trigger('change');				
							$('#price').val(data.price);
							$('#price_shop').val(data.price_shop);
							$('#group_parts_by').val(data.group);
							}      
						);
						
					}",
				],
			'pluginOptions' => [
				'tags' => true,
				'allowClear' => true,],
				
			]);?>
		</div>
		<div class="col-md-5">
			<?= $form->field($model, 'group_parts_by')
				->dropDownList($groupParts ,
				['id'=>'group_parts_by','prompt' => 'Выберите один вариант',
						'onchange'=>'$.get("list",{"id":$(this).val()},
							function(data)
							{$("#parts_by").html(data);}     
						);',
				 
				
				]);	?>
				
		</div><!---->
		
	</div>
	
	<div class="row">
		<div style="display: none;">
				<?= $form->field($model, 'store_by')->dropDownList($store ,[/*'prompt' => 'Выберите мастера',*/ 'id' => 'store_by']);?>	
		</div>	
		<div class="col-md-4">
			<?= $form->field($model, 'storage_id')->dropDownList($storage, [/*'value'=>1, 'prompt' => 'Выберите склада',*/ /*'disabled' => true*/]); ?>
		</div>
		<div class="col-md-5">
			<?= $form->field($model, 'supplier')->widget(Select2::classname(), [
			'data' => $client,
			'options' => ['id' =>'supplier','placeholder' => 'Выберите ...'],	 
			'pluginOptions' => [
				'tags' => true,
				'allowClear' => true,],
				
			]);?>
		</div>
	</div>	

	<div class="row">
		<div class="col-md-4">
				<?= $form->field($model, 'status_parts_by')
					->dropDownList($statusParts ,
					['prompt' => 'Выберите один вариант']);	?>
		</div>			
		<div class="col-md-5">
				<?= $form->field($model, 'type_parts_by')
					->dropDownList($typeParts ,
					['prompt' => 'Выберите один вариант']);	?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
		
			<?= $form->field($model, 'count')->textInput() ?>
		</div>	
		<div class="col-md-3">	
			<?= $form->field($model, 'price')->textInput(['id' => 'price']) ?>
		</div>	
		<div class="col-md-3">	
			<?= $form->field($model, 'price_shop')->textInput(['id' => 'price_shop']) ?>		
		</div>
	</div>
	<div class="row">
		<div class="col-md-9">
			<?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
			
		</div>
	</div>



	<div style="display:none">
	<?= $form->field($model, 'number')->textInput() ?>
	
    <?= $form->field($model, 'user_by_cr')->textInput(['value'=>$user_by_cr_v]) ?>

    <?= $form->field($model, 'user_by_up')->textInput(['value'=>$user_by_up_v]) ?>

     
    <?= $form->field($model, 'date_cr')->textInput(['value'=>$date_cr_v]) ?>

    <?= $form->field($model, 'date_up')->textInput(['value'=>$date_up_v]) ?>
	</div>
	



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::a('Закрыть документ', ['index'], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>