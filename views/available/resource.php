﻿<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AvailableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Availables';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="available-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div id='crud-datatable-pjax'>	
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            'id',
		/*	
			[
				'attribute' => 'group_parts_by',				
				'label' => 'Группа',	
				'value'=>'group.name',
			 ],
			 [
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
			 */
			[
				'attribute' => 'parts_by',				
				'label' => 'Товар',	
				'value'=>'parts.name',
			 ],
			[
				'attribute' => 'store_by',				
				'label' => 'Пользователь',	
				'value'=>'store.name',
			 ],	
			[
				'attribute' => 'storage_id',				
				'label' => 'Склад',	
				'value'=>'storage.name',
			],			
			[
				'attribute' => 'status_parts_by',				
				'label' => 'Статус',	
				'value'=>'status.name',
			 ],
            'count',		
            'price',		
         //   'price_shop',		
			[
				'attribute' => 'type_parts_by',				
				'label' => 'Тип',	
				'value'=>'type.name',
			 ],
            // 'comment',
            // 'user_by_cr',
            // 'user_by_up',
            // 'date_cr',
			[
		        //'class'=>'\kartik\grid\DataColumn',
		        'attribute'=>'move',
		        'label' => 'Перемещение',
		        'content' => function ($data) {
		            return Html::a('<center><i class="glyphicon glyphicon-share" style="font-size: 16px;"></i></center>', 
                        ['/storage/move', 'tovar' => $data->id], [
                        'role'=>'modal-remote', 'title'=>'Переместит',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ]);
		        },
		    ],

            ['class' => 'yii\grid\ActionColumn',
            //'header' => 'Удалить',
			'template' => '{delete}'
			],
        ],
    ]); ?>
	
<?php Pjax::end(); ?></div></div>
