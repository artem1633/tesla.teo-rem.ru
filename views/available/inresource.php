﻿<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AvailableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Availables';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="available-index">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            'id',
          //  'number',
			
			/*[
				'attribute' => 'group_parts_by',				
				'label' => 'Группа',	
				'value'=>'group.name',
			 ],*/
			 /*[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/
			 
			[
				'attribute' => 'parts_by',				
				'label' => 'Товар',	
				'value'=>'parts.name',
			 ],
			[
				'attribute' => 'store_by',				
				'label' => 'Пользователь',	
				'value'=>'store.name',
			 ],
			 [
				'attribute' => 'storage_id',				
				'label' => 'Склад',	
				'value'=>'storage.name',
			],			
			[
				'attribute' => 'status_parts_by',				
				'label' => 'Статус',	
				'value'=>'status.name',
			 ],
            'count',		
            'price',		
            'price_shop',		
			[
				'attribute' => 'type_parts_by',				
				'label' => 'Тип',	
				'value'=>'type.name',
			 ],
			 [
		        //'class'=>'\kartik\grid\DataColumn',
		        'attribute'=>'move',
		        'label' => 'Перемещение',
		        'content' => function ($data) {
		            return Html::a('<center><i class="glyphicon glyphicon-share"></i></center>', 
                        ['/storage/move-available', 'tovar' => $data->id], [
                        'role'=>'modal-remote', 'title'=>'Переместит',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ]);
		        },
		    ],
            // 'comment',
            // 'user_by_cr',
            // 'user_by_up',
            // 'date_cr',
			

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{link} ',
			'buttons' => [
             //   'update' => function ($url, $model, $key){                 
             //      return Html::a('<span class="glyphicon glyphicon-briefcase"></span>', '/g-service/job-list/index?id='.$key);
             //   },
				'link' => function ($url, $model, $key){                 
					if ($model->status_parts_by == 3){
						return Html::a('<span class="glyphicon glyphicon-log-in"></span>', 
							['addresource','id'=>$key],
							[
								'title'=>'Поступило',
								'aria-label'=>'Поступило',
							]);
					}
                        elseif($model->status_parts_by == 4)
                        {
                            return Html::a('<span class="glyphicon glyphicon-eye-open""></span>', 
							['view','id'=>$key],
							[
								'title'=>'Просмотр',
								'aria-label'=>'Просмотр',
							]);
                        }
					
                },
            ],
			],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
