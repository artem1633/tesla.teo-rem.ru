<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
 

/* @var $this yii\web\View */
/* @var $model app\models\Available */

//$this->title = 'Создание Available';
$this->title = $title;
//$this->params['breadcrumbs'][] = ['label' => 'Availables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="available-create">

   <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'parts' 			=> $parts,
		'typeParts' 		=> $typeParts,
		'groupParts' 		=> $groupParts,
		'statusParts' 		=> $statusParts,
		'store' 			=> $store,
		'client' 			=> $client,
		'user_by_cr_v' 		=> $user_by_cr_v,
		'user_by_up_v' 		=> $user_by_up_v,
		'date_cr_v' 		=> $date_cr_v,
		'date_up_v' 		=> $date_up_v,
        'model' 			=> $model,
        'storage'			=> $storage,
    ]) ?>
	
	<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    //   'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			

            'id',
			
			[
				'attribute' => 'group_parts_by',				
				'label' => 'Группа',	
				'value'=>'group.name',
			 ],
			 /*[
				'attribute' => 'date_up',
				'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],*/
			 
			[
				'attribute' => 'parts_by',				
				'label' => 'Товар',	
				'value'=>'parts.name',
			 ],
			[
				'attribute' => 'store_by',				
				'label' => 'Пользователь',	
				'value'=>'store.name',
			 ],	
			[
				'attribute' => 'storage_id',				
				'label' => 'Склад',	
				'value'=>'storage.name',
			],
			[
				'attribute' => 'status_parts_by',				
				'label' => 'Статус',	
				'value'=>'status.name',
			 ],
            'count',		
            'price',		
            'price_shop',		
			[
				'attribute' => 'type_parts_by',				
				'label' => 'Тип',	
				'value'=>'type.name',
			 ],
            // 'comment',
            // 'user_by_cr',
            // 'user_by_up',
            // 'date_cr',
			

            ['class' => 'yii\grid\ActionColumn',
			'template' => '{view}{delete} '
			],
        ],
    ]); ?>
<?php Pjax::end(); ?>

</div>
