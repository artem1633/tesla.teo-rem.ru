<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Available */

//$this->title = 'Изменение Available: ' . $model->id;
$this->title = $title . $model->id;
$this->params['breadcrumbs'][] = ['label' => $label, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'Availables', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="available-update">

 <!--   <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
		'parts' 				=> $parts,
		'typeParts' 		=> $typeParts,
		'groupParts' 		=> $groupParts,
		'statusParts' 		=> $statusParts,
		'store' 				=> $store,
		'user_by_cr_v' 	=> $user_by_cr_v,
		'user_by_up_v' 	=> $user_by_up_v,
		'date_cr_v' 		=> $date_cr_v,
		'date_up_v' 		=> $date_up_v,
        'model' 				=> $model,
    ]) ?>

</div>
