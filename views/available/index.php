<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\tabs\TabsX;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use yii\data\ActiveDataProvider;
use app\models\AvailableSearch;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;

CrudAsset::register($this);
$gridColumn = [
     'id',
     'parts_by',
     'store_by',
     'status_parts_by',
     'count' ,
     'price',
     'type_parts_by',
];
$this->title = 'Склад';
/*$dataProviderWaid = new ActiveDataProvider([
            'query' => AvailableSearch::find(),
            
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),

        ]); 
foreach ($dataProviderWaid->models as  $value) {
   echo "<pre>";
   print_r($value);
   echo "</pre>";
   die;
}*/
?>

<div class="available-index">


       <?= Html::a('Оприходовать', ['create'], ['class' => 'btn btn-success']) ?>
       <?php 
                echo ExportMenu::widget([
                    'dataProvider' => $ostatki,
                    'columns' => $gridColumn
                ]);
            ?>

        
        <?php 

            echo ButtonDropdown::widget([
                'label' => 'Печатать',
                'options' => [
                    'target'=>'_blank',
                    'class' => 'btn-info',
                    'style' => 'margin:2px',
                ],
                'dropdown' => [
                        'items' => [
                            [
                            'label' => 'Остатки',
                            'url' => ['printresource'],
                            'linkOptions' => ['target'=>'_blank'],
                            ],
                            [
                                'label' => 'Ожидает поступления',
                                'url' =>['printinresource'],
                                'linkOptions' => ['target'=>'_blank'],
                            ],
                            [
                                'label' => 'Оприходовано',
                                'url' =>['printavailable'],
                                'linkOptions' => ['target'=>'_blank'],
                            ],
                        ]
                ]   
            ]);
        ?>

 <div class="box box-default">  
    <div class="box-body">  
        <?php /*Html::a('История перемешении', 
            ['/storage/changing',], [
             'title'=>'История',
            'class' => 'btn btn-default',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
        ])*/ ?>
        <?php  echo $this->render('_search', ['model' => new \app\models\Available(), 'post'=>$post]); ?> 
       </div>
    </div>
    <div class="box box-default">  
        <div class="box-body"> 
            <div style="position:relative; left:0px; top:30px"> 
                    <?= 
                        TabsX::widget([
                            'items' => [
                                [
                                    'label' => 'Остатки',
                                    'options' => ['id' => 'tab1'],
            						'content' => $contentResource,
                                ],
                                [
                                    'label' => 'Ожидает поступления',
                                    'options' => ['id' => 'tab3'],
                                    'content' => $contentResourceWaid,
            						//'content' => $this->redirect(['/g-servise/joblist/index', 'id' => $model->id])
            						//'linkOptions'=>['data-url'=>Url::to(['job-list/index?id=7'])]
                                ],
                                [
                                    'label' => 'Оприходовано',
                                    'options' => ['id' => 'tab2'],
                                    'content' => $contentInResource,
            						//'content' => $this->redirect(['/g-servise/joblist/index', 'id' => $model->id])
            						//'linkOptions'=>['data-url'=>Url::to(['job-list/index?id=7'])]
                                ],
                                [
                                    'label' => 'История перемещении',
                                    'options' => ['id' => 'tab4'],
                                    'content' => $this->render('@app/views/storage/changing', [
                                        'result' => $result ,
                                        'model' => $model,
                                        'post' => $post,
                                    ]),
                                    //'content' => $this->redirect(['/g-servise/joblist/index', 'id' => $model->id])
                                    //'linkOptions'=>['data-url'=>Url::to(['job-list/index?id=7'])]
                                ],
                                
                            ],
                        ]);
                    ?>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size"=>"modal-lg",
    //"size" => "modal-wide",
    "options" => [
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>