<?php

namespace app\controllers;

use Yii;
use app\models\Client;
use app\models\ClientSearch;
use app\models\manual\GroupClient;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use app\models\Order;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintlist()
    {
        
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                   
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
          
        $content = $this->renderPartial('@app/views/client/listclient',[
            'dataProvider' => $dataProvider->getModels(),
        ]);
             
         $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);
        
        //echo "<pre>".print_r($pdf,true)."</pre>";
        
        return $pdf->render();
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$model =$this->findModel($id);
		
		$userCr   = User::find()->where(['id'=>$model->user_by_cr])->one();
		$userUp   = User::find()->where(['id'=>$model->user_by_up])->one();
		$groupUs  = GroupClient::find()->where(['id'=>$model->group_by])->one();
		//Создатель и редактор
		if (isset($userCr->name) ){ $model->user_by_cr = $userCr->name;}else{$model->user_by_cr ="Не задан";}
		if (isset($userUp->name) ){ $model->user_by_up = $userUp->name;}else{$model->user_by_up ="Не задан";}
	
	
		 if ($model->type == 'person'){
			$model->type = 'Физ. лицо'; 
		}
		 if ($model->type == 'company'){
			$model->type = 'Юр. лицо'; 
		 }
		 
		 $model->group_by = $groupUs ->name;
		
        return $this->render('view', [
			'title' => 'Изменить клиента: ',
			'label' => 'Клиенты',
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Client();
		$groupClient = ArrayHelper::map(GroupClient::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
			
				'user_by_cr_v' => Yii::$app->user->identity->id,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создать клиента',
				'label' => 'Клиенты',
                'model' => $model,
                'groupClient' => $groupClient,
            ]);
        }
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$groupClient = ArrayHelper::map(GroupClient::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
				'user_by_cr_v' => $model->user_by_cr,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' => $model->date_cr,
                'date_up_v' => time(),
				'title' => 'Просмотр клиента: ',
				'label' => 'Клиенты',
                'model' => $model,
                'groupClient' => $groupClient,
            ]);
        }
    }

    /**
     * Deletes an existing Client model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $order = Order::find()->where(['client_by' => $id])->one();        
        if(isset($order)) {
         \Yii::$app->session->setFlash('error','Вы не можете удалить этот клиент, потому что он прикреплено к заказу');
                return $this->redirect(['index']);         
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
