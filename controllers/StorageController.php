<?php

namespace app\controllers;

use Yii;
use app\models\Storage;
use app\models\StorageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Resource;
use app\models\Move;
use app\models\Available;
/**
 * StorageController implements the CRUD actions for Storage model.
 */
class StorageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Storage models.
     * @return mixed
     */
    public function actionIndex()
    {   
        if(Yii::$app->user->identity->permission != 'admin' && Yii::$app->user->identity->permission != 'super_admin') return $this->redirect(['/indicators/index']);
        $searchModel = new StorageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Storage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Склад #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionChanging()
    {     
        $request = Yii::$app->request;
        $model = new Storage();
        $post = $request->post();

            /*echo "<pre>";
            print_r($post);
            echo "</pre>";*/
            $tovar = $post['Storage']['tovar'];
            $storages = $post['Storage']['storages'];
        if(Yii::$app->user->identity->permission == 'admin'){
            
            if($tovar == null && $storages == null) $result = Move::find()->all(); 
            if($tovar != null && $storages == null) $result = Move::find()->where(['part_id' => $tovar])->all();   
            if($tovar == null && $storages != null) $result = Move::find()->where(['storage_form' => $storages])->all();  
            if($tovar != null && $storages != null) $result = Move::find()->where(['storage_form' => $storages, 'part_id' => $tovar])->all();     
        }
        else{
            $storage = Storage::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
            if($tovar == null) $result = Move::find()->where(['storage_form' => $storage->id])->all();  
            else $result = Move::find()->where(['storage_form' => $storage->id, 'part_id' => $tovar ])->all();  
        }
        return $this->render('changing', [
            'result' => $result ,
            'model' => $model,
            'post' => $post,
        ]);
        
    }

    public function actionMove($tovar)
    {   
        $request = Yii::$app->request;
        $model = new Storage();
        $resource = Resource::findOne($tovar);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){

                if(Yii::$app->user->identity->move != 1 && Yii::$app->user->identity->permission == 'master')
                    return [
                        'title'=> "".'<b>Error</b>',
                        'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить товаров. Потому что для перемещения вам не хватает прав</b></span></center>',                              
                    ];

                $post = $request->post();
                $count = (int)$post['Resource']['move_count'];
                $tovar_id = $post['Resource']['parts_by'];
                $storage_id = $post['Resource']['storage_id'];

                if((int)($resource->count) < $count) {
                    $defis = (int)$count - $resource->count;
                    return [
                        'title'=> "".'<b>Error</b>',
                        'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить '. $count.' товаров. Потому что для перемещения вам не хватает '.$defis.' товаров </b></span></center>',                              
                    ];
                }

                $move = new Move();
                $move->table = 'resource';
                $move->storage_form = $resource->storage_id;
                $move->storage_to = $storage_id;
                $move->old_count = $resource->count;
                $move->sending_count = $count;
                $move->part_id = $resource->parts_by;
                $move->data = date('Y-m-d H:i:s');
                $move->save();

                $new_resource = Resource::find()->where(['parts_by' => $tovar_id, 'storage_id' => $storage_id])->one();
                if($new_resource == null) {
                    $new_resource = new Resource();
                    $new_resource->parts_by = $tovar_id;
                    $new_resource->store_by = (Storage::find()->where(['id' => $storage_id])->one()->user_id);
                    $new_resource->status_parts_by = $resource->status_parts_by;
                    $new_resource->count = $count;
                    $new_resource->price = $resource->price;
                    $new_resource->type_parts_by = $resource->type_parts_by;
                    $new_resource->storage_id = $storage_id;
                    $new_resource->save();
                }
                else{
                    $new_resource->count = $new_resource->count + $count;
                    $new_resource->save();
                }

                $resource->count = $resource->count - $count;
                $resource->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax']; 
            }else{
                 return [
                    'title'=> "Перемешение",
                    'content'=>$this->renderAjax('move', [
                        'model' => $model,
                        'resource' => $resource,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Переместить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*echo "<pre>";
            print_r($request->post());
            echo "</pre>";*/
            return $this->render('move', [
                'model' => $model,
                'resource' => $resource,
            ]);
        }
    }

    public function actionMoveAvailable($tovar)
    {   
        $request = Yii::$app->request;
        $model = new Storage();
        $available = Available::findOne($tovar);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){

                $post = $request->post();
                $count = (int)$post['Available']['move_count'];
                $tovar_id = $post['Available']['parts_by'];
                $storage_id = $post['Available']['storage_id'];

                if(Yii::$app->user->identity->move != 1 && Yii::$app->user->identity->permission == 'master')
                    return [
                        'title'=> "".'<b>Error</b>',
                        'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить товаров. Потому что для перемещения вам не хватает прав</b></span></center>',                              
                    ];

                if((int)($available->count) < $count) {
                    $defis = (int)$count - $available->count;
                    return [
                        'title'=> "".'<b>Error</b>',
                        'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить '. $count.' товаров. Потому что для перемещения вам не хватает '.$defis.' товаров </b></span></center>',                              
                    ];
                }

                $move = new Move();
                if($available->status_parts_by == 3)$move->table = 'available1';
                else $move->table = 'available2';
                $move->storage_form = $available->storage_id;
                $move->storage_to = $storage_id;
                $move->old_count = $available->count;
                $move->sending_count = $count;
                $move->part_id = $available->parts_by;
                $move->data = date('Y-m-d H:i:s');
                $move->save();

                $new_available = Available::find()->where(['parts_by' => $tovar_id, 'storage_id' => $storage_id])->one();
                if($new_available == null) {
                    $new_available = new Available();
                    $new_available->parts_by = $tovar_id;
                    $new_available->store_by = (Storage::find()->where(['id' => $storage_id])->one()->user_id);
                    $new_available->status_parts_by = $available->status_parts_by;
                    $new_available->count = $count;
                    $new_available->price = $available->price;
                    $new_available->type_parts_by = $available->type_parts_by;
                    $new_available->storage_id = $storage_id;
                    $new_available->group_parts_by = $available->group_parts_by;
                    $new_available->supplier = $available->supplier;
                    $new_available->user_by_cr = Yii::$app->user->identity->id;;
                    $new_available->user_by_up = Yii::$app->user->identity->id;;
                    $new_available->date_cr = time();
                    $new_available->date_up = time();
                    $new_available->price_shop = $available->price_shop;
                    $new_available->save();
                }
                else{
                    $new_available->count = $new_available->count + $count;
                    $new_available->save();
                }

                $available->count = $available->count - $count;
                $available->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax']; 
            }else{
                 return [
                    'title'=> "Перемешение",
                    'content'=>$this->renderAjax('move-available', [
                        'model' => $model,
                        'available' => $available,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Переместить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            return $this->render('move-available', [
                'model' => $model,
                'available' => $available,
            ]);
        }
    }

    /**
     * Creates a new Storage model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->permission != 'admin' && Yii::$app->user->identity->permission != 'super_admin' ) return $this->redirect(['/indicators/index']);
        $request = Yii::$app->request;
        $model = new Storage();  
        $model->is_main = 0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить новый склад",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать склад",
                    'content'=>'<span class="text-success">Добавление нового склада успешно завершено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить новый склад",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Storage model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->permission != 'admin') return $this->redirect(['/indicators/index']);
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Склад #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Storage model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Storage model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Storage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Storage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Storage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
