<?php

namespace app\controllers;

use Yii;
use app\models\Cash;
use app\models\CashSearch;
use app\models\User;
use app\models\manual\TypeCash;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use yii\web\ForbiddenHttpException;

/**
 * CashController implements the CRUD actions for Cash model.
 */
class CashController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        	'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    	/*
	Фильтры
	*/
	public function Myfilter($searchId = null,$linkTime = null,$statusSearch = null)
    {
       if($searchId != null){
			if ($searchId == 'week'){//текущая неделя
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx and type_cash_by = :st',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday')
					]);

				}
			}
			
			if ($searchId == 'lastWeek'){//за прошлую неделю
				$day = strtotime('last Monday');//начало текущей недели	
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx and type_cash_by = :st',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday')
					]);
				}
			}
			
			if ($searchId == 'today'){//за сегодня
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr <= :nx and type_cash_by = :st',[':ls' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))),':nx' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))) + 86400,':st' => $statusSearch 
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr <= :nx',[':ls' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))),':nx' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))) + 86400
					]);
				}
			}
			
			if ($searchId == 'yesterday'){//за вчера
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr <= :nx and type_cash_by = :st',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time())),':st' => $statusSearch
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr <= :nx',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time()))
					]);
				}
			}
			
			if ($searchId == 'Month'){//с начало месяца					
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx and type_cash_by = :st',[':ls' => date('Y-m-01'),':nx' => time(),':st' => $statusSearch
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx',[':ls' => date('Y-m-01'),':nx' => time()
					]);

				}
			}
			
			if ($searchId == 'lastMonth'){//За прошлый месяц
				$day = date('m')-1;		
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx and type_cash_by = :st',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr < :nx',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01')
					]);
				}
			}
			
			if ($searchId == 'dinamick'){//с начало месяца
				if($statusSearch != null)
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr <= :nx and type_cash_by = :st',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400,':st' => $statusSearch
					]);
				}
				else
				{
					$query = Cash::find()
					->where(
					'cash.date_cr >= :ls and cash.date_cr <= :nx',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400
					]);
				}
			}
			
			
	
		}else{	
			if ($statusSearch != null){$query = Cash::find()->where(['type_cash_by' => $statusSearch]);}else{$query = Cash::find();}
		}

        /*$query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->andWhere('cash.user_by_cr = user.id');*/
		
		$dataProvider = new ActiveDataProvider([
		'query' => $query,
		
		'sort'=>array(
			'defaultOrder'=>['id' => SORT_DESC],
		),
		'pagination' => [
			'pageSize' => 20,
			'validatePage' => false,
		],

		]);
		
		return $dataProvider;
    }
    /**
     * Lists all Cash models.
     * @return mixed
     */
    public function actionIndex($searchId = null,$linkTime = null,$statusSearch = null)
    {
        $labelId = 'Фильтры';
			if($searchId == 'week'){$labelId = 'С начала недели';}
			if($searchId == 'lastWeek'){$labelId = 'За прошлую неделю';}
			if($searchId == 'today'){$labelId = 'Сегодня';}
			if($searchId == 'yesterday'){$labelId = 'Вчера';}			
			if($searchId == 'Month'){$labelId = 'С начала месяца';}	
			if($searchId == 'lastMonth'){$labelId = 'За прошлый месяц';}
			if($searchId == ''){$labelId = 'За все время';}		
			if($searchId == 'dinamick'){$labelId = 'Фиксированая дата';}
			
		$dataProvider = $this->myfilter($searchId,$linkTime,$statusSearch);
        
        $sum = 0;
		$data = $dataProvider->getModels();
		 foreach($data as $inf)
		{
			$sum += $inf->summa;
		}

        $typeCash = ArrayHelper::map(typeCash::find()->all(), 'id', 'name');
        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'labelId' => $labelId ,
			'typeCash' => $typeCash,
			'statusSearch' => $statusSearch,
			'searchId' => $searchId,
			'linkTime' => $linkTime,
			'sum' => $sum,

        ]);
    }

    /**
     * Displays a single Cash model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		
		$model         = $this->findModel($id);
		$typeCash      = TypeCash::find()->where(['id'=>$model->type_cash_by])->one();
		$userCr 	   = User::find()->where(['id'=>$model->user_by_cr])->one();
		$userUp 	   = User::find()->where(['id'=>$model->user_by_up])->one();
		
				//Создатель и редактор
		if (isset($userCr->name) ){ $model->user_by_cr  = $userCr->name;}else{$model->user_by_cr ="Не задан";}
		if (isset($userUp->name) ){ $model->user_by_up = $userUp->name;}else{$model->user_by_up ="Не задан";}
		
		$model->type_cash_by = $typeCash->name;
		
		if ($model->type == "income" ){ $model->type  = "Приход";}else{$model->type ="Расход";}

			
        return $this->render('view', [
			'title' => 'Просмотр документ: ',
			'label' => 'Касса',
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Cash model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type_cash=null,$sum=null,$type=null,$comment=null)
    {
		
        $model = new Cash();
		$typeCash = ArrayHelper::map(TypeCash::find()->all(), 'id', 'name');		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$model -> type_cash_by = $type_cash;
			$model -> summa = $sum;
			$model -> type = $type;
			$model -> comment = $comment;
			
            return $this->render('create', [
				'user_by_cr_v' => Yii::$app->user->identity->id,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание документ: ',
				'label' => 'Касса',
                'model' => $model,
                'typeCash' => $typeCash,
            ]);
        }
    }

    /**
     * Updates an existing Cash model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$typeCash = ArrayHelper::map(TypeCash::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
				'user_by_cr_v' => $model->user_by_cr,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменить документ: ',
				'label' => 'Касса',
                'model' => $model,
                'typeCash' => $typeCash,
            ]);
        }
    }

    public function actionPrintlist($searchId = null,$linkTime = null,$statusSearch = null)
    {
		
		$dataProvider = $this->myfilter($searchId,$linkTime,$statusSearch);
		
			$sum = 0;
			$dataProvider = $dataProvider->getModels();
			 foreach($dataProvider as $inf)
			{
				$sum += $inf->summa;
			}
			
			Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			$headers = Yii::$app->response->headers;
			$headers->add('Content-Type', 'application/pdf');
			
			$content = $this->renderPartial('@app/views/cash/listcash',[
			'searchId' => $searchId,
			'linkTime' => $linkTime,
			'dataProvider' => $dataProvider,
			'sum' => $sum
			]);
			 
		 $pdf = new Pdf([
			'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
			'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
			'content' => $content,
			'orientation' => Pdf::ORIENT_LANDSCAPE,
		]);
		
		//echo "<pre>".print_r($pdf,true)."</pre>";
		
		return $pdf->render();
    }
    /**
     * Deletes an existing Cash model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cash model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cash the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cash::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
