<?php

namespace app\controllers;

use Yii;
use app\models\JobList;
use app\models\JobListSearch;
use app\models\Order;
use app\models\AboutCompany;
use app\models\manual\Service;
use app\models\manual\Parts;
use app\models\manual\StatusOrder;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use app\models\Available;
use app\models\Resource;

/**
 * JobListController implements the CRUD actions for JobList model.
 */
class JobListController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all JobList models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
		$visualButton = true;
		$sum = 0;

		$statusOrder = ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name');
		$order = Order::find()->where(['id'=>$id])->one();
		
		if ($id > 0)
		{		
			$query = JobList::find()->where(['order_by'=>$id]);			
			$jobList =JobList::find()->where(['order_by'=>$id])->all();
			foreach($jobList as $job){$sum += $job->price * $job->count;}	//считаем сумму по заказу	
			if($order->discount_order > 0)
			{
				$discount = ($sum / 100) * $order->discount_order;
				$sum = $sum - $discount;
			}
			if($order->prepay > 0)
			{
				$sum = $sum - $order->prepay;
			}
		}
		else
		{
			$query = JobList::find();
		}

        $query
            ->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->leftJoin('order','order.id = job_list.order_by')
            ->andWhere('order.user_by_cr = user.id');
		
			
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                'validatePage' => false,
            ],

        ]);

		
		 unset( $statusOrder[7] );
		 unset( $statusOrder[3] );
		
		if (!$id > 0 || $order->status_order	== 7)	{$visualButton = false;}
		
        return $this->render('@app/views/joblist/index', [
			'title' =>  'Заказ #' .$id,
			'label' => 'Заказ',
			'id'	 =>$id,
			'sum' => $sum,
			'visualButton' => $visualButton,
         //   'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'statusOrder' => $statusOrder,
			'order' => $order,
        ]);
    }

    /**
     * Displays a single JobList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JobList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id, $job)
    {
        $model = new JobList();
		
		$order = Order::find()->where(['id'=>$id])->one();
		
		
		
		$service = ArrayHelper::map(Service::find()->all(), 'id', 'name');

        $masters_tovar = Available::find()->where(['store_by' => $order->user_by])->all();
        $tovars_id = [];
        foreach ($masters_tovar as $tovar) {
            $tovars_id [] = $tovar->parts_by;
        }

		//$parts = ArrayHelper::map(Parts::find()->where(['group_by'=>$order->type_device_by, 'id' => array_unique($tovars_id)])->all(), 'id', 'name');
        $parts = ArrayHelper::map(Resource::find()->where([ 'store_by' => $order->user_by ])->all(), 'id', 'parts.name');

		/*echo "<br>".$order->user_by;
        echo "<br>".$order->type_device_by;
        echo "<pre>".print_r($tovars_id,true)."</pre>";*/
		//echo $order->type_device_by;
		//echo "<pre>".print_r($parts,true)."</pre>";
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['order/view', 'id' => $id]);
        } else {
			$model->count = 1;
            return $this->renderAjax ('@app/views/joblist/create', [
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Добавить к заказу: ',
				'label' => 'Список работ',
				'id' =>$id,
                'model' => $model,
                'service' => $service,
                'parts' => $parts,
                'job' => $job,
            ]);
        }
    }

    /**
     * Updates an existing JobList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$order_id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['order/view', 'id' => $order_id]);
        } else {
            return $this->render('@app/views/joblist/update', [
				'title' => '',
				'label' => '',
                'model' => $model,
                'order_id' => $order_id,
            ]);
        }
    }

	/*
	
	Получаем цену услуги или заказа
	*/
	public function actionGetprice($id,$job)
    {
     
		$price = 0;
		
		if($job == "service"){
			 $service =Service::find()->where(['id'=>$id])->one();
			 $price	= $service->price;
		}else{
			 $parts = Resource::find()->where(['parts_by'=>$id])->one();
			 $price	= $parts->price;
		}
	
        return $price;
    }
	
	
	/*
	Печатать документы
	*/
	public function actionPrint($id)
    {
		
		$order = Order::find()->where(['id'=>$id])->one();
		$jobList =JobList::find()->where(['order_by'=>$id])->all();
		$aboutCompany =AboutCompany::find()->where(['id'=>1])->one();
			
		
			Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			$headers = Yii::$app->response->headers;
			$headers->add('Content-Type', 'application/pdf');
			
			$content = $this->renderPartial('@app/views/joblist/rabot',[
			'order' => $order, 
			'jobList' => $jobList,
			'aboutCompany' => $aboutCompany
			]);
			 
		 $pdf = new Pdf([
			'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
			'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
			'content' => $content,
		]);
		
		//echo "<pre>".print_r($pdf,true)."</pre>";
		
		return $pdf->render();
    }
    /**
     * Deletes an existing JobList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$job = JobList::find()->where(['id'=>$id])->one();

        $this->findModel($id)->delete();
		
		//echo "<pre>".print_r($_POST,true)."</pre>";	
       return $this->redirect(['order/view','id' => $job->order_by]);
    }

    /**
     * Finds the JobList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JobList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JobList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
