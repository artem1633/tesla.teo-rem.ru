<?php

namespace app\controllers;

use app\models\manual\Parts;
use Yii;
use app\models\manual\StatusParts;
use app\models\manual\StatusPartsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatusPartsController implements the CRUD actions for StatusParts model.
 */
class StatusPartsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all StatusParts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StatusPartsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/manual/statusparts/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StatusParts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StatusParts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StatusParts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/manual/statusparts/create', [
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание статуса запчасей',
				'label' => 'Статусы запчасей',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StatusParts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/manual/statusparts/update', [
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменение статуса запчасей:',
				'label' => 'Статусы запчасей',
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StatusParts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!empty(StatusParts::DEFAULT_STATUSES[$id])){
            \Yii::$app->session->setFlash('error', 'Данный статус невозможно удалить');
        }else{
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the StatusParts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StatusParts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StatusParts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
