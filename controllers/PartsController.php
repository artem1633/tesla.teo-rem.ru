<?php

namespace app\controllers;

use Yii;
use app\models\manual\Parts;
use app\models\manual\PartsSearch;
use app\models\manual\TypeDevice;
use app\models\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PartsController implements the CRUD actions for Parts model.
 */
class PartsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Parts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartsSearch();
        //$typeParts = new TypeParts();
        $groupParts = ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/manual/parts/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'groupParts'   => $groupParts,
        ]);
    }

    /**
     * Displays a single Parts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@app/views/manual/parts/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Parts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Parts();
		$groupParts = ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
		$client 		= ArrayHelper::map(Client::find()->where(['group_by' => 6])->all(), 'id', 'name');
				
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/manual/parts/create', [
			    'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание',
                'model' => $model,
                'groupParts' => $groupParts,
                'client' => $client,
            ]);
        }
    }

    /**
     * Updates an existing Parts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model 		= $this->findModel($id);
		$groupParts 	= ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
		$client 			= ArrayHelper::map(Client::find()->where(['group_by' => 6])->all(), 'id', 'name');
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('@app/views/manual/parts/update', [
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменение',
                'model' => $model,
                'groupParts' => $groupParts,
                'client' => $client,
            ]);
        }
    }

    /**
     * Deletes an existing Parts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Parts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Parts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
