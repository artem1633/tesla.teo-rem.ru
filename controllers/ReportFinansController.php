<?php

namespace app\controllers;

use app\models\OrderSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;

class ReportFinansController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @return string
     */
    public function actionIndex($dateEnd = null, $dateStart = null)
    {

        return $this->render('index', [
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    public function actionReklama($reklama = null, $dateEnd = null, $dateStart = null)
    {

        return $this->render('reklama', [
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'reklama' => $reklama,
        ]);
    }


    /**
     * @return string
     */
    public function actionProfit($dateEnd = null, $dateStart = null)
    {
        return $this->render('profit', [
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

}
