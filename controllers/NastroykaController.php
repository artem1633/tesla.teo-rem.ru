<?php

namespace app\controllers;

use Yii;
use app\models\Nastroyka;
use app\models\NastroykaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NastroykaController implements the CRUD actions for Nastroyka model.
 */
class NastroykaController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Nastroyka models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NastroykaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Nastroyka model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Nastroyka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Nastroyka();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionChangesms()
    {
        $model = new Nastroyka();

        if ($model->load(Yii::$app->request->post())) {
            $parol = Nastroyka::findOne(1);
            
            if($model->stariy_sms_parol != $parol->sms_parol){
                \Yii::$app->session->setFlash('error', 'Неправильный старый пароль');
                    /*return $this->render('change_sms', [
                    'model' => $model,
                ]);*/
                return $this->redirect(['sending-sms/index']);
            }
            else {
                Yii::$app->db->createCommand()->update('nastroyka', ['sms_parol' => $model->sms_parol_retry], [ 'id' => 1 ])->execute();
            }
            return $this->redirect(['sending-sms/index']);
        } else {
            return $this->renderAjax('change_sms', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Nastroyka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Nastroyka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nastroyka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nastroyka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nastroyka::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
