<?php

namespace app\controllers;

use app\models\LogsSearch;
use Yii;
use yii\web\Controller;
use app\models\Settings;
use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class StatisticController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogsSearch();
        $dataProviderAuthorized = $searchModel->getAuthorized(Yii::$app->request->queryParams);
        $dataProviderRegistered = $searchModel->getRegistered(Yii::$app->request->queryParams);
        $dataProviderOpenedRegistrationPage = $searchModel->getOpenedRegistrationPage(Yii::$app->request->queryParams);

        $searchModel->load(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProviderAuthorized' => $dataProviderAuthorized,
            'dataProviderRegistered' => $dataProviderRegistered,
            'dataProviderOpenedRegistrationPage' => $dataProviderOpenedRegistrationPage,
        ]);

    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->isSuperAdmin() === false)
            throw new ForbiddenHttpException('Доступ запрещен');

        return parent::beforeAction($action);
    }
}
