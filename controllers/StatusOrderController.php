<?php

namespace app\controllers;

use app\models\Order;
use app\models\Statistics;
use Yii;
use app\models\manual\StatusOrder;
use app\models\manual\StatusOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatusOrderController implements the CRUD actions for StatusOrder model.
 */
class StatusOrderController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all StatusOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StatusOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/manual/statusorder/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StatusOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

     public function actionView1()
    {
        return $this->renderAjax('@app/views/manual/statusorder/view1', [
            //'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new StatusOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StatusOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //Yii::$app->db->createCommand()->update('template', ['tekst' => $model->shablon], [ 'id' => 3 ])->execute();
            
            return $this->redirect(['index', 'id' => $model->id]);
        } else {$model->tekst = "Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания Цифровые технологии";
            return $this->render('@app/views/manual/statusorder/create', [
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание статуса заказа',
				'label' => 'Статусы заказов',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StatusOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           // Yii::$app->db->createCommand()->update('template', ['tekst' => $model->shablon], [ 'id' => 3 ])->execute();
            return $this->redirect(['index', 'id' => $model->id]);
        } else {//$model->shablon = Template::findOne(3)->tekst;
            return $this->render('@app/views/manual/statusorder/update', [
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменение статуса заказа:',
				'label' => 'Статусы заказов',
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StatusOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $order = Order::findOne(['status_order' => $id]);
        if(!empty($order) || !empty(StatusOrder::DEFAULT_STATUSES[$id])){
            \Yii::$app->session->setFlash('error', 'Данный статус невозможно удалить');
        }else{
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the StatusOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StatusOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StatusOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
