<?php

namespace app\controllers;

use Yii;
use app\models\ShopList;
use app\models\manual\Parts;
use app\models\Resource;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ShopListController implements the CRUD actions for ShopList model.
 */
class ShopListController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all ShopList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ShopList::find(),
        ]);

        return $this->render('@app/views/shoplist/index', [
			'title' => '',
			'label' => '',
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@app/views/shoplist/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $model = new ShopList();
		$parts = ArrayHelper::map(Parts::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['shop/create',
			'id' => $id
			]);
        } else {
			$model->count = 1;
            return $this->renderAjax('@app/views/shoplist/create', [
				'title' => '',
				'label' => '',
                'model' => $model,
                'parts' => $parts,
                'id' 	=> $id,
            ]);
        }
    }

    /**
     * Updates an existing ShopList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/shoplist/update', [
				'title' => '',
				'label' => '',
                'model' => $model,
            ]);
        }
    }
	
	/*
	Получаем ценну на товар
	*/
	public function actionPrice($id)
    {
		//echo "sadasdasdasdasd";
       	$resource = Resource::find()->where(['parts_by'=>$id])->one();
        return $resource->price;
    }	
	/*
	Получаем остаток на складе на товар
	*/
	public function actionCount($id, $count)
    {
       	$resource = Resource::find()->where(['parts_by'=>$id])->one();
		if(isset($resource->count)){
			if ($resource->count >= $count)
			{
				return true;	//все норм
			}
			else
			{
				return false; //слишком мало
			}
		}
		else
		{
		   return false;//если нет на складе вообще
		}
    }

    /**
     * Deletes an existing ShopList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$shopList = ShopList::find()->where(['id'=>$id])->one();
				
		$resource = Resource::find()->where(['parts_by'=>$shopList->parts_by])->one();
		if($resource != null){
            $resource->count = $resource->count  + $shopList->count;		
	       	$resource->save();		
        }

		
        $this->findModel($id)->delete();
		
		//echo "<pre>".print_r($_POST,true)."</pre>";	
       return $this->redirect(['shop/create','id' => $shopList->shop_by]);
    }

    /**
     * Finds the ShopList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
