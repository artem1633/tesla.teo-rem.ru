<?php

namespace app\controllers;

use Yii;
use app\models\manual\Made;
use app\models\manual\MadeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MadeController implements the CRUD actions for Made model.
 */
class MadeController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Made models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MadeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/manual/made/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Made model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@app/views/manual/made/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Made model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Made();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/manual/made/create', [
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание производителя',
				'label' => 'Производители',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Made model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/manual/made/update', [
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменение производителя',
				'label' => 'Производители',
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Made model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Made model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Made the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Made::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
