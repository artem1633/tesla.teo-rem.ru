<?php

namespace app\controllers;

use Yii;
use app\models\manual\Marketing;
use app\models\manual\MarketingSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MarketingController implements the CRUD actions for Marketing model.
 */
class MarketingController extends BaseController
{
    /**
     * @inheritdoc
     */
	public $title = 'Реклама';

    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Marketing models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MarketingSearch();
		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		
		
        return $this->render('@app/views/manual/marketing/index', [
			'title' => $this->title,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Marketing model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@app/views/manual/marketing/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Marketing model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Marketing();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
			 return $this->render('@app/views/manual/marketing/create', [
			    'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Marketing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('@app/views/manual/marketing/update', [
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Marketing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Marketing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Marketing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Marketing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
