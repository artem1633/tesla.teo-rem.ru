<?php

namespace app\controllers;

use Yii;
use app\models\SendingSms;
use app\models\SendingSmsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AboutCompany;
use app\models\Client;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use app\models\Nastroyka;
use app\models\SmsObject;


/**
 * SendingSmsController implements the CRUD actions for SendingSms model.
 */
class SendingSmsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all SendingSms models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SendingSmsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintlist()
    {
        //echo "1"; 
        $query = SendingSms::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);       
        $dataProvider = $dataProvider->getModels();
           //echo "1"; 
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
            
        $content = $this->renderPartial('@app/views/sending-sms/listshop',[
            'dataProvider' => $dataProvider,
        ]);
           
        $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);
        
        return $pdf->render();
    }

    /**
     * Displays a single SendingSms model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SendingSms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SendingSms();

        if ($model->load(Yii::$app->request->post())) {
            $model->count = 0;
            $model->created_date = date('Y-m-d');
            $model->update_date = date('Y-m-d');
            $model->save();
             /*$post = Yii::$app->request->post();
             echo "<pre>";
             print_r($post);
             echo "</pre>";
             die;*/

            $post = Yii::$app->request->post('SmsObject');

            if (is_array($post['Клиенты']))
            {
                $post = $post['Клиенты'];

                foreach ($post as $key => $item)
                {
                    $smsobject = new SmsObject();
                    $smsobject->sms_id = $model->id;
                    $smsobject->user_id = $item;
                    $smsobject->save();
                }
            }
            //$this->actionParol($model->id);
           /* return $this->render('parol', [
                'id' => $model->id,
            ]);*/
            return $this->redirect(['parol?id='.$model->id]);
            
        } else {$model->tekst = "{name}";
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionParol($id = null)
    {

        $model = new Nastroyka();

        if ($model->load(Yii::$app->request->post())) {
          
           $parol = Nastroyka::findOne(1);//echo "<pre>";print_r($parol);die;
           if($model->sms_parol == $parol->sms_parol) {
                $this->actionSendsms($model->sms_id);                 
            }
           else {
             \Yii::$app->session->setFlash('error', 'Неправильный пароль');
                //return $this->redirect(['index']);
                return $this->redirect(['parol?id='.$model->sms_id]);
           }
                       
        } else { $model->sms_id = $id; 
            return $this->render('passwording', [
                'model' => $model,
                
            ]);
        }
    }

    public function actionSendsms($id)
    {
        $model = SendingSms::findOne($id);        
        $model->count = $model->count + 1;
        $about = AboutCompany::find()->where(['id'=>1])->one();

        $sms_user_id = SmsObject::find()->where(['sms_id' => $id])->all();
        $selected = [];
        foreach ($sms_user_id as $items)
        {
            $selected[] = $items->user_id;
        }
/*echo "<pre>";
            print_r($sms_user_id);
            echo "</pre>";die;*/
        $query = Client::find()->where([ 'id' => $selected, ]);        
        $dataProvider = new ActiveDataProvider(['query' => $query,]);
        
        $message = $model->tekst;
        $result_tekst = "";
       
        foreach ($dataProvider->models as $client) {
           
            //file_get_contents("http://smsc.ru/sys/send.php?login=".$about->smslog."&psw=".$about->smspas."&phones=".$client->phone."&mes=".$message."&charset=utf-8");  

            //$url_get = 'http://smsc.ru/sys/balance.php?login=s'.$about->smslog.'&psw='.$about->smspas;
            //$message = \yii\helpers\Html::encode($message);
            $page = str_replace ("{name}", $client->name, $message);
           /* echo "mes = ".$page;
            die;*/
            $url_get = 'http://smsc.ru/sys/send.php?login='.$about->smslog.'&psw='.$about->smspas.'&phones='.$client->phone.'&mes='.$page.'&charset=utf-8';                
            $ch = curl_init();  
            curl_setopt($ch,CURLOPT_URL,$url_get);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $result=curl_exec($ch);
            $r=json_decode($result,TRUE);
            //echo "result = ".$r;die;
            $result_tekst = $result_tekst.'  '. $r. '\n';
            curl_close($ch);

        }     
        $model->status = $result_tekst;
        $model->update_date = date('Y-m-d');
        $model->save();  
        return $this->redirect(['index']);         
    }

    /**
     * Updates an existing SendingSms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SendingSms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SendingSms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SendingSms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SendingSms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
