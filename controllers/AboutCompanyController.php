<?php

namespace app\controllers;

use Yii;
use app\models\AboutCompany;
use app\models\AboutCompanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AboutCompanyController implements the CRUD actions for AboutCompany model.
 */
class AboutCompanyController extends BaseController
{
	
	public $defaultAction = 'view';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all AboutCompany models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AboutCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //return $this->render('@app/views/aboutcompany/index', [
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AboutCompany model.
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('@app/views/aboutcompany/view', [
            'model' => $this->findModel(\Yii::$app->user->getIdentity()->company_id),
        ]);
    }

    /**
     * Creates a new AboutCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AboutCompany();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/aboutcompany/create', [
				'title' => '',
				'label' => '',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AboutCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

        if($model->login != null && $model->login != "")  Yii::$app->db->createCommand()->update('about_company', ['smslog' => $model->login], [ 'id' => $model->id ])->execute();
        if($model->parol != null && $model->parol != "") Yii::$app->db->createCommand()->update('about_company', ['smspas' => $model->parol], [ 'id' => $model->id ])->execute();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/aboutcompany/update', [
				'title' => '',
				'label' => '',
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AboutCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AboutCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AboutCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AboutCompany::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
