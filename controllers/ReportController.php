<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\OrderSearch;
use app\models\manual\TypeOrder;
use app\models\manual\Nothealth;
use app\models\manual\TypeDevice;
use app\models\User;
use app\models\Client;
use app\models\manual\Marketing;
use app\models\manual\StatusOrder;
use app\models\manual\Made;
use app\models\manual\ModelDevice;
use app\models\JobList;
use app\models\Resource;
use app\models\AboutCompany;
use app\models\manual\Service;
use app\models\manual\Parts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;


/**
 * OrderController implements the CRUD actions for Order model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        	'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



		/*
	Фильтры
	*/
	public function Myfilter($searchId = null,$linkTime = null,$statusSearch = null,$userSearch = null)
    {
       if($searchId != null){
			if ($searchId == 'week'){//текущая неделя
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx and status_order = :st',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday')
					]);

				}
			}

			if ($searchId == 'lastWeek'){//за прошлую неделю
				$day = strtotime('last Monday');//начало текущей недели
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx and status_order = :st',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday')
					]);
				}
			}

			if ($searchId == 'today'){//за сегодня
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr <= :nx and status_order = :st',[':ls' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))),':nx' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))) + 86400,':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr <= :nx',[':ls' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))),':nx' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))) + 86400
					]);
				}
			}

			if ($searchId == 'yesterday'){//за вчера
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr <= :nx and status_order = :st',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time())),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr <= :nx',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time()))
					]);
				}
			}

			if ($searchId == 'Month'){//с начало месяца
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx and status_order = :st',[':ls' => date('Y-m-01'),':nx' => time(),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx',[':ls' => date('Y-m-01'),':nx' => time()
					]);

				}
			}

			if ($searchId == 'lastMonth'){//За прошлый месяц
				$day = date('m')-1;
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx and status_order = :st',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr < :nx',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01')
					]);
				}
			}

			if ($searchId == 'dinamick'){//с начало месяца
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr <= :nx and status_order = :st',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400,':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'date_cr >= :ls and date_cr <= :nx',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400
					]);
				}
			}



		}else{
			if ($statusSearch != null){$query = OrderSearch::find()->where(['status_order' => $statusSearch]);}else{$query = OrderSearch::find();}

		}

		if (isset($userSearch)){$query->andFilterWhere(['user_by_cr' => $userSearch]);}
		$query->andFilterWhere(['status_order' => 7]);
		$dataProvider = new ActiveDataProvider([
		'query' => $query,

		'sort'=>array(
			'defaultOrder'=>['status_order' => SORT_ASC,],
		),
		'pagination' => [
			'pageSize' => 20,
			'validatePage' => false,
		],

		]);

		return $dataProvider;
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionFinance($timeStart = null,$timeEnd = null,$userSearch=null)
    {
    	$post = Yii::$app->request->post();	
		/*echo "<pre>";
        print_r($post);
        echo "</pre>";*/
        $userSearch = $post['OrderSearch']['search_user'];
        
        if($post['from'] != null) $timeStart = $post['from'];
        else { $timeStart =  date('Y-m-d 00:00:00'); $post['from'] = date('Y-m-d'); }

        if($post['to'] != null) $timeEnd = $post['to'];
        else { $timeEnd = date('Y-m-d 23:59:59'); $post['to'] = date('Y-m-d'); }


		$infoUser = User::find()->where(['id'=>$userSearch])->one();

		$query = OrderSearch::find();		
		$query = OrderSearch::find()
					->where(
					'order.date_up >= :ls and order.date_up < :nx',[':ls'=> strtotime($timeStart),':nx' => strtotime($timeEnd),
					]);

		if (isset($userSearch)){
			if ($infoUser->permission == "master"){
				$query->andFilterWhere(['order.user_by' => $userSearch]);
			}else{
				$query->andFilterWhere(['order.user_by_cr' => $userSearch]);
			}
		}

        $query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->andWhere('order.user_by_cr = user.id');

		$query->andFilterWhere(['status_order' => 7]);

		$dataProvider = new ActiveDataProvider([
		'query' => $query,

		'sort'=>array(
			'defaultOrder'=>['status_order' => SORT_ASC,],
		),
		'pagination' => [
			'pageSize' => 20,
			'validatePage' => false,
		],

		]);

		$dataProvider1 = new ActiveDataProvider([
		'query' => $query,
		'pagination' => false,

		]);

		$sum = 0;
		$sum2 = 0;
		 foreach($dataProvider1->getModels() as $inf)
		{
			//echo '<br/>'.JobList::find()->where(['order_by'=>$inf->id])->sum('price');
			$sum2 += $inf->price;
			$sum += JobList::find()->where(['order_by'=>$inf->id,'type'=>'service'])->sum('price');
			//echo $sum;
		}
		//echo $sum;
		$protect = 0;
		if (isset($infoUser->percent_finish)){
			$protect = $sum / 100;
			$protect = $infoUser->percent_finish * $protect;
			//echo $protect;
		}


		$model = new OrderSearch();
	//$userManeger = ArrayHelper::map(User::find()->where(['permission'=>'maneger'])->all(), 'id', 'name');
		$user = ArrayHelper::map(user::find()->all(), 'id', 'name');
		$statusOrder = ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name');
		return $this->render('finance', [
			'dataProvider' => $dataProvider,
			'timeStart' => $timeStart ,
			'timeEnd' => $timeEnd,
			'user' => $user,
			'userSearch' => $userSearch,
			'sum' => $sum2,
			'protect' => $protect,
			'post' => $post,
			'model' => $model,
		]);



    }





}
