<?php

namespace app\controllers;

use app\models\Statistics;

class IndicatorsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],

        ], parent::behaviors());
    }

    public function actionIndex()
    {
        $statistics = new Statistics(\Yii::$app->user->getIdentity()->company);
        $statistics->init();
        return $this->render('index', [
            'data' => $statistics->getData()
        ]);
    }

}
