<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		
		
		$model =$this->findModel($id);
		
		$userCr = User::find()->where(['id'=>$model->user_by_cr])->one();
		$userUp = User::find()->where(['id'=>$model->user_by_up])->one();
		//Создатель и редактор
		if (isset($userCr->name) ){ $model->user_by_cr = $userCr->name;}else{$model->user_by_cr ="Не задан";}
		if (isset($userUp->name) ){ $model->user_by_up = $userUp->name;}else{$model->user_by_up ="Не задан";}
	
		 
		 
		if ($model->permission == 'admin'){
			$model->permission = 'Администратор'; 
		 }
		  if ($model->permission == 'maneger'){
			$model->permission  = 'Менеджер'; 
		 }
		  if ($model->permission == 'master'){
			$model->permission = 'Мастер'; 
		 }
		 
		 if ($model->status == '1'){
			$model->status = 'Доступ'; 
		 }else{
			$model->status = 'Без доступа';  
		 }
		 

       return $this->render('view', [
			'title' => 'Просмотр сотрудника: ',
			'label' => 'Сотрудники',
            'model' => $model,
       ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
				'user_by_cr_v' => Yii::$app->user->identity->id,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создать сотрудника',
				'label' => 'Сотрудники',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			//echo "<pre>".print_r($model,true)."</pre>";
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
				'user_by_cr_v' => $model->user_by_cr,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' => $model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменить сотрудника: ',
				'label' => 'Сотрудники',
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
