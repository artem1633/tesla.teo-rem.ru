<?php

namespace app\controllers;

use Yii;
use app\models\Shop;
use app\models\ShopList;
use yii\data\ActiveDataProvider;
use app\models\ShopSearch;
use app\models\Available;
use app\models\Resource;
use app\models\Client;
use app\models\User;
use app\models\manual\Parts;
use app\models\AboutCompany;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * ShopController implements the CRUD actions for Shop model.
 */
class ShopController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }




    public function Myfilter($searchId = null,$linkTime = null)
    {
        if($searchId != null){
            if ($searchId == 'week'){//текущая неделя
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr < :nx',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday')
                    ]);
            }

            if ($searchId == 'lastWeek'){//за прошлую неделю
                $day = strtotime('last Monday');//начало текущей недели
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr < :nx',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday')
                    ]);
            }

            if ($searchId == 'today'){//за сегодня
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr <= :nx',[':ls' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))),':nx' => strtotime(date('Y.m.d 00:00:00',time('Y.m.d'))) + 86400
                    ]);
            }

            if ($searchId == 'yesterday'){//за вчера
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr <= :nx',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time()))
                    ]);
            }

            if ($searchId == 'Month'){//с начало месяца
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr < :nx',[':ls' => date('Y-m-01'),':nx' => time()
                    ]);
            }

            if ($searchId == 'lastMonth'){//За прошлый месяц
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr < :nx',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01')
                    ]);
            }

            if ($searchId == 'dinamick'){//с начало месяца
                $query = Shop::find()
                    ->where(
                        'shop.date_cr >= :ls and shop.date_cr <= :nx',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400
                    ]);
            }



        }else{
            $query = Shop::find();

        }

        /*$query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->andWhere('shop.user_by_cr = user.id');*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]);

        return $dataProvider;
    }
    /**
     * Lists all Shop models.
     * @return mixed
     */
    public function actionIndex($searchId = null,$linkTime = null)
    {
//        if (Yii::$app->user->isGuest)
//        {
//            return $this->redirect(['site/login']);
//		}
//

        $labelId = 'Фильтры';
        if($searchId == 'week'){$labelId = 'С начала недели';}
        if($searchId == 'lastWeek'){$labelId = 'За прошлую неделю';}
        if($searchId == 'today'){$labelId = 'Сегодня';}
        if($searchId == 'yesterday'){$labelId = 'Вчера';}
        if($searchId == 'Month'){$labelId = 'С начала месяца';}
        if($searchId == 'lastMonth'){$labelId = 'За прошлый месяц';}
        if($searchId == ''){$labelId = 'За все время';}
        if($searchId == 'dinamick'){$labelId = 'Фиксированая дата';}

        $dataProvider = $this->myfilter($searchId,$linkTime);

        $sum = 0;
        $data = $dataProvider->getModels();
        foreach($data as $inf)
        {
            $sum += $inf->price;
        }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'labelId' => $labelId ,
            'searchId' => $searchId,
            'linkTime' => $linkTime,
            'sum' => $sum,
        ]);
    }

    /**
     * Displays a single Shop model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);


        $userCr   = User::find()->where(['id'=>$model->user_by_cr])->one();
        $userUp   = User::find()->where(['id'=>$model->user_by_up])->one();

        //Создатель и редактор
        if (isset($userCr->name) ){ $model->user_by_cr = $userCr->name;}else{$model->user_by_cr ="Не задан";}
        if (isset($userUp->name) ){ $model->user_by_up = $userUp->name;}else{$model->user_by_up ="Не задан";}

        $clienN  = Client::find()->where(['id'=>$model->client_by])->one();
        $model->client_by = $clienN->name;
        $parts  = Parts::find()->where(['id'=>$model->story_by])->one();
        $model->client_by = $clienN->name;


        $sum = 0;
        $shopList = ShopList::find()->where(['shop_by'=>$id])->all();
        foreach($shopList as $shop){$sum += $shop->price * $shop->count;}

        //echo "<pre>".print_r($shopList,true)."</pre>";

        $dataProvider = new ActiveDataProvider([
            'query' => ShopList::find()->where(['shop_by'=>$id]),

            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                'validatePage' => false,
            ],

        ]);


        //	echo "<pre>".print_r(,true)."</pre>";

        return $this->render('view', [

            'title' => 'Документ: ',
            'label' => 'Магазин',
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Shop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
//        return var_dump('test');
        $kolichestvo = Client::find()->where("id > 0")->count();
        if($kolichestvo == 0){ \Yii::$app->session->setFlash('error', 'Вы эту операцию не можете выполнять. Потому что, на системе нет клиенты'); return $this->redirect(['index']);
        }
        //echo "id=".$id;

        $visualButton = true;
        $sum = 0;

        if ($id > 0)
        {
            $query = ShopList::find()->where(['shop_by'=>$id]);
            $shopList =ShopList::find()->where(['shop_by'=>$id])->all();
            foreach($shopList as $shop){$sum += $shop->price * $shop->count;}	//считаем сумму по заказу

        }
        else
        {
            $model = Shop::find()->orderBy(['id'=>SORT_DESC])->one();

            $dataProvider = new ActiveDataProvider([
                'query' => ShopList::find()->where(['shop_by' => $model->id]),

                'sort'=>array(
                    'defaultOrder'=>['id' => SORT_DESC],
                ),
                'pagination' => [
                    'pageSize' => 10,
                    'validatePage' => false,
                ],
            ]);

            if ($dataProvider->getCount() > 0){
                //создание нового документа для привязки списка товара
                $timeSearch = time();
                $shop   	= new Shop();
                $shop->client_by = 13;
                $shop->price = 0;
                $shop->count = 1;
                $shop->komment = '';                
                $shop->status = 'create';
                $shop->user_by_cr = Yii::$app->user->identity->id;
                $shop->user_by_up = Yii::$app->user->identity->id;
                $shop->date_cr = $timeSearch;
                $shop->date_up = $timeSearch;

                //return var_dump('log point 2');

                if($shop->save()) {
                    $id = $shop->id;
                    $query = ShopList::find()->where(['shop_by' => $id]);
                }
                else{
                    echo "<pre>".print_r($shop,true)."</pre>";
                }
            }
            else
            {
                $id = $model->id;
                $query = ShopList::find()->where(['shop_by' => $id]);
            }
        }

        if(isset($id))
            $model = Shop::find()->where(['id'=>$id])->one();
        else{
            $model = new Shop();
            $timeSearch = time();
            
            $model->client_by = 13;
            $model->price = 0;
            $model->count = 1;
            $model->komment = '';    
            $model->status = 'create';
            $model->user_by_cr = Yii::$app->user->identity->id;
            $model->user_by_up = Yii::$app->user->identity->id;
            $model->date_cr = $timeSearch;
            $model->date_up = $timeSearch;

            if($model->save()) {
                $id = $model->id;
                $query = ShopList::find()->where(['shop_by' => $id]);
            }
        }

        $client = ArrayHelper::map(Client::find()->all(), 'id', 'name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                'validatePage' => false,
            ],

        ]);



        //echo "<pre>".print_r($available,true)."</pre>";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return var_dump('log point');
            //$sum = $model->price * $model->count;
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'user_by_cr_v' 	=> Yii::$app->user->identity->id,
                'user_by_up_v' 	=> Yii::$app->user->identity->id,
                'date_cr_v' 	=> time(),
                'date_up_v' 	=> time(),
                'title' 		=> 'Продажа: ',
                'label' 		=> 'Магазин',
                'model'		 	=> $model,
                'dataProvider' 	=> $dataProvider,
                'client' 		=> $client,
                'id' 			=> $id,
                'sum' 			=> $sum,
            ]);
        }
    }

    /**
     * Updates an existing Shop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'title' => '',
                'label' => '',
                'model' => $model,
            ]);
        }
    }

    public function actionPrintlist($searchId = null,$linkTime = null)
    {

        $dataProvider = $this->myfilter($searchId,$linkTime);

        $sum = 0;
        $dataProvider = $dataProvider->getModels();
        foreach($dataProvider as $inf)
        {
            $sum += $inf->price;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        $content = $this->renderPartial('@app/views/shop/listshop',[
            'searchId' => $searchId,
            'linkTime' => $linkTime,
            'dataProvider' => $dataProvider,
            'sum' => $sum
        ]);

        $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);

        //echo "<pre>".print_r($pdf,true)."</pre>";

        return $pdf->render();
    }
    /*

    Проверяем остатке на скледе и отправляем ответ достаточно или нет
    */
    public function actionLimit($count,$res)
    {
        $resource   = Resource::find()->where(['id'=>$res])->one();

        if($resource ->count >= $count){
            return  true;
        }else{
            return  false;
        }
    }

    public function actionPrice($res)
    {
        $resource   = Resource::find()->where(['id'=>$res])->one();

        if($resource->parts ->price > 0){
            return  $resource->parts ->price;
        }else{
            return  0;
        }
    }
    public function actionClientchange($id,$client)
    {
        $shop = Shop::find()->where(['id'=>$id])->one();
        $shop->client_by = $client;

        if(!$shop->save()){
            echo "<pre>".print_r($shop,true)."</pre>";
        }
    }

    /*
    Печатать документы
    */
    public function actionPrint($id)
    {

        $shop = Shop::find()->where(['id'=>$id])->one();
        $shopList =ShopList::find()->where(['shop_by'=>$id])->all();
        $aboutCompany =AboutCompany::find()->where(['id'=>1])->one();


        //echo "<pre>".print_r($shopList,true)."</pre>";

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        $content = $this->renderPartial('@app/views/shop/check',[
            'shop' => $shop,
            'shopList' => $shopList,
            'aboutCompany' => $aboutCompany
        ]);

        $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
        ]);


        return $pdf->render();
    }
    /**
     * Deletes an existing Shop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $shopList =ShopList::find()->where(['shop_by'=>$id])->all();
        foreach($shopList as $shop)
        {
            $resource = Resource::find()->where(['parts_by'=>$shop->parts_by])->one();
            if($resource != null) {
                $resource->count = $resource->count  + $shop->count;
                $resource->save();
                $shop->delete();
            }
        }

        $this->findModel($id)->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the Shop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shop::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
