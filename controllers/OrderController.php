<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\OrderSearch;
use app\models\manual\TypeOrder;
use app\models\manual\Nothealth;
use app\models\manual\TypeDevice;
use app\models\User;
use app\models\Client;
use app\models\manual\Marketing;
use app\models\manual\StatusOrder;
use app\models\manual\Made;
use app\models\manual\ModelDevice;
use app\models\JobList;
use app\models\Resource;
use app\models\AboutCompany;
use app\models\manual\Service;
use app\models\manual\Parts;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use app\models\Template;
use app\models\Cash;
use app\models\Barcode;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        	'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function Myfilter($client, $texnika, $master, $fio, $searchId = null, $linkTime = null, $statusSearch = null)
    {
       if($searchId != null){
			if ($searchId == 'week'){//текущая неделя
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx and order.status_order = :st',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx',[':ls' => strtotime('last Monday'),':nx' => strtotime('next Monday')
					]);

				}
			}
			
			if ($searchId == 'lastWeek'){//за прошлую неделю
				$day = strtotime('last Monday');//начало текущей недели	
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx and order.status_order = :st',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx',[':ls' => strtotime('last Monday',$day),':nx' => strtotime('last Monday')
					]);
				}
			}
			
			if ($searchId == 'today'){//за сегодня
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr <= :nx and order.status_order = :st',[':ls' => strtotime(date('Y-m-d 00:00:00',time('Y-m-d'))),':nx' => strtotime(date('Y-m-d 00:00:00',time('Y-m-d'))) + 86400,':st' => $statusSearch 
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr <= :nx',[':ls' => strtotime(date('Y-m-d 00:00:00',time('Y-m-d'))),':nx' => strtotime(date('Y-m-d 00:00:00',time('Y-m-d'))) + 86400
					]);
				}
			}
			
			if ($searchId == 'yesterday'){//за вчера
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr <= :nx and order.status_order = :st',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time())),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr <= :nx',[':ls' => strtotime(date("Y-m-d 00:00:00", time())) - 86400,':nx' => strtotime(date("Y-m-d 00:00:00", time()))
					]);
				}
			}
			
			if ($searchId == 'Month'){//с начало месяца					
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx and order.status_order = :st',[':ls' => date('Y-m-01'),':nx' => time(),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx',[':ls' => date('Y-m-01'),':nx' => time()
					]);

				}
			}
			
			if ($searchId == 'lastMonth'){//За прошлый месяц
				$day = date('m')-1;		
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx and order.status_order = :st',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01'),':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr < :nx',[':ls' => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)),':nx' => date('Y-m-01')
					]);
				}
			}
			
			if ($searchId == 'dinamick'){//с начало месяца
				if($statusSearch != null)
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr <= :nx and order.status_order = :st',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400,':st' => $statusSearch
					]);
				}
				else
				{
					$query = OrderSearch::find()
					->where(
					'order.date_cr >= :ls and order.date_cr <= :nx',[':ls' => strtotime($linkTime),':nx' => strtotime($linkTime) + 86400
					]);
				}
			}
			
			
	
		}else{	
			if ($statusSearch != null){$query = OrderSearch::find()->where(['status_order' => $statusSearch]);}else{$query = OrderSearch::find();}
			
		}

        $query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->andWhere('order.user_by_cr = user.id');

        if ($fio) {
            $query->andFilterWhere(['order.id' => $fio]);
        }

//		$query->joinWith('user');
		$query->joinWith('type_device');
		$query->joinWith('client');

        if ($master) {
			$query->andFilterWhere(['like', 'user.id', $master]);
        }
        
        if($texnika){
        	$query->andFilterWhere(['like', 'type_device.name', $texnika]);
        }

        if($client){
        	$query->andFilterWhere(['like', 'client.name', $client]);
        }



		$dataProvider = new ActiveDataProvider([
		'query' => $query,
		
		'sort'=>array(
			'defaultOrder'=>['id' => SORT_DESC,],
		),
		'pagination' => [
			'pageSize' => 20,
			'validatePage' => false,
		],

		]);
		
		return $dataProvider;
    }
	

    public function actionIndex()
    {
    	$post = Yii::$app->request->post();

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->filtr(Yii::$app->request->queryParams,$post);

        $sum = 0;
		$data = $dataProvider->getModels();
		 foreach($data as $inf)
		{
			$sum += $inf->price;
		}

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'post' => $post,
            'sum' => $sum,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		
		
/////////////////Для информации о работе///////////////////////////////////////////////////////////////////////
		$visualButton = true;
		$sum = 0;	
		$statusOrder = ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name');
		$order = Order::find()->where(['id'=>$id])->one();
		
		
		if ($id > 0)
		{		
			$query = JobList::find()->where(['order_by'=>$id]);			
			$jobList =JobList::find()->where(['order_by'=>$id])->all();
			foreach($jobList as $job){$sum += $job->price * $job->count;}	//считаем сумму по заказу	
			if($order->discount_order > 0)
			{
				$discount = ($sum / 100) * $order->discount_order;
				$sum = $sum - $discount;
			}
			if($order->prepay > 0)
			{
				$sum = $sum - $order->prepay;
			}
		}
		else
		{
			$query = JobList::find();
		}
		
			
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' 		=> 10,
                'validatePage' 	=> false,
            ],

        ]);

		
		 unset( $statusOrder[7] );
		 unset( $statusOrder[3] );
		
		if (!$id > 0 || $order->status_order == 7)	{$visualButton = false;}
		
		
       $contentJob = $this->renderPartial('job', [
			'title' =>  'Заказ #' .$id,
			'label' => 'Заказ',
			'id'	=>$id,
			'sum' 	=> $sum,
			'visualButton' 	=> $visualButton,
            'dataProvider' 	=> $dataProvider,
			'statusOrder' 	=> $statusOrder,
			'order' 		=> $order,
			'order_id'		=>$id,
        ]);
////////////////////////////////////////////////////////////////////////////////////////////////
		$model = $this->findModel($id);
		
		$typeOrder = ArrayHelper::map(TypeOrder::find()->all(), 'id', 'name');
		$notHealth = ArrayHelper::map(Nothealth::find()->all(), 'id', 'name');
		$notHealth = ArrayHelper::map(Nothealth::find()->all(), 'id', 'name');
		$typeDevice = ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
		$madeDevice = ArrayHelper::map(Made::find()->all(), 'id', 'name');
		$modelDevice = ArrayHelper::map(ModelDevice::find()->all(), 'id', 'name');
		$user = ArrayHelper::map(User::find()->where(['permission'=>'master'])->all(), 'id', 'name');
		$marketing = ArrayHelper::map(Marketing::find()->all(), 'id', 'name');
		$statusOrder = ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name');
		$clients = ArrayHelper::map(Client::find()->all(), 'id', 'name');
		
		if ($model->return_waid > 0){$model->return_waid = date('Y-m-d H:i', $model->return_waid);}else{$model->return_waid ="Не задан";}
		if ($model->return_date > 0){$model->return_date = date('Y-m-d H:i', $model->return_date);}else{$model->return_date ="Заказ не завершен";}
				
		$contentOrder = $this->renderPartial('_form', [
			'model' => $model,
			'title' => 'Просмотр заказа: ',
			'label' => 'Заказы',
			'typeOrder' => $typeOrder,
			'madeDevice' => $madeDevice,
			'modelDevice' => $modelDevice,
			'notHealth' => $notHealth,
			'typeDevice' => $typeDevice,
			'user' => $user,
			'clients' => $clients,
			'marketing' => $marketing,
			'statusOrder' => $statusOrder,
			'user_by_up_v' => Yii::$app->user->identity->id,
			'user_by_cr_v' => $model->user_by_cr,
			'date_cr_v' =>$model->date_cr,
			'date_up_v' => time(),
		]);
		
///////////////////////////////////////////////////////////////////////////////////////////////////
          return $this->render('view', [
			'title' => 'Просмотр заказа: ',
			'label' => 'Заказы',
            'id' => $id,
			'contentJob' => $contentJob,
			'contentOrder' => $contentOrder,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionLists($id)
    {
        
    	//echo "<option value = '".$data->id."'>".$data->name."</option>" ;
    	echo "id=".$id;
               
    }

    public function actionCreate()
    {
        $model = new Order();
		$typeOrder = ArrayHelper::map(TypeOrder::find()->all(), 'id', 'name');
		$notHealth = ArrayHelper::map(Nothealth::find()->all(), 'id', 'name');
		$notHealth = ArrayHelper::map(Nothealth::find()->all(), 'id', 'name');
		$typeDevice = ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
		$madeDevice = ArrayHelper::map(Made::find()->all(), 'id', 'name');
		$modelDevice = ArrayHelper::map(ModelDevice::find()->all(), 'id', 'name');
		$user = ArrayHelper::map(User::find()->where(['permission'=>'master'])->all(), 'id', 'name');
		$marketing = ArrayHelper::map(Marketing::find()->all(), 'id', 'name');
		$statusOrder = ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name');
		$clients = ArrayHelper::map(Client::find()->all(), 'id', 'name');
		$model->return_waid = date('Y-m-d H:i');
		

		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
		/*	if($model->client_address == null) {
				$client = Client::find()->where(['id'=> $model->client_by ])->one();
				$model->client_address = $client->adress;
			}
						
			$model->return_date = time();
			echo "address = ".$client->adress."<br>";
			echo "<pre>";
			print_r($model);
			echo "</pre>";
			die;*/
			

			return $this->redirect(['view', 'id' => $model->id]);
			//return $this->redirect(['index']);
        } else {
			
			$model->type_order_by = 6;
			$model->model_device_by = 8;
			$model->status_order = 3;
			$model->discount_order = 0;
			$model->marketing_by = 18;
			$model->view = "Царапины, сколы";
			
            return $this->render('create', [
						
				'user_by_cr_v' => Yii::$app->user->identity->id,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>time(),
                'date_up_v' => time(),
				'title' => 'Создание заказа: ',
				'label' => 'Заказы',
                'model' => $model,
				'typeOrder' => $typeOrder,
				'madeDevice' => $madeDevice,
				'modelDevice' => $modelDevice,
				'notHealth' => $notHealth,
				'typeDevice' => $typeDevice,
				'user' => $user,
				'clients' => $clients,
				'marketing' => $marketing,
				'statusOrder' => $statusOrder,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$typeOrder = ArrayHelper::map(TypeOrder::find()->all(), 'id', 'name');
		$notHealth = ArrayHelper::map(Nothealth::find()->all(), 'id', 'name');
		$notHealth = ArrayHelper::map(Nothealth::find()->all(), 'id', 'name');
		$typeDevice = ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
		$madeDevice = ArrayHelper::map(Made::find()->all(), 'id', 'name');
		$modelDevice = ArrayHelper::map(ModelDevice::find()->all(), 'id', 'name');
		$user = ArrayHelper::map(User::find()->all(), 'id', 'name');
		$marketing = ArrayHelper::map(Marketing::find()->all(), 'id', 'name');
		$statusOrder = ArrayHelper::map(StatusOrder::find()->all(), 'id', 'name');
		$clients = ArrayHelper::map(Client::find()->all(), 'id', 'name');
		
		
		
		
		
		$stariy_data = $model->return_date;
		$status = $model->status_order;
        if ($model->load(Yii::$app->request->post())) {
        	if($status == 7)$model->return_date = $stariy_data;
        	$model->save();
        	
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$model->return_waid = date('Y-m-d H:i', $model->return_waid);
			$model->return_date = date('Y-m-d H:i', $model->return_date);
            return $this->render('update', [
				'user_by_cr_v' => $model->user_by_cr,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
				'title' => 'Изменение заказа: ',
				'label' => 'Заказы',
                'model' => $model,
				'typeOrder' => $typeOrder,
				'madeDevice' => $madeDevice,
				'modelDevice' => $modelDevice,
				'notHealth' => $notHealth,
				'typeDevice' => $typeDevice,
				'user' => $user,
				'clients' => $clients,
				'marketing' => $marketing,
				'statusOrder' => $statusOrder,
            ]);
        }
    }

	/*
	Проверяем скидку на работу
	*/
	public function actionMydiscount($id)
    {
       	$client = Client::find()->where(['id'=>$id])->one();
        return $client->discount_order;
    }
		/*
	Подгружаем номер телефона
	*/
	public function actionMyphone($id)
    {
       	$client = Client::find()->where(['id'=>$id])->one();
        return $client->phone;
    }	
    public function actionMyaddress($id)
    {
       	$client = Client::find()->where(['id'=>$id])->one();
        return $client->adress;
    }
    public function actionMymenu($id)
    {
       $session = Yii::$app->session;
    	$menu = isset($_SESSION['menu']) ? $_SESSION['menu'] : null;
    	if($menu == null) $_SESSION['menu'] = 'large';
    	else {
    		if($menu == 'large') $_SESSION['menu'] = 'small';
    		else $_SESSION['menu'] = 'large';
    	}
    }		
	/*
	Меняем статус заказа
	*/

    public function actionNewstatus($idOrder, $newSum, $status_order)
    {
		Yii::$app->db->createCommand()->update('order', ['status_order' => $status_order], [ 'id' => $idOrder ])->execute();		
		$sum = 0;
		$jobList =JobList::find()->where(['order_by'=>$idOrder])->all();
		foreach($jobList as $job){$sum += $job->price * $job->count;}
		Yii::$app->db->createCommand()->update('order', ['price' => $sum], [ 'id' => $idOrder ])->execute();	
        $this->actionSetCash($idOrder);
        $this->SendStatus($status_order,$order->phone,$order->id,$order->price);    
		$this->redirect(['view','id' => $idOrder]);
    }

    public function actionSetCash($idOrder)
    {
    	$jobListService = JobList::find()->where(['order_by'=>$idOrder, 'type' => 'service'])->all();
    	$sum = 0;
		foreach($jobListService as $job){$sum += $job->price * $job->count;}
		if($jobListService != null)
		{
			$cash = new Cash();
			$cash->type_cash_by = 7;
			$cash->summa = $sum;
			$cash->comment = ' ';
			$cash->user_by_cr = Yii::$app->user->identity->id;
			$cash->user_by_up = Yii::$app->user->identity->id;
			$cash->date_cr = time();
			$cash->date_up = time();
			$cash->type = 'income';
			$cash->save();
		}

		$jobListParts =JobList::find()->where(['order_by'=>$idOrder, 'type' => 'parts'])->all();
		$sum = 0;
		foreach($jobListParts as $job){$sum += $job->price * $job->count;}

		if($jobListParts != null)
		{
			$cash = new Cash();
			$cash->type_cash_by = 6;
			$cash->summa = $sum;
			$cash->comment = ' ';
			$cash->user_by_cr = Yii::$app->user->identity->id;
			$cash->user_by_up = Yii::$app->user->identity->id;
			$cash->date_cr = time();
			$cash->date_up = time();
			$cash->type = 'income';
			$cash->save();
		}
    }

    public function SendStatus($idStatus = null,$tel,$idOrder = 0,$sum = 0)
    {
		$status = StatusOrder::find()->where(['id'=>$idStatus])->one();
        if ($status->push){
			$about = AboutCompany::find()->where(['id'=>1])->one();
			//$message = "Ваш заказ №".$idOrder." ".$status->name." ".$status->tekst.", сумма ремонта ".$sum." руб. Компания Цифровые технологии";
			//$shablon = \app\models\Template::find()->where(['id' => 3])->one();
			$page = $status->tekst;
			$page = str_replace ("{order_id}", $idOrder, $page);
			$page = str_replace ("{status_name}", $status->name, $page);
			//$page = str_replace ("{status_tekst}", $status->tekst, $page);
			$page = str_replace ("{summa}", $sum, $page);

			//file_get_contents("http://smsc.ru/sys/send.php?login=".$about->smslog."&psw=".$about->smspas."&phones=".$tel."&mes=".$message."&charset=utf-8");  
			file_get_contents("http://smsc.ru/sys/send.php?login=".$about->smslog."&psw=".$about->smspas."&phones=".$tel."&mes=".$page."&charset=utf-8");  
        }

    }
	
	/*
	
	Получаем цену услуги или заказа
	*/
	public function actionGetprice($id,$job)
    {
     
		$price = 0;
		
		if($job == "service"){
			 $service =Service::find()->where(['id'=>$id])->one();
			 $price	= $service->price;
		}else{
			 $parts = Resource::find()->where(['parts_by'=>$id])->one();
			 $price	= $parts->price;
		}
	
        return $price;
    }
	
	/*
	Печатать документы
	*/
	public function actionPrint0($id)
    {
    	//на этом функции создается строка АКТ ВЫПОЛНЕННЫХ РАБОТ и отправляется на ckeditor.
		$model = Order::findOne($id);
		$model->akt = $this->GetRabot($id);
		$template = Template::find()->where(['key' => 'done', 'company_id' => Yii::$app->user->identity->company_id ])->one();
		if($template->dostup == 0) return $this->actionPrint($model);
		else return $this->render('@app/views/order/_form2', ['model' => $model,]);		 
		
    }

     public function GetRabot($order_id)
    {
    	$a1 = $order_id;
		$l = strlen($a1);
		$a2 = '0000000000000';
		$a3 = substr_replace($a2, $a1, 13 - $l, $l);
		$barcode = new Barcode($a3 , 1);
		$barcode->saving();

    	$order = Order::find()->where(['id'=>$order_id])->one();
		$jobList =JobList::find()->where(['order_by'=>$order_id])->all();
		$aboutCompany =AboutCompany::find()->where(['id'=> Yii::$app->user->identity->company_id ])->one();	

		$shablon = $template = Template::find()->where(['key' => 'done', 'company_id' => Yii::$app->user->identity->company_id ])->one();;
		$page = $shablon->tekst;
		$page = str_replace ("{company_name}", $aboutCompany->name, $page);
		$page = str_replace ("{company_adress}", $aboutCompany->adress, $page);
		$page = str_replace ("{company_phone}", $aboutCompany->phone, $page);
		$page = str_replace ("{mode_of_operation}", "Режим работы", $page);
		$page = str_replace ("{template_name}", "Акт приемки на ремонт", $page);
		$page = str_replace ("{akt_number}", $order->id, $page);
		$page = str_replace ("{akt_date}", date('Y.m.d H:i', time()), $page);

		$page = str_replace ("{client_name}", $order->client->name, $page);
		$page = str_replace ("{client_phone}",$order->client->phone, $page);
		$page = str_replace ("{device}", $order->device->name, $page);
		$page = str_replace ("{device_made_name}", $order->made->name, $page);
		$page = str_replace ("{device_modeldevice_name}", $order->modeldevice->name, $page);
		$page = str_replace ("{nothealth_name}",$order->nothealth->name, $page);
		$page = str_replace ("{discount_order}",$order->discount_order, $page);
		$page = str_replace ("{prepay}",$order->prepay, $page);
		$page = str_replace ("{warranty_period}","Гарантийный период", $page);
		$content = $this->renderPartial('@app/views/order/rabot',[
            'order' => $order,
            'jobList' => $jobList,
            'aboutCompany' => $aboutCompany
		]);
		$page = str_replace ("{table}",$content, $page); 
		$page = str_replace ("{client_signature}","Подпись клиента", $page);
		$page = str_replace ("{employee_signature}","Подпись сотрудника", $page);
		$page = str_replace ("{order_usermas_name}",$order->usermas->name, $page);
		$page = str_replace ("{barcode}",Html::img('barcode.png', ['style' => '']), $page);
		
		//echo "u=".$page;die;
		return $page;

    }
	public function actionPrint($model = null)
    {//здесь создается pdf файли	
    	
    	if($model == null){

	    	$model = new Order();
	    	if ($model->load(Yii::$app->request->post())) {

				$mpdf = new \Mpdf\Mpdf();
		        $mpdf->WriteHTML($model->akt);
		        $mpdf->SetWatermarkText('');
		        $mpdf->showWatermarkText = true;
		        $mpdf->watermarkTextAlpha = 0.1;

		        $mpdf->Output("phpflow.pdf", 'F');
		        $mpdf->Output();
	    	}
	    }
	    else {
		$mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($model->akt);
        $mpdf->SetWatermarkText('');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;

        $mpdf->Output("phpflow.pdf", 'F');
        $mpdf->Output();
	    	/*Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			$headers = Yii::$app->response->headers;
			$headers->add('Content-Type', 'application/pdf');									 
			$pdf = new Pdf([
				'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
				'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
				'content' => $model->akt,
			]);			
			return $pdf->render();*/
	    }
	
		//echo "<pre>".print_r($pdf,true)."</pre>";
		
    }
	
	public function actionPrintlist($searchId = null,$linkTime = null,$statusSearch = null)
    {
		
		$dataProvider = $this->myfilter($searchId,$linkTime,$statusSearch,null);
		
			$sum = 0;
			$dataProvider = $dataProvider->getModels();
			 foreach($dataProvider as $inf)
			{
				$sum += $inf->price;
			}
			
			Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			$headers = Yii::$app->response->headers;
			$headers->add('Content-Type', 'application/pdf');
			
			$content = $this->renderPartial('@app/views/order/listorder',[
			'searchId' => $searchId,
			'linkTime' => $linkTime,
			'dataProvider' => $dataProvider,
			'sum' => $sum
			]);
			 
		 $pdf = new Pdf([
			'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
			'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
			'content' => $content,
			'orientation' => Pdf::ORIENT_LANDSCAPE,
		]);
		
		//echo "<pre>".print_r($pdf,true)."</pre>";
		
		return $pdf->render();
    }

    public function actionPrintOrder($id)
    {
    	$order = Order::find()->where(['id'=>$id])->one();
    	$a1 = $id;
		$l = strlen($a1);
		$a2 = '0000000000000';
		$a3 = substr_replace($a2, $a1, 13 - $l, $l);
		$barcode = new Barcode($a3 , 1);
		$barcode->saving();

		$shablon = Template::find()->where(['key' => 'order_form', 'company_id' => Yii::$app->user->identity->company_id ])->one();;
		$page = $shablon->tekst;
		$page = str_replace ("{order_usermas_name}", $order->user->name, $page);
		$page = str_replace ("{tipzakaza}", $order->typeorder->name, $page);
		$page = str_replace ("{akt_date}", date('Y.m.d H:i', time()), $page);
		$page = str_replace ("{client_name}", $order->client->name, $page);
		$page = str_replace ("{client_phone}",$order->client->phone, $page);
		$page = str_replace ("{device}", $order->device->name, $page);
		$page = str_replace ("{device_made_name}", $order->made->name, $page);
		$page = str_replace ("{model}", $order->modeldevice->name, $page);
		$page = str_replace ("{nothealth_name}",$order->nothealth->name, $page);
		$page = str_replace ("{discount_order}",$order->discount_order, $page);
		$page = str_replace ("{prepay}",$order->prepay, $page);
		$page = str_replace ("{configuration}",$order->configuration, $page);
		$page = str_replace ("{appearance}",$order->view, $page);
		$page = str_replace ("{price}",$order->price, $page);
		$page = str_replace ("{comment}",$order->comment, $page);
		$page = str_replace ("{reklama}",$order->marketing->name, $page);
		$page = str_replace ("{barcode}",Html::img('barcode.png', ['style' => '']), $page);

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		$headers = Yii::$app->response->headers;
		$headers->add('Content-Type', 'application/pdf');
	
			 
		$pdf = new Pdf([
			'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
			'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
			'content' => $page,
			'orientation' => Pdf::ORIENT_LANDSCAPE,
		]);		
		return $pdf->render();
    }
	
	/*
	Печатать документы
	*/
	public function actionPrintcreate0($id)
    {
		//на этом функции создается строка АКТ ПРИЁМА и отправляется на ckeditor.

		$model = Order::findOne($id);
		$model->akt = $this->GetActCreate($id);
		$template = $template = Template::find()->where(['key' => 'acceptance', 'company_id' => Yii::$app->user->identity->company_id ])->one();
		if($template->dostup == 0) return $this->actionActcreate($model);
		else return $this->render('@app/views/order/_form3', [
			    'model' => $model,
        ]);	
    }

    public function GetActCreate($order_id)
    {
    	$a1 = $order_id;
		$l = strlen($a1);
		$a2 = '0000000000000';
		$a3 = substr_replace($a2, $a1, 13 - $l, $l);
		$barcode = new Barcode($a3 , 1);
		$barcode->saving();

    	$order = Order::find()->where(['id'=>$order_id])->one();
		$jobList =JobList::find()->where(['order_by'=>$order_id])->all();
		$aboutCompany = AboutCompany::find()->where(['id'=> Yii::$app->user->identity->company_id ])->one();

		$shablon = Template::find()->where(['key' => 'acceptance', 'company_id' => Yii::$app->user->identity->company_id ])->one();;
		$page = $shablon->tekst;
		$page = str_replace ("{company_name}", $aboutCompany->name, $page);
		$page = str_replace ("{company_adress}", $aboutCompany->adress, $page);
		$page = str_replace ("{company_phone}", $aboutCompany->phone, $page);
		$page = str_replace ("{mode_of_operation}", "Режим работы", $page);
		$page = str_replace ("{template_name}", "Акт приемки на ремонт", $page);
		$page = str_replace ("{akt_number}", $order->id, $page);
		$page = str_replace ("{akt_date}", date('Y.m.d H:i', time()), $page);

		$page = str_replace ("{client_name}", $order->client->name, $page);
		$page = str_replace ("{client_phone}",$order->client->phone, $page);
		$page = str_replace ("{device}", $order->device->name.' '.$order->made->name.' '.$order->modeldevice->name, $page);
		$page = str_replace ("{configuration}",$order->configuration, $page);
		$page = str_replace ("{appearance}",$order->view, $page);
		$page = str_replace ("{defect}",$order->nothealth->name, $page);

		$page = str_replace ("{approx_data}",$order->return_waid, $page);
		$page = str_replace ("{approx_cost}",$order->price, $page);
		$page = str_replace ("{preorder}",$order->prepay, $page);
		$page = str_replace ("{notes}",$order->comment, $page);

		$page = str_replace ("{usercr_name}",$order->usercr->name, $page);
		$page = str_replace ("{client_name}",$order->client->name, $page);
		$page = str_replace ("{barcode}",Html::img('barcode.png', ['style' => '']), $page);

		return $page;

    }

    public function actionActcreate($model = null)
    {	
    	//здесь создается pdf файли	
    	if($model == null){
		 	$model = new Order();

	    	if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
				$headers = Yii::$app->response->headers;
				$headers->add('Content-Type', 'application/pdf');
									 
				 $pdf = new Pdf([
					'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
					'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
					'content' => $model->akt,
				]);
			
				return $pdf->render();
	    	}
	    }
	    else {
	    	Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			$headers = Yii::$app->response->headers;
			$headers->add('Content-Type', 'application/pdf');									 
			$pdf = new Pdf([
				'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
				'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
				'content' => $model->akt,
			]);			
			return $pdf->render();
	    }
    }
    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
		$jobList =JobList::find()->where(['order_by'=>$id])->all();
		foreach($jobList as $list)
		{
			if ($list->type == "parts"){
				$resource = Resource::find()->where(['parts_by'=>$list->job_by])->one();
				if($resource != null){
					$resource->count = $resource->count  + $list->count;		
					$resource->save();
				}
				$list->delete();
			}
		}
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	
    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
