<?php

namespace app\controllers;

use Yii;
use app\models\Available;
use app\models\AvailableSearch;
use app\models\User;
use kartik\mpdf\Pdf;
use app\models\Client;
use app\models\manual\Parts;
use app\models\manual\TypeParts;
use app\models\manual\TypeDevice;
use app\models\manual\StatusParts;
use app\models\Store;
use app\models\Resource;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use app\models\Storage;
use app\models\Move;



/**
 * AvailableController implements the CRUD actions for Available model.
 */
class AvailableController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge([
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
			[
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['info'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Available models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $company = \Yii::$app->user->getIdentity()->company;
        $model = new Storage();
        $post = $request->post();
        $tovar = $post['Available']['tovar'];
        $storages = $post['Available']['storages'];
        $tip = Yii::$app->user->identity->permission;
        $user_id = Yii::$app->user->identity->id;
		//--------------------------------------------------------------------//
        if($tip == 'admin' || $tip == 'super_admin') {
            //$query = AvailableSearch::find()->where(['status_parts_by' => 3,])->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id');
            if($tovar == null && $storages == null) $query = AvailableSearch::find()->where(['status_parts_by' => 3,])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/;
            if($tovar != null && $storages == null) $query = AvailableSearch::find()->where(['status_parts_by' => 3, 'parts_by' => $tovar])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/;
            if($tovar == null && $storages != null) $query = AvailableSearch::find()->where(['status_parts_by' => 3, 'storage_id' => $storages])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/;
            if($tovar != null && $storages != null) $query = AvailableSearch::find()->where(['status_parts_by' => 3, 'parts_by' => $tovar, 'storage_id' => $storages])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/;
        }
        if($tip == 'master'){
            $storage = Storage::find()->where(['user_id' => $user_id])->one();
            //$query = AvailableSearch::find()->where(['status_parts_by' => 3, 'storage_id' => $storage->id])->leftJoin('user', ['company' => $company ])->andWhere('available.user_by_cr = user.id');
            if($tovar == null) $query = AvailableSearch::find()->where(['status_parts_by' => 3, 'storage_id' => $storage->id])/*->leftJoin('user', ['company' => $company ])->andWhere('available.user_by_cr = user.id')*/;
            else $query = AvailableSearch::find()->where(['status_parts_by' => 3, 'storage_id' => $storage->id, 'parts_by' => $tovar])/*->leftJoin('user', ['company' => $company ])->andWhere('available.user_by_cr = user.id')*/;
        }

        $dataProviderWaid = new ActiveDataProvider([
           'query' => $query,			
            'sort'=>[
                'defaultOrder'=>['id' => SORT_DESC],
            ],
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]);	

		$contentInResourceWaid = $this->renderPartial('inresource', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProviderWaid,
        ]);


		//////////////////////////////---------------------------------------------//
        
		if($tip == 'admin' || $tip == 'super_admin') {
            $query = AvailableSearch::find();
            //$query = AvailableSearch::find()->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id');
            //if($tovar == null && $storages == null) $query = AvailableSearch::find()/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/
            //if($tovar != null && $storages == null) $query = AvailableSearch::find()->where(['parts_by' => $tovar])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/
            //if($tovar == null && $storages != null) $query = AvailableSearch::find()->where(['storage_id' => $storages])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/
            //if($tovar != null && $storages != null) $query = AvailableSearch::find()->where(['parts_by' => $tovar, 'storage_id' => $storages])/*->leftJoin('user', [ 'company' => $company ])->andWhere('available.user_by_cr = user.id')*/

            //$query = AvailableSearch::find()->where(['parts_by' => $tovar, 'storage_id' => $storages]);
            $query = AvailableSearch::find()->where(['or', ['parts_by' => $tovar], ['storage_id' => $storages]]);
        }
        if($tip == 'master'){
            $storage = Storage::find()->where(['user_id' => $user_id])->one();
            //$query = AvailableSearch::find()->where([ 'storage_id' => $storage->id])->leftJoin('user', [ 'company' => $company])->andWhere('available.user_by_cr = user.id');
            if($tovar == null) $query = AvailableSearch::find()->where([ 'storage_id' => $storage->id])/*->leftJoin('user', [ 'company' => $company])->andWhere('available.user_by_cr = user.id')*/;
            else $query = AvailableSearch::find()->where(['parts_by' => $tovar, 'storage_id' => $storage->id])/*->leftJoin('user', [ 'company' => $company])->andWhere('available.user_by_cr = user.id')*/;
        }

        $dataProvider = new ActiveDataProvider([
           'query' => $query,
			
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]);	
        
		$contentInResource = $this->renderPartial('inresource', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		//--------------------------------------------------------------------------------------------////
        if($tip == 'admin' || $tip == 'super_admin') {
            $query = Resource::find(); 
            /*if($tovar == null && $storages == null) $query = Resource::find(); 
            if($tovar != null && $storages == null) $query = Resource::find()->where(['parts_by' => $tovar]); 
            if($tovar == null && $storages != null) $query = Resource::find()->where(['storage_id' => $storages]); */
            /*if($tovar != null && $storages != null) $query = Resource::find()->where(['parts_by' => $tovar,'storage_id' => $storages]);*/
            $query->andFilterWhere(['or', ['parts_by' => $tovar], ['storage_id' => $storages]]);
        }
        if($tip == 'master'){
            $storage = Storage::find()->where(['user_id' => $user_id])->one();
            //$query = Resource::find()->where([ 'storage_id' => $storage->id]);
            /*if($tovar == null) $query = Resource::find()->where([ 'storage_id' => $storage->id]);
            else $query = Resource::find()->where([ 'storage_id' => $storage->id, 'parts_by' => $tovar]);*/
            $query = Resource::find()->where(['or', ['parts_by' => $tovar], ['storage_id' => $storage->id]]);
        }
		$dataInResurs = new ActiveDataProvider([
           'query' => $query,
			
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]);
		
		$contentResource = $this->renderPartial('resource', [
            'dataProvider' => $dataInResurs,
        ]);
		///////////////////////////---------------------------------------------------------//
        if($tip == 'admin' || $tip == 'super_admin') {
            
            if($tovar == null && $storages == null) $result = Move::find()->orderBy(['id'=>SORT_DESC])->all(); 
            if($tovar != null && $storages == null) $result = Move::find()->where(['part_id' => $tovar])->orderBy(['id'=>SORT_DESC])->all();   
            if($tovar == null && $storages != null) $result = Move::find()->where(['storage_form' => $storages])->orderBy(['id'=>SORT_DESC])->all();  
            if($tovar != null && $storages != null) $result = Move::find()->where(['storage_form' => $storages, 'part_id' => $tovar])->orderBy(['id'=>SORT_DESC])->all();     
        }
        else{
            $storage = Storage::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
            if($tovar == null) $result = Move::find()->where(['storage_form' => $storage->id])->orderBy(['id'=>SORT_DESC])->all();  
            else $result = Move::find()->where(['storage_form' => $storage->id, 'part_id' => $tovar ])->orderBy(['id'=>SORT_DESC])->all();  
        }
        
        /*return $this->render('changing', [
            'result' => $result ,
            'model' => $model,
            'post' => $post,
        ]);  */ 
		
        return $this->render('index', [
            'contentInResource' => $contentInResource,
            'contentResource' => $contentResource,
            'contentResourceWaid' => $contentInResourceWaid,
            'ostatki' => $dataProvider,
            'ojidaet_postupleniya' => $dataProviderWaid,
            'oprixodovano' => $dataInResurs,
            'result' => $result ,
            'model' => $model,
            'post' => $post,
        ]);
		
    }
    public function actionPrintlist1()
    {
        $dataInResurs = new ActiveDataProvider([
           'query' => Resource::find(),
            
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]);

        $dataProvider = $dataInResurs->getModels();
        foreach ($dataProvider as $value) {
            echo "<pre>";
            print_r($value);
            echo "</pre>";
            die;
        }
        
        $dataProvider = new ActiveDataProvider([
           'query' => AvailableSearch::find()->where(['status_parts_by' => 3]),
            
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),          

        ]);        
                   
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
          
        $content = $this->renderPartial('@app/views/client/listavailable1',[
            'dataProvider' => $dataProvider->getModels(),
        ]);
             
         $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);
                     
        return $pdf->render();
    }

    public function actionPrintresource()
    {
        
        $dataInResurs = new ActiveDataProvider([
           'query' => Resource::find(),
            
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]);

        $dataProvider = $dataInResurs->getModels();
                    
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
            
        $content = $this->renderPartial('@app/views/available/resourcelist',[
            'dataProvider' => $dataProvider,            
        ]);
             
        $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);
        
                
        return $pdf->render();
    }

    public function actionPrintinresource()
    {
        
        $dataProviderWaid = new ActiveDataProvider([
           'query' => AvailableSearch::find()->where(['status_parts_by' => 3]),
            
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]); 

        $dataProvider = $dataProviderWaid->getModels();
                    
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
            
        $content = $this->renderPartial('@app/views/available/inresourcelist',[
            'dataProvider' => $dataProvider,            
        ]);
             
        $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);
        
                
        return $pdf->render();
    }

    public function actionPrintavailable()
    {
        
        $dataProvider = new ActiveDataProvider([
           'query' => AvailableSearch::find(),
            
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false,
            ],

        ]); 

        $dataProvider = $dataProvider->getModels();
                    
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
            
        $content = $this->renderPartial('@app/views/available/availablelist',[
            'dataProvider' => $dataProvider,            
        ]);
             
        $pdf = new Pdf([
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
        ]);
        
                
        return $pdf->render();
    }

    /**
     * Displays a single Available model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

		$model 	= Available::find()->where(['id'=>$id])->one();
		
        return $this->render('view', [
			'title' => 'Просмотр товара: ',
			'label' => 'Склад',
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Available model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        $model = new Available();

		$parts 			= ArrayHelper::map(Parts::find()->all(), 'id', 'name');
		$typeParts 		= ArrayHelper::map(TypeParts::find()->all(), 'id', 'name');
		$groupParts 	= ArrayHelper::map(TypeDevice::find()->all(), 'id', 'name');
		$statusParts 	= ArrayHelper::map(StatusParts::find()->all(), 'id', 'name');
		$store 			= ArrayHelper::map(User::find()->where(['permission'=>'admin'])->all(), 'id', 'name'); //ArrayHelper::map(Store::find()->all(), 'id', 'name');
		$client 		= ArrayHelper::map(Client::find()->where(['group_by' => 6])->all(), 'id', 'name');
		$model->store_by = (User::find()->where(['permission' => 'admin'])->one()->id);
        $storage = ArrayHelper::map(Storage::find()->where(['is_main' => 1])->all(), 'id', 'name');


		if ($id > 0)
		{		
			$query = Available::find()->where(['number'=>$id]);	
			$model ->number = $id;
		}
		else
		{
			$query = Available::find()->where(['id'=>'null']);				
		}
		

		$dataProvider = new ActiveDataProvider([
            'query' => $query,		
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                'validatePage' => false,
            ],

        ]);
		
		
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /*echo "<pre>";
            print_r(Yii::$app->request->post());
            echo "</pre>";die;*/
	        if($model->status_parts_by == 4)
            {
                $this->setResource($model);
            }
            return $this->redirect(['create', 'id' => $model->number]);
        } else {
			$model->store_by = 1;
            return $this->render('create', [
				'parts' 		=> $parts,
				'typeParts' 	=> $typeParts,
				'groupParts' 	=> $groupParts,
				'statusParts' 	=> $statusParts,			
				'store' 		=> $store,	
                'model' 		=> $model,		
                'client' 		=> $client,		
                'dataProvider' 	=> $dataProvider,
                'storage'		 => $storage,

				'user_by_cr_v' 	=> Yii::$app->user->identity->id,
				'user_by_up_v' 	=> Yii::$app->user->identity->id,
				'date_cr_v' 	=> time(),
                'date_up_v' 	=> time(),
				'title' 		=> 'Поступление на склад: ',
				'label' 		=> 'Склад',
            ]);
        }
    }

   
    /**
     * Updates an existing Available model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		$parts 			= ArrayHelper::map(Parts::find()->all(), 'id', 'name');
		$typeParts 		= ArrayHelper::map(TypeParts::find()->all(), 'id', 'name');
		$groupParts 	= ArrayHelper::map(GroupParts::find()->all(), 'id', 'name');
		$statusParts 	= ArrayHelper::map(StatusParts::find()->all(), 'id', 'name');
		$store 			= ArrayHelper::map(User::find()->all(), 'id', 'name');
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
		
				'parts' 		=> $parts,
				'typeParts' 	=> $typeParts,
				'groupParts' 	=> $groupParts,
				'statusParts' 	=> $statusParts,			
				'store' 		=> $store,	
                'model' 		=> $model,		

				'title' => 'Изменение товара: ',
				'label' => 'Склад',
				'user_by_cr_v' => $model->user_by_cr,
				'user_by_up_v' => Yii::$app->user->identity->id,
				'date_cr_v' =>$model->date_cr,
                'date_up_v' => time(),
            ]);
        }
    }
	
    public function setResource($model)
	{
		$resource = Resource::find()
							->where([
							'parts_by'=>$model->parts_by,
							'store_by'=>$model->store_by,
							'type_parts_by'=>$model->type_parts_by,
							])->one();
		 if (isset($resource))
		 {
			 $resource->count = $resource->count + $model->count;
			 $resource->price = $model->price;
			 $resource->status_parts_by = $model->status_parts_by;
			 if (!$resource->save()){//находим запчасть
				echo "<pre>".print_r($resource,true)."</pre>";
			}
			
		 }
		else
		{
			$resource = new Resource();
			$resource->parts_by 		= $model->parts_by;
			$resource->store_by	 		= $model->store_by;
            $resource->storage_id       = $model->storage_id;
			$resource->status_parts_by 	= $model->status_parts_by;
			$resource->count 			= $model->count;
			$resource->price 			= $model->price;
			$resource->type_parts_by 	= $model->type_parts_by;
			if (!$resource->save()){//находим запчасть
				echo "<pre>".print_r($resource,true)."</pre>";
			}
		}			
		return true;
	}

    public function actionAddresource($id)
    {
	    $available 	= Available::find()->where(['id'=>$id])->one();
        $available->status_parts_by = 4;
        $this->setResource($available);
		if(!$available->save()){
            echo "<pre>".print_r($available,true)."</pre>";
        }else{
            return $this->redirect(['index']);    
        }

    }

 
	public function actionList($id)
    {
		
		
        $countParts = Parts::find()
            ->where(['group_by'=>$id])
            ->count();
 
        $parts = Parts::find()
            ->where(['group_by'=>$id])
            ->all();
 
        if($countParts > 0)
        {
            foreach ($parts as $row) {
                echo "<option value='".$row->id."'>".$row->name."</option>";
            }
        }
        else{
            echo "<option>В этой категории нет товара</option>";
        }
    }

    /**
     Получаем цену закупа
     */
    public function actionInfo($id)
    {
		$parts 		= Parts::find()->where(['id' => $id])->one();
		$available 	= Available::find()->where(['parts_by' => $parts->id])->one();		

        if($available != null) {
    		return [
    			'supplier'	 => $available->supplier,
    			'price'		 => $available->price,
    			'price_shop' => $available->price_shop,
    			'group'      => $available->group_parts_by,
    		];
        }
        else {
            return [
                'supplier'   => $parts->supplier,
                'price'      => $parts->price,
                'price_shop' => $parts->price_shop,
                'group'      => $parts->group_by,
            ];
        }
    }
	
	/**
     * Deletes an existing Available model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
		$available 	= Available::find()->where(['id'=>$id])->one();
		$number 	= $available->number;
		$resource 	= Resource::find()->where(['parts_by'=>$available->parts_by])->one();
		$resource->count = $resource->count  - $available->count;		
		$resource->save();
		
        $available->delete();
		
        return $this->redirect(['create','id' => $number ]);
    }*/

    public function actionDelete($id)
    {
        $resource   = Resource::find()->where(['id'=>$id])->one();
        $resource->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Available model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Available the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Available::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
