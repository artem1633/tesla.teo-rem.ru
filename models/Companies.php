<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property string $company_name Название компании
 * @property string $created_date Дата регистрации
 * @property int $admin_id Id администратора компании
 * @property int $rate_id Тариф
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $access_end_datetime Дата и время потери доступа к системе
 * @property int $is_super Супер компания
 *
 * @property User $admin
 * @property Rates $rate
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'last_activity_datetime', 'access_end_datetime'], 'safe'],
            [['company_name', 'rate_id', 'admin_id'], 'required'],
            [['admin_id', 'rate_id', 'is_super'], 'integer'],
            [['company_name'], 'string', 'max' => 255],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Название компании',
            'created_date' => 'Дата регистрации',
            'admin_id' => 'Администратор компании',
            'rate_id' => 'Тариф',
            'last_activity_datetime' => 'Последняя активность',
            'access_end_datetime' => 'Дата и время окончания доступа',
            'is_super' => 'Супер компания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectParamsTypes()
    {
        return $this->hasMany(ProjectParamsType::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectMaterialsTypes()
    {
        return $this->hasMany(ProjectMaterialsType::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentations()
    {
        return $this->hasMany(Documentation::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectAddMaterialsTypes()
    {
        return $this->hasMany(ProjectAddMaterialsType::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }
    
    public function isSuperCompany()
    {
        return $this->is_super == 1 ? true : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentstypes()
    {
        return $this->hasMany(Documentstype::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStagesDocuments()
    {
        return $this->hasMany(StagesDocument::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(Template::className(), ['company_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if($this->isSuperCompany() === false)
        {
            //$this->deleteAllObjects($this->id);
            return parent::beforeDelete();
        }
    }


    public function DeleteCompanies()
    {
        $sms = SendingSms::find()->where(['company_id' => $this->id])->all();
        foreach ($sms as $value) {
            $sms_object = SmsObject::find()->where(['sms_id' => $value->id])->all();
            foreach ($sms_object as $object) {
                $object->delete();
            }
            $value->delete();
        }

        $about_company = AboutCompany::find()->where(['company_id' => $this->id])->one();
        if($about_company != null) $about_company->delete();

        $cash = Cash::find()->where(['company_id' => $this->id])->all();
        foreach ($cash as $value) {
            $value->delete();
        }

        $shop = Shop::find()->where(['company_id' => $this->id])->all();
        foreach ($shop as $value) {

            $shop_list = ShopList::find()->where(['shop_by' => $value->id])->all();
            foreach ($shop_list as $object) {
                $object->delete();
            }
            $value->delete();
        }

        $order = Order::find()->where(['company_id' => $this->id])->all();
        foreach ($order as $value) {

            $joblist = Joblist::find()->where(['order_by' => $value->id])->all();
            foreach ($joblist as $object) {
                $object->delete();
            }
            $value->delete();
        }

        $available = Available::find()->where(['company_id' => $this->id])->all();
        foreach ($available as $value) {
            $value->delete();
        }

        $recource = Resource::find()->where(['company_id' => $this->id])->all();
        foreach ($recource as $value) {
            $value->delete();
        }

        $parts = \app\models\manual\Parts::find()->where(['company_id' => $this->id])->all();
        foreach ($parts as $value) {

            $move = Move::find()->where(['part_id' => $value->id])->all();
            foreach ($move as $object) {
                $object->delete();
            }
            $value->delete();
        }

        $storage = Storage::find()->where(['company_id' => $this->id])->all();
        foreach ($storage as $value) {
            $value->delete();
        }

        $client = Client::find()->where(['company_id' => $this->id])->all();
        foreach ($client as $value) {
            $value->delete();
        }

        $user = User::find()->where(['company_id' => $this->id])->all();
        foreach ($user as $value) {
            $value->delete();
        }
    }
}
