<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Shablon;

/**
 * ShablonSearch represents the model behind the search form about `app\models\Shablon`.
 */
class ShablonSearch extends Shablon
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['komp_name', 'komp_address', 'komp_tel', 'rejim_raboti', 'tekst'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shablon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'komp_name', $this->komp_name])
            ->andFilterWhere(['like', 'komp_address', $this->komp_address])
            ->andFilterWhere(['like', 'komp_tel', $this->komp_tel])
            ->andFilterWhere(['like', 'rejim_raboti', $this->rejim_raboti])
            ->andFilterWhere(['like', 'tekst', $this->tekst]);

        return $dataProvider;
    }
}
