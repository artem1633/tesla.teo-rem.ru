<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public $search_user;
    public function rules()
    {
        return [
            [['id', 'return_waid', 'return_date', 'type_order_by', 'client_by', 'type_device_by', 'made_by', 'model_device_by', 'price', 'prepay', 'user_by', 'marketing_by', 'discount_order', 'status_order', 'quick', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['nothealth_by', 'view', 'configuration', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'return_waid' => $this->return_waid,
            'return_date' => $this->return_date,
            'type_order_by' => $this->type_order_by,
            'client_by' => $this->client_by,
            'type_device_by' => $this->type_device_by,
            'made_by' => $this->made_by,
            'model_device_by' => $this->model_device_by,
            'price' => $this->price,
            'prepay' => $this->prepay,
            'user_by' => $this->user_by,
            'marketing_by' => $this->marketing_by,
            'discount_order' => $this->discount_order,
            'status_order' => $this->status_order,
            'quick' => $this->quick,
            'user_by_cr' => $this->user_by_cr,
            'user_by_up' => $this->user_by_up,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
        ]);

        $query->andFilterWhere(['like', 'nothealth_by', $this->nothealth_by])
            ->andFilterWhere(['like', 'view', $this->view])
            ->andFilterWhere(['like', 'configuration', $this->configuration])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    public function filtr($params, $post)
    {
        /*$search_days = $post['OrderSearch']['search_days'];
        $search_id = $post['OrderSearch']['search_id'];
        $search_master = $post['OrderSearch']['search_master'];
        $search_texnika = $post['OrderSearch']['search_texnika'];
        $search_status = $post['OrderSearch']['search_status'];
        $search_client = $post['OrderSearch']['search_client'];
        $linkTime = $post['OrderSearch']['link_time'];
        $search_address = $post['OrderSearch']['search_address'];
        $search_tel = $post['OrderSearch']['search_tel'];

        $from = $post['from'];
        $to = $post['to'];*/

        /*echo "<br>search_days=".$search_days;
        echo "<br>search_id=". $search_id;
        echo "<br>search_master=". $search_master;
        echo "<br>search_texnika=". $search_texnika;
        echo "<br>search_client=". $search_client;
        echo "<pre>";
        print_r($search_status);
        echo "</pre>";*/
        $session = Yii::$app->session;
        if($post != null)
        {
            $session['search_id'] = $post['OrderSearch']['search_id'];
            $session['search_master'] = $post['OrderSearch']['search_master'];
            $session['search_texnika'] = $post['OrderSearch']['search_texnika'];
            $session['search_status'] = $post['OrderSearch']['search_status'];
            $session['search_client'] = $post['OrderSearch']['search_client'];
            $session['linkTime'] = $post['OrderSearch']['linkTime'];
            $session['search_address'] = $post['OrderSearch']['search_address'];
            $session['search_serial_number'] = $post['OrderSearch']['search_serial_number'];
            $f = "";
            $tel = $post['OrderSearch']['search_tel'];
            $strlen = strlen( $tel );
            $numeric = 0; 
            for( $i = 0; $i <= $strlen; $i++ ) 
            {
                $char = substr( $tel, $i, 1 );             
                if(ord($char) > 47 && ord($char) < 58) $f .= $char;
            }

            $session['search_tel'] = $f; //$post['OrderSearch']['search_tel'];
            $session['from'] = $post['from'];
            $session['to'] = $post['to'];
            $session['vidat_from'] = $post['search_vidat_from'];
            $session['vidat_to'] = $post['search_vidat_to'];

        } 

        $array = [];
        foreach ($session['search_status'] as $value) {
            $array [] = $value;
        }
        
        if( $array != null) $query = Order::find()->where(['status_order'=>$array]);
        else  $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('user');
        $query->joinWith('client');
        $query->joinWith('type_device');

        $query->andFilterWhere(['order.id' => $session['search_id']]);
        $query->andFilterWhere(['like', 'user.name', $session['search_master']]);
        $query->andFilterWhere(['like', 'client.name', $session['search_client']]);
        $query->andFilterWhere(['like', 'order.client_address', $session['search_address']]);
        $query->andFilterWhere(['like', 'order.phone', $session['search_tel']]);
        $query->andFilterWhere(['like', 'type_device.name', $session['search_texnika']]);
        $query->andFilterWhere(['like', 'order.serial_number', $session['search_serial_number']]);
        if($session['from'] != null) $query->andFilterWhere(['between', 'order.date_cr', strtotime($session['from']) , (strtotime($session['to']) + 86400) ]);
        if($session['vidat_from'] != null) $query->andFilterWhere(['between', 'order.return_waid', strtotime($session['vidat_from']) , (strtotime($session['vidat_to']) + 86400) ]);

        /*if($search_days == 'week' ) $query->andFilterWhere(['between', 'order.date_cr', strtotime('last Monday'), strtotime('next Monday')]);

        if($search_days == 'lastWeek' ) 
            $query->andFilterWhere(['between', 'order.date_cr', strtotime('last Monday',strtotime('last Monday')), strtotime('last Monday')]);

        if($search_days == 'today' ) 
            $query->andFilterWhere(['between', 'order.date_cr', strtotime(date('Y-m-d 00:00:00',time('Y-m-d'))), strtotime(date('Y-m-d 00:00:00',time('Y-m-d'))) + 86400]);

        if($search_days == 'yesterday' ) $query->andFilterWhere(['between', 'order.date_cr', strtotime(date("Y-m-d 00:00:00", time())) - 86400, strtotime(date("Y-m-d 00:00:00", time()))]);

        if($search_days == 'Month' ) $query->andFilterWhere(['between', 'order.date_cr', date('Y-m-01'), time()]);

        if($search_days == 'lastMonth' ) $query->andFilterWhere(['between', 'order.date_cr', date('Y-m-d', mktime(0, 0, 0, date('m') - 1, 1)), date('Y-m-01')]);
        if($search_days == 'dinamick' ) $query->andFilterWhere(['between', 'order.date_cr', strtotime($linkTime), strtotime($linkTime) + 86400]);*/

       
        return $dataProvider;
    }
}
