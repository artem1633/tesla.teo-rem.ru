<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;
use app\models\Template;
/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $company_name;
    public $username;
    //public $position;
    public $password;
    public $phone;
    public $rate_id;

    /**
     * @var Users содержит созраненную модель users для возможности доступа к ней
     * через обработчик событий
     * @see \app\event_handlers\RegisteredFormEventHandler
     */
    public $_user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'class' => \app\event_handlers\RegisteredFormEventHandler::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'username', 'rate_id', 'password', 'company_name', 'phone'], 'required'],
            //[['position'], 'string'],
            [['rate_id'], 'integer'],
            [['username'], 'email'],
            [['phone'], 'unique', 'targetClass' => '\app\models\User'],
            //[['username'], 'unique', 'targetClass' => '\app\models\User'],
            [['company_name'], 'unique', 'targetClass' => '\app\models\Companies'],
            ['username', 'validateUserName'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_name' => 'Названия компании',
            'fio' => 'ФИО',
            'username' => 'Логин',
            //'position' => 'Должность',
            'password' => 'Пароль',
            'rate_id' => 'Тариф',
            'phone' => 'Телефон',
        ];
    }

    public function validateUserName($attribute, $params)
    {
        $user = User::find()->where(['username' => $this->username])->one();
        if (isset($user))
        { 
            $this->addError($attribute,"Значение «".$this->username."» для «Логин» уже занято.");
        }               
        else
        {
            $user = User::find()->where(['email' => $this->username])->one();
            if (isset($user))
            {
                $this->addError($attribute,"Значение «".$this->username."» для «Логин» уже занято.");
            }
        }       
        
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($type = User::USER_TYPE_ADMIN)
    {
        if($this->validate() === false){
            return null;
        }

        $user = new User();
        //$user->attributes = $this->attributes;
        $user->username = $this->username;
        $user->name = $this->fio;
        $user->phone = $this->phone;
        $user->email = $this->username;
        $user->auth_key = $this->password;
        $user->permission = $type;
        $user->percent_sale = 0;
        $user->percent_service = 0;
        $user->percent_finish = 0;
        $user->user_by_cr = 2;
        $user->user_by_up = 2;
        $user->status = 1;
        $user->date_cr = time();
        $user->date_up = time();

        $dateNow = time();
        $rate = Rates::findOne($this->rate_id);
        if($rate != null){
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ {$rate->time} seconds", $dateNow));
        } else {
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ 14 days", $dateNow));
        }

        if($user->save())
        {
            $this->_user = $user;
            $company = new Companies([
                'company_name' => $this->company_name,
                'admin_id' => $user->id,
                'access_end_datetime' => $accessEndDateTime,
                'rate_id' => $this->rate_id,
                'created_date' => date('Y-m-d'),
            ]);

            $company->save();

            $user->link('companies', $company);

            $this->trigger(self::EVENT_REGISTERED);

            $template = EmailTemplates::findByKey('success_registration');

            if($template != null){
                $body = $template->applyTags($user);

                try {
                    Yii::$app->mailer->compose()
                        ->setFrom('zvonki.crm@mail.ru')
                        ->setTo($user->username)
                        ->setSubject('Успешная регистрация')
                        ->setHtmlBody($body)
                        ->send();
                } catch(\Exception $e){

                }
            }

            $user->company_id = $company->id;
            $user->save();

            $storage = new Storage();
            $storage->name = 'Основной склад (' . $this->company_name . ')';
            $storage->user_id = $user->id;
            $storage->is_main = 1;
            $storage->company_id = $user->company_id;
            $storage->save();

            \Yii::$app->db->createCommand()->update('storage', [ 'company_id' => $user->company_id ], [ 'id' => $storage->id ])->execute();

            //$example_company = AboutCompany::findOne(1);
            $about_company = new AboutCompany();
            $about_company->name = $this->company_name;
            $about_company->adress = 'Новосибирск';
            $about_company->bank = 'Наименования банка';
            $about_company->director = $user->name;
            $about_company->smslog = ' ';
            $about_company->smspas = ' ';
            $about_company->email = $this->username;
            $about_company->phone = $this->phone;
            $about_company->user_by_cr = $user->id;
            $about_company->user_by_up = $user->id;
            $about_company->date_cr = time();
            $about_company->date_up = time();
            $about_company->site_name = ' ';
            $about_company->company_id = $company->id;
            $about_company->save();

            Template::setTemplates($company->id);
            
            return $user;
        }

        return null;
    }

}