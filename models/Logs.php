<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $event_datetime Дата и время события
 * @property string $event Наименование события
 * @property string $description Описание события
 *
 * @property Users $user
 */
class Logs extends \yii\db\ActiveRecord
{

    const EVENT_USER_REGISTERED = 'event_user_registered';
    const EVENT_USER_OPEN_REGISTRATION_PAGE = 'event_user_open_registration_page';
    const EVENT_USER_AUTHORIZED = 'event_user_authorized';
    const EVENT_USER_ADDED = 'event_user_added';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['event_datetime'], 'safe'],
            [['event', 'description'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'event_datetime' => 'Дата и время',
            'description' => 'Описание',
        ];
    }

    /**
     * Возвращает описание событий
     * @return array
     */
    public function getEventDescriptions()
    {
        return [
            self::EVENT_USER_REGISTERED => 'Пользователь {user_fio} ({request_ip}) успешно зарегистрировался | {datetime}',
            self::EVENT_USER_AUTHORIZED => 'Пользователь {user_fio} ({request_ip}) успешно авторизировался | {datetime}',
            self::EVENT_USER_OPEN_REGISTRATION_PAGE => 'Переход на страницу регистрации с IP: {request_ip} | {datetime}',
            self::EVENT_USER_ADDED => 'Пользователь {user_fio} ({request_ip}) успешно добавил нового пользователя | {datetime}',
        ];
    }

    /**
     * Рендерит описание события
     * @return null|string
     */
    public function generateEventDescription()
    {
        $user_fio = null;
        $datetime = $this->event_datetime != null ? $this->event_datetime : date('Y-m-d H:i:s');
        $request_ip = Yii::$app->request->getRemoteIP();

        if($this->user_id != null)
        {
            $user = User::findOne($this->user_id);

            if($user != null)
            {
                $user_fio = $user->name;
            }
        }

        $tags = [
            '{user_fio}' => $user_fio,
            '{datetime}' => $datetime,
            '{request_ip}' => $request_ip
        ];

        if(isset($this->getEventDescriptions()[$this->event]))
        {
            return strtr($this->getEventDescriptions()[$this->event], $tags);
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
