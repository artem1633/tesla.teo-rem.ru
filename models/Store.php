<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property integer $id
 * @property string $name
 * @property string $adress
 * @property integer $status
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'adress', 'status', 'user_by_cr', 'user_by_up'], 'required'],
            [['status', 'user_by_cr', 'user_by_up'], 'integer'],
            [['name', 'adress'], 'string', 'max' => 255],
            [['date_cr', 'date_up'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'adress' => 'Адрес',
            'status' => 'Статус',
            'user_by_cr' => 'Создатель',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_by_cr']);
    }
}
