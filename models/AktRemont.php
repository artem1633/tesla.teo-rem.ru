<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akt_remont".
 *
 * @property integer $id
 * @property string $top
 * @property string $shablon_name
 * @property string $klient
 * @property string $telephone
 * @property string $device
 * @property string $equipment
 * @property string $appearance
 * @property string $defect
 * @property string $orient_data
 * @property string $orient_cost
 * @property string $prepay
 * @property string $zametka
 * @property string $tekst
 * @property string $priyomshik
 * @property string $inform_master
 */
class AktRemont extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akt_remont';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['top', 'tekst'], 'string'],
            [['shablon_name', 'klient', 'telephone', 'device', 'equipment', 'appearance', 'defect', 'orient_data', 'orient_cost', 'prepay', 'zametka', 'priyomshik', 'inform_master'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'top' => 'Верхный часть',
            'shablon_name' => 'Названия шаблона',
            'klient' => 'Клиент',
            'telephone' => 'Телефон',
            'device' => 'Устройство',
            'equipment' => 'Комплектация',
            'appearance' => 'Внешний вид',
            'defect' => 'Дефект',
            'orient_data' => 'Ориентировочная дата готовности',
            'orient_cost' => 'Ориентировочная стоимость',
            'prepay' => 'Предоплата',
            'zametka' => 'Заметки приемщика',
            'tekst' => 'Текст',
            'priyomshik' => 'Приемщик',
            'inform_master' => 'Информация для мастера',
        ];
    }
}
