<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JobList;

/**
 * JobListSearch represents the model behind the search form about `app\models\JobList`.
 */
class JobListSearch extends JobList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_by', 'job_by', 'count', 'price', 'date_cr', 'date_up'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_by' => $this->order_by,
            'job_by' => $this->job_by,
            'count' => $this->count,
            'price' => $this->price,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
        ]);

        return $dataProvider;
    }
}
