<?php

namespace app\models;

use Yii;

use app\models\manual\Parts;
use app\models\manual\StatusParts;
use app\models\Store;
use app\models\manual\TypeParts;
use app\models\manual\TypeDevice;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "resource".
 *
 * @property integer $id
 * @property integer $parts_by
 * @property integer $store_by
 * @property integer $status_parts_by
 * @property integer $count
 * @property integer $price
 * @property integer $type_parts_by
 */
class Resource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $move_count;
    public $tovar_name;
    public static function tableName()
    {
        return 'resource';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }


    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parts_by', 'store_by', 'status_parts_by', 'count', 'price', 'type_parts_by'], 'required'],
            [['tovar_name'], 'string'],
            [['parts_by', 'store_by', 'status_parts_by', 'count', 'price', 'type_parts_by', 'storage_id', 'move_count', 'company_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parts_by' => 'Parts By',
            'store_by' => 'Store By',
            'status_parts_by' => 'Status Parts By',
            'count' => 'Количество',
            'price' => 'Цена',
            'type_parts_by' => 'Type Parts By',
            'storage_id' => 'Склад',
            'move_count' => 'Количество товаров',
            'company_id' => 'Company',
        ];
    }
	
	public function getParts()
	{
		return $this->hasOne(Parts::className(), ['id' => 'parts_by']);
	}
	
	public function getStore()
	{
		return $this->hasOne(User::className(), ['id' => 'store_by']);
	}	
	
	public function getStatus()
	{
		return $this->hasOne(StatusParts::className(), ['id' => 'status_parts_by']);
	}

	
	public function getType()
	{
		return $this->hasOne(TypeParts::className(), ['id' => 'type_parts_by']);
	}
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }
}
