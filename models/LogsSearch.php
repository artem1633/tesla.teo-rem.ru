<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logs;
use yii\helpers\ArrayHelper;

/**
 * LogsSearch represents the model behind the search form about `app\models\Logs`.
 */
class LogsSearch extends Logs
{

    /**
     * @var string
     */
    public $period;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['event_datetime', 'event', 'description', 'period'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'period' => 'Период'
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Logs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'event_datetime' => $this->event_datetime,
        ]);

        $query->andFilterWhere(['like', 'event', $this->event])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getPeriodsList()
    {
        return [
            date('Y-m-d') => '1 день',
            (new \DateTime('-7 days'))->format('Y-m-d') => '1 неделя',
            (new \DateTime('-30 days'))->format('Y-m-d') => '1 месяц',
            (new \DateTime('-365 days'))->format('Y-m-d') => '1 год',
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function getAuthorized($params)
    {
        $query = Logs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $query->andWhere(['event' => Logs::EVENT_USER_AUTHORIZED]);
        $query->andFilterWhere(['between', 'event_datetime', $this->period, date('Y-m-d H:i:s')]);

        $this->load($params);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function getRegistered($params)
    {
        $query = Logs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $query->andWhere(['event' => Logs::EVENT_USER_REGISTERED]);
        $query->andFilterWhere(['between', 'event_datetime', $this->period, date('Y-m-d H:i:s')]);

        $this->load($params);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function getOpenedRegistrationPage($params)
    {
        $query = Logs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $query->andWhere(['event' => Logs::EVENT_USER_OPEN_REGISTRATION_PAGE]);
        $query->andFilterWhere(['between', 'event_datetime', $this->period, date('Y-m-d H:i:s')]);

        $this->load($params);

//        echo $query->createCommand()->getRawSql();
//        exit;

        return $dataProvider;
    }
}
