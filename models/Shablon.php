<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shablon".
 *
 * @property integer $id
 * @property string $komp_name
 * @property string $komp_address
 * @property string $komp_tel
 * @property string $rejim_raboti
 * @property string $tekst
 */
class Shablon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shablon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['komp_name', 'komp_address', 'komp_tel', 'rejim_raboti'], 'required'],
            [['komp_name', 'komp_address', 'komp_tel', 'rejim_raboti'], 'string', 'max' => 255],
            [['tekst'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'komp_name' => 'Компания',
            'komp_address' => 'Адрес',
            'komp_tel' => 'Телефон номер',
            'rejim_raboti' => 'Режим работы ',
            'tekst' => 'Текст',
        ];
    }
}
