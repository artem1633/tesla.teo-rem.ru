<?php

namespace app\models;


class Statistics
{
    private $data = [];
    private $company;

    public function __construct($company)
    {
        $this->company = $company;
    }

    public function getData(){
        return $this->data;
    }

    /**
     * Инициализация, запуск всех методов подсчета статистики
     *
     * @return array
     */
    public function init(){
        $this->data['sumFinish'] = $this->calculatedFinishSum();
        $this->data['countAllOrders'] = $this->calculatedCountOrders();
        $this->data['countFinishOrders'] = $this->calculatedCountFinishOrders();
        $this->data['countWorkOrders'] = $this->calculatedCountWorks();
        $this->data['sumAll'] = $this->calculatedSumAll();
        $this->data['salaries'] = $this->calculatedSalaries();
        $this->data['marketing'] = $this->calculatedMarketing();
    }

    /**
     * Считает сумму всех завершенных ордеров
     *
     * @return array
     */
    public function calculatedFinishSum()
    {
        return (new \yii\db\Query())
            ->select('(SUM(order.price))/count(*) as result')
            ->from('order')
            ->where('status_order = 7')
            ->leftJoin('user',['company' => $this->company])
            ->andWhere('order.user_by_cr = user.id')
            ->one()['result'];
    }

    /**
     * Считает все ордеры
     *
     * @return array
     */
    public function calculatedCountOrders()
    {
        return (new \yii\db\Query())
            ->select('count(*) as result')
            ->from('order')
            ->leftJoin('user',['company' => $this->company])
            ->andWhere('order.user_by_cr = user.id')
            ->one()['result'];
    }

    /**
     * Считает все завершенные ордеры
     *
     * @return array
     */
    public function calculatedCountFinishOrders()
    {
        return (new \yii\db\Query())
            ->select('count(*) as result')
            ->from('order')
            ->where('status_order = 7')
            ->leftJoin('user',['company' => $this->company])
            ->andWhere('order.user_by_cr = user.id')
            ->one()['result'];
    }

    /**
     * Считает все ордеры в работе
     *
     * @return array
     */
    public function calculatedCountWorks()
    {
        return (new \yii\db\Query())
            ->select('count(*) as result')
            ->from('order')
            ->where('status_order = 2')
            ->leftJoin('user',['company' => $this->company])
            ->andWhere('order.user_by_cr = user.id')
            ->one()['result'];
    }

    /**
     * Считает сумму цен всех ордеров
     *
     * @return array
     */
    public function calculatedSumAll()
    {
        return (new \yii\db\Query())
            ->select('(SUM(order.price))/count(*) as result')
            ->from('order')
            ->leftJoin('user',['company' => $this->company])
            ->andWhere('order.user_by_cr = user.id')
            ->one()['result'];
    }

    /**
     * Расчитывает данные по зарплатам
     *
     * @return array
     */
    public function calculatedSalaries()
    {
        return (new \yii\db\Query())
            ->select('(SUM(job_list.price)/100*user.percent_finish) as result, user.name')
            ->from('user')
            ->leftJoin('order','user.id = order.user_by')
            ->leftJoin('job_list','order_by= order.id and type = \'service\'')
            ->where('user.permission = \'master\'')
            ->andWhere('user.company = company',['company' => $this->company])
            ->groupBy('user.id')
            ->all();
    }

    /**
     * Расчитывает данные о рекламе
     *
     * @return array
     */
    public function calculatedMarketing()
    {
        return (new \yii\db\Query())
            ->select('count(*) as count, marketing.name')
            ->from('order')
            ->leftJoin('marketing','marketing.id = order.marketing_by')
            ->where('marketing.show_in_indicators = 1')
            ->groupBy('order.marketing_by')
            ->leftJoin('user',['company' => $this->company])
            ->andWhere('order.user_by_cr = user.id')
            ->all();
    }

}
