<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "storage".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $is_main
 *
 * @property User $user
 */
class Storage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['user_id', 'is_main'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Названия склада',
            'user_id' => 'Пользователь',
            'is_main' => 'Тип склад',
            'storages' => 'Склад',
            'tovar' => 'Товар',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['storage_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['storage_id' => 'id']);
    }
    public function getUsers($user_id)
    {
        $result = [];
        $masters = User::find()->where(['permission' => 'master'])->all();
        foreach ($masters as $master) {
            $storage = Storage::find()->where(['user_id' => $master->id])->one();
            if($storage == null) $result [] = [
                'id' => $master->id,
                'name' => $master->name,
            ];
            else {
                if($storage->user_id == $user_id) $result [] = [
                'id' => $master->id,
                'name' => $master->name,
            ];
            }

        }
        return ArrayHelper::map($result,'id', 'name');
    }

    public function Masters($storage_id)
    {
        $result = [];
        $storages = Storage::find()->all();
        foreach ($storages as $storage) {
            
            if($storage_id != $storage->id){
                if($storage->is_main == 1)
                    $result [] = [
                        'id' => $storage->id,
                        'name' => 'Пользователь : Администратор / ' .$storage->user->name . $storage->name,
                    ];
                else $result [] = [
                    'id' => $storage->id,
                    'name' => 'Пользователь :' .$storage->user->name  . ' / '. $storage->name,
                ];
            }
        }
        return ArrayHelper::map($result,'id', 'name');
    }
}
