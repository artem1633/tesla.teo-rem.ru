<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;
use app\models\manual\GroupClient;

/**
 * ClientSearch represents the model behind the search form about `app\models\Client`.
 */
class ClientSearch extends Client
{
	
		public $group_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_by', 'discount_order', 'discount_parts', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
			[['group_name'],'string'],
            [['name', 'type', 'iin', 'ogrn', 'expense',  'group_by', 'adress', 'phone', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        /*$query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->andWhere('client.user_by_cr = user.id');*/
		
		$query->joinWith(['group'])->andFilterWhere(['like', GroupClient::tableName().'.name', $this->group_name]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		$dataProvider->sort->attributes['group_name'] = [
        'asc' => ['name' => SORT_ASC],
        'desc' => ['name' => SORT_DESC],
			];
			
        // grid filtering conditions
        $query->andFilterWhere([
            //'id' => $this->id,
            'group_by' => $this->group_by,
            'discount_order' => $this->discount_order,
            'discount_parts' => $this->discount_parts,
            'user_by_cr' => $this->user_by_cr,
            'user_by_up' => $this->user_by_up,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
        ]);
        //echo "hi=".$this->inn;die;

        $query->andFilterWhere(['like', 'client.name', $this->name])
            ->andFilterWhere(['like', 'client.id', $this->id])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'iin', $this->iin])
            ->andFilterWhere(['like', 'ogrn', $this->ogrn])
            ->andFilterWhere(['like', 'expense', $this->expense])
            ->andFilterWhere(['like', 'adress', $this->adress])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
