<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nastroyka".
 *
 * @property integer $id
 * @property string $password
 * @property string $sms_parol
 */
class Nastroyka extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $sms_id;
    public $sms_parol_retry;
    public $stariy_sms_parol;
    public static function tableName()
    {
        return 'nastroyka';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'sms_parol', 'sms_parol_retry', 'stariy_sms_parol'], 'string', 'max' => 255],
            [['sms_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Пароль',
            'sms_parol' => 'Пароль для отправка смса',
            'sms_parol_retry' => 'Новый пароль',
            'stariy_sms_parol' => 'Старый смс пароль',
        ];
    }
}
