<?php

namespace app\models;

use Yii;

use app\models\manual\Parts;
/**
 * This is the model class for table "shop_list".
 *
 * @property integer $id
 * @property integer $parts_by
 * @property integer $price
 * @property integer $count
 * @property integer $shop_by
 */
class ShopList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'count'], 'required'], // parts_by и shop_by должны быть обязательными полями?
            [['parts_by', 'price', 'count', 'shop_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parts_by' => 'Товар',
            'price' => 'Цена',
            'count' => 'Кол',
            'shop_by' => 'shop_by',
        ];
    }
		
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {//если заказ завершен то ставим дату изменения на дату фактическую
		
				$resource = Resource::find()->where(['parts_by'=>$this->parts_by])->one();
				if($resource != null){
                    $resource->count = $resource->count  - $this->count;		
				    $resource->save();
                }
				
			return true;
		} else {
			return false;
		}
	}
	
	public function getParts()
	{
		return $this->hasOne(Parts::className(), ['id' => 'parts_by']);
	}
}
