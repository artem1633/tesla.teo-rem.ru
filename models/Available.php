<?php

namespace app\models;


use Yii;
use app\models\manual\Parts;
use app\models\manual\TypeParts;
use app\models\manual\TypeDevice;
use app\models\manual\StatusParts;
use app\models\Store;
use app\models\Resource;
use yii\helpers\ArrayHelper;
use app\models\Client;

use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;
/**
 * This is the model class for table "available".
 *
 * @property integer $id
 * @property integer $group_parts_by
 * @property integer $parts_by
 * @property integer $store_by
 * @property integer $status_parts_by
 * @property integer $count
 * @property integer $type_parts_by
 * @property string $comment
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 * @property integer $price_shop
 * @property integer $price
 */
class Available extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $move_count;
    public $tovar;
    public $storages;
    public static function tableName()
    {
        return 'available';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if (isset(Yii::$app->user->identity->id)){
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'parts_by','group_parts_by', 'supplier', 'store_by', 'status_parts_by', 'count', 'type_parts_by',  'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'price', 'price_shop'], 'required'],
            [['group_parts_by', 'store_by', 'status_parts_by', 'count','price','price_shop', 'type_parts_by','number', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'storage_id', 'company_id'], 'integer'],
            [['comment'], 'string', 'max' => 255],
            ['supplier', 'validateSupplier'],
            ['parts_by', 'validateParts'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_parts_by' => 'Група',
            'parts_by' => 'Товар',
            'supplier' => 'Поставщик',
            //'store_by' => 'Склад',
            'store_by' => 'Мастер',
            'status_parts_by' => 'Статус',
            'count' => 'Количество',
            'price' => 'Цена',
            'price_shop' => 'Цена закупа',
            'type_parts_by' => 'Тип',
            'comment' => 'Коментарий',
            'number' => 'Номер документа',
            'user_by_cr' => 'Создатель',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'storage_id' => 'Склад',
            'move_count' => 'Количество товаров',
            'storages' => 'Склад',
            'tovar' => 'Товар',
        ];
    }
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }
	
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) 
		{	
	
			if(!$this->number > 0){
				$number = Available::find()->orderBy('id desc')->one();
				if (!isset($number)){$this->number  = 1;} else{$this->number = $number->number + 1;}			
			}
			
			return true;
		} else {
			return false;
		}
	}

	//Новый клиент 
	public function validateSupplier($attribute, $params)
	{
		$supplier = Client::find()->where(['id'=>$this->supplier])->one();
		if (!isset($supplier)){
			$supplier = new Client();
			
			//$this->client_by = "ффф";
			$supplier->name = $this->supplier;
			$supplier->group_by = 6;
			$supplier->type = "person";
			$supplier->discount_order = 0;
			$supplier->discount_parts = 0;
			$supplier->user_by_cr = Yii::$app->user->identity->id;
			$supplier->user_by_up = Yii::$app->user->identity->id;
			$supplier->date_cr = time();
			$supplier->date_up = time();
			
			if ($supplier->save()){
				$this->supplier = $supplier->id;
			}else{
				$this->addError($attribute,"Не создан новый Поставщик");
			//	echo "<pre>".print_r($supplier,true)."</pre>";
			}
		}
	}
	
		//Новый товар
	public function validateParts($attribute, $params)
	{
        $parts = Parts::find()->where(['id' => $this->parts_by])->one();
			if (!isset($parts)){
                
                $parts = new Parts();
                $parts->name = $this->parts_by;
                $parts->group_by = $this->group_parts_by;
                $parts->supplier = $this->supplier;
                $parts->price = $this->price;
                $parts->price_shop = $this->price_shop;
                $parts->date_cr = time();
                $parts->date_up = time();
                $error = $parts->errors;
                
            if ($parts->save()){
                    $this->parts_by = $parts->id;
                }else{
                    $this->addError($attribute,"Не создан новая запчасть");
                //	echo "<pre>".print_r($supplier,true)."</pre>";
                }
                /*echo "<pre>";
                print_r($parts);
                echo "</pre>";

                echo "<pre>";
                print_r($error);
                echo "</pre>";*/
                //die;
		
            }
	}
	
	public function getGroup()
	{
		return $this->hasOne(TypeDevice::className(), ['id' => 'group_parts_by']);
	}
	
	public function getParts()
	{
		return $this->hasOne(Parts::className(), ['id' => 'parts_by']);
	}
	
	public function getStatus()
	{
		return $this->hasOne(StatusParts::className(), ['id' => 'status_parts_by']);
	}

	
	public function getType()
	{
		return $this->hasOne(TypeParts::className(), ['id' => 'type_parts_by']);
	}

	public function getStore()
	{
		return $this->hasOne(User::className(), ['id' => 'store_by']);
	}	
	
	//Поставщик
	//public function getSuppliername()
	//{
	//	return $this->hasOne(Client::className(), ['id' => 'supplier']);
	//}
	
	//Создатель
	public function getUsercr()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by_cr']);
	}
	
	public function getUserup()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by_up']);
	}

	public function PartList()
    {
        return ArrayHelper::map(manual\Parts::find()->all(),'id', 'name');
    }
    public function StorageList()
    {
        return ArrayHelper::map(Storage::find()->all(),'id', 'name');
    }
}
