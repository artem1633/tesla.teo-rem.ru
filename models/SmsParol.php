<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_parol".
 *
 * @property integer $id
 * @property string $password
 */
class SmsParol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $sms_id;
    public static function tableName()
    {
        return 'sms_parol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            [['password'], 'string', 'max' => 255],
            [['sms_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Password',
        ];
    }
}
