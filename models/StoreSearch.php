<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Store;

/**
 * StoreSearch represents the model behind the search form about `app\models\Store`.
 */
class StoreSearch extends Store
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'user_by_up', 'date_cr', 'date_up'], 'integer'],
            [['name', 'adress', 'user_by_cr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Store::find();

        $query->leftJoin('user',['company' => \Yii::$app->user->getIdentity()->company])
            ->andWhere('store.user_by_cr = user.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['user'])->andFilterWhere(['like', 'user.name', $this->user_by_cr]);

        // grid filtering conditions
        $query->andFilterWhere([
            'store.id' => $this->id,
            'store.status' => $this->status,
            //'user_by_cr' => $this->user_by_cr,
            'store.user_by_up' => $this->user_by_up,
            'store.date_cr' => $this->date_cr,
            'store.date_up' => $this->date_up,
        ]);

        $query->andFilterWhere(['like', 'store.name', $this->name])
            ->andFilterWhere(['like', 'store.adress', $this->adress]);

        return $dataProvider;
    }
}
