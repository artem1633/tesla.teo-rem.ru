<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cash;

/**
 * CashSearch represents the model behind the search form about `app\models\Cash`.
 */
class CashSearch extends Cash
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type_cash_by', 'summa', 'comment', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cash::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_cash_by' => $this->type_cash_by,
            'summa' => $this->summa,
            'comment' => $this->comment,
            'user_by_cr' => $this->user_by_cr,
            'user_by_up' => $this->user_by_up,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
