<?php

namespace app\models;

use Yii;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property string $name
 * @property string $tekst
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /*public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }*/

    /**
     * @inheritdoc
     */
    /*public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }*/

    /**
     * @inheritdoc
     */
    /*public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tekst'], 'required'],
            [['tekst'], 'string'],
            [['dostup', 'company_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Названия шаблона',
            'tekst' => 'Текст',
            'dostup' => 'Доступ редактирования перед печатью акта',
            'company_id' => 'Компания',
        ];
    }

    public function getStatus()
    {
        if($this->push == 1) return 'Есть';
        else return 'Нет';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    public function setTemplates($company_id)
    {
        $acceptance_text = '<div class="WordSection1">
<p>&quot; {company_name} &quot;<br />
{company_adress}<br />
тел.: {company_phone}<br />
{mode_of_operation} Пн-Пт&nbsp; 9&shy;-18, Сб,Вс - выходной</p>
&nbsp;

<p style="text-align:center"><strong><span style="font-size:14.0pt">{template_name} № {akt_number} от {akt_date}</span></strong></p>
&nbsp;

<table border="0" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none; margin-left:5.4pt">
    <tbody>
        <tr>
            <td style="width:239.25pt">
            <p>Клиент: {client_name}<br />
            Тел.: {client_phone}<br />
            Устройство: {device}<br />
            Комплектация: {configuration}<br />
            Внешний вид: {appearance}<br />
            Дефект: {defect}</p>
            </td>
            <td style="width:278.15pt">
            <p>Ориентировочная дата готовности: {approx_data}<br />
            Ориентировочная стоимость: {approx_cost} руб<br />
            Предоплата: {preorder} руб<br />
            Заметки приемщика: {notes}</p>
            </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>1. Технический центр не несет ответственности за возможную потерю данных в памяти устройства, а так же за оставленные SIM и FLASH карты. Заблаговременно примите меры по резервированию информации.<br />
2. Заказчик принимает на себя риск возможной полной или частичной утраты работоспособности устройства в процессе ремонта, в случае грубых нарушений пользователем условий эксплуатации , наличие следов попадания токопроводящей жидкости (коррозии), либо механических повреждений.<br />
3. На восстановленные после попадания жидкости на устройство гарантия не распространяется и не продлевается.<br />
4. Срок хранения аппарата 30 дней с ориентировочной даты готовности. После данного срока аппарат утилизируется и претензии по нему не принимаются.<br />
5. В случае утери квитанции, устройство выдается по предъявлению паспорта на имя заказчика.</p>
&nbsp;

<table border="0" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none">
    <tbody>
        <tr>
            <td style="height:28.05pt; width:264.05pt">
            <p>Приемщик: ________________({usercr_name})</p>

            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; М.П.</p>
            </td>
            <td style="height:28.05pt; width:264.1pt">
            <p style="text-align:right">_____________________ ({client_name})</p>

            <p style="text-align:right">с условиями ремонта ознакомлен и согласен</p>
            </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>______________________________________________________________________________________________</p>

<p style="text-align:center"><strong><span style="font-size:14.0pt">Информация для мастера № {akt_number} от {akt_date}</span></strong></p>

<p>Клиент: {client_name},{client_phone}<br />
Устройство: {device}<br />
Комплектация: {configuration}<br />
Внешний вид: {appearance}<br />
Дефект: {defect}<br />
Заметки приемщика: {notes}
<br>
<br>
{barcode}</p>
</div>
';

$order_form_text = "<div class=Section1>
 <br><br><br><br><br>
<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 style='margin-left:.25pt;border-collapse:collapse;border:none'> 
 
<tr style='height:19.85pt'>
    <td width=200 height=70 colspan='6'>
        <p class=MsoNormal style='font-size:26.0pt'><center><b>Форма заказа</b></center></p>
    </td>
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Выдать</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{akt_date}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Тип заказа</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{tipzakaza}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Мастер</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{order_usermas_name}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Клиент</b></p>
        </td>
        <td width=300 height=70 colspan='3'>
            <p class=MsoNormal style='font-size:16.0pt'><center>{client_name}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Телефон клиента</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{client_phone}</center></p>
        </td>
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Не исправность</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{nothealth_name}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Устройство</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{device}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Производитель</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{device_made_name}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Модель</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{model}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Внешний вид</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{appearance}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Комплектация</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{configuration}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Цена</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0t'><center>{price}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Предоплата</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{prepay}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Скидка</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{discount_order}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Коментарий</b></p>
        </td>
        <td width=300 height=70 colspan='3'>
            <p class=MsoNormal style='font-size:16.0pt'><center>{comment}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Реклама</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{reklama}</center></p>
        </td>
</tr>
 
</table>
<br>
<br>
{barcode}
</div>

";

$done_text = "<div class=Section1>
                <p class=MsoNormal>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                normal'><p class=MsoNormal  style='font-size:8.00pt;'>

                &quot; {company_name} &quot; <br>
                {company_adress} <br>
                тел.: {company_phone} <br>
                {mode_of_operation}</p>
                </p>
                <br/>
                <p class=MsoNormal align=center style='text-align:center'>
                <u><span
                style='font-size:12.0pt;font-family:\"Arial\",\"sans-serif\"'>{template_name}  <b style='mso-bidi-font-weight:
                normal'>№ </b></u><b style='mso-bidi-font-weight:normal'><u>
                {akt_number}

                </u></b><b
                style='mso-bidi-font-weight:normal'><u></u></b><i
                style='mso-bidi-font-style:normal'> от {akt_date}<o:p></o:p></span></i>

                </p>

                <br/><br/>

                              
                <br/>

                <b
                style='mso-bidi-font-weight:normal'>Клиент:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'> {client_name},  {client_phone} </span><o:p></o:p></span>

                <br/>
                
                
                <b
                style='mso-bidi-font-weight:normal'>Устройство:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{device} {device_made_name} {device_modeldevice_name} </span><o:p></o:p></span>

                <br/>

                <b
                style='mso-bidi-font-weight:normal'>Не исправность:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{nothealth_name} </span><o:p></o:p></span>

                <br/>
                <b
                style='mso-bidi-font-weight:normal'>Скидка:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{discount_order} </span><o:p></o:p></span>

                <br/>
                <b
                style='mso-bidi-font-weight:normal'>Предоплата:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{prepay} </span><o:p></o:p></span>

                </p>

                <br/>
                <b style='mso-bidi-font-weight:normal'>
                {warranty_period} : 30 дней
                </b>
                <p class=MsoNormal>&nbsp;</p>
                
                <br><br>
                {table}
                <br>
                <br>
                {barcode}
                <br>
                <br>

                <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
                style='font-size:8.0pt;line-height:115%;font-family:\"Arial\",\"sans-serif\";
                color:black;background:white'>{client_signature}: ____________________
                (________________)<span style='mso-spacerun:yes'>     </span>{employee_signature}: __________________({order_usermas_name})</span></b><b style='mso-bidi-font-weight:
                normal'><span style='font-size:8.0pt;line-height:115%'><o:p></o:p></span></b></p>

                <p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
                \"Arial\",\"sans-serif\";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

                <p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
                \"Arial\",\"sans-serif\";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

                </div>

           
         ";

$sms_text = 'Ваш заказ № {order_id} {status_name} {status_tekst}, сумма ремонта {summa} руб. Компания Цифровые технологии';

            $template = new Template();
            $template->name = 'Акт приемки на ремонт';
            $template->tekst = $acceptance_text;
            $template->dostup = null;
            $template->company_id = $company_id;
            $template->key = 'acceptance';
            $template->save();

            $template = new Template();
            $template->name = 'Акт выполненных работ';
            $template->tekst = $done_text;
            $template->dostup = null;
            $template->company_id = $company_id;
            $template->key = 'done';
            $template->save();

            $template = new Template();
            $template->name = 'Смс шаблон';
            $template->tekst = $sms_text;
            $template->dostup = null;
            $template->company_id = $company_id;
            $template->key = 'sms';
            $template->save();

            $template = new Template();
            $template->name = 'Форма заказа';
            $template->tekst = $order_form_text;
            $template->dostup = null;
            $template->company_id = $company_id;
            $template->key = 'order_form';
            $template->save();


    }
}
