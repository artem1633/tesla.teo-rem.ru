<?php

namespace app\models;

use Yii;
use app\models\manual\GroupClient;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $iin
 * @property string $ogrn
 * @property string $expense
 * @property integer $group_by
 * @property string $adress
 * @property string $phone
 * @property string $email
 * @property integer $discount_order
 * @property integer $discount_parts
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'group_by', 'discount_order', 'discount_parts',  'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'phone'], 'required'],
            [['group_by', 'discount_order', 'discount_parts', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer' ],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['type'], 'string', 'max' => 100],
            [['iin', 'ogrn'], 'string', 'max' => 30],
            [['expense', 'adress', 'email'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'type' => 'Тип',
            'iin' => 'ИИН',
            'ogrn' => 'ОГРН',
            'expense' => 'р/с',
            'group_by' => 'Группа',
            'adress' => 'Адрес',
            'phone' => 'Телефон',
            'email' => 'Email',
            'discount_order' => 'На заказ(%)',
            'discount_parts' => 'На работу(%)',
            'user_by_cr' => 'Создатель',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
	
	 public function getGroup()
	{
		return $this->hasOne(GroupClient::className(), ['id' => 'group_by']);
	}
}
