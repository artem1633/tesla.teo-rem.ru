<?php

namespace app\models;

use Yii;
use app\models\manual\TypeCash;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "cash".
 *
 * @property integer $id
 * @property integer $type_cash_by
 * @property integer $summa
 * @property string $comment
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 * @property string $type
 */
class Cash extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_cash_by', 'summa', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'type'], 'required'],
            [['type_cash_by', 'summa', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['comment'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_cash_by' => 'Статья',
            'summa' => 'Сумма',
            'comment' => 'Коментарий',
            'user_by_cr' => 'Создатель',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'type' => 'Тип',
        ];
    }
	
		public function getTypecash()
	{
		return $this->hasOne(TypeCash::className(), ['id' => 'type_cash_by']);
	}
	
		//Создатель
	public function getUsercr()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by_cr']);
	}
}
