<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AktRemont;

/**
 * AktRemontSearch represents the model behind the search form about `app\models\AktRemont`.
 */
class AktRemontSearch extends AktRemont
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['top', 'shablon_name', 'klient', 'telephone', 'device', 'equipment', 'appearance', 'defect', 'orient_data', 'orient_cost', 'prepay', 'zametka', 'tekst', 'priyomshik', 'inform_master'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AktRemont::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'top', $this->top])
            ->andFilterWhere(['like', 'shablon_name', $this->shablon_name])
            ->andFilterWhere(['like', 'klient', $this->klient])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'device', $this->device])
            ->andFilterWhere(['like', 'equipment', $this->equipment])
            ->andFilterWhere(['like', 'appearance', $this->appearance])
            ->andFilterWhere(['like', 'defect', $this->defect])
            ->andFilterWhere(['like', 'orient_data', $this->orient_data])
            ->andFilterWhere(['like', 'orient_cost', $this->orient_cost])
            ->andFilterWhere(['like', 'prepay', $this->prepay])
            ->andFilterWhere(['like', 'zametka', $this->zametka])
            ->andFilterWhere(['like', 'tekst', $this->tekst])
            ->andFilterWhere(['like', 'priyomshik', $this->priyomshik])
            ->andFilterWhere(['like', 'inform_master', $this->inform_master]);

        return $dataProvider;
    }
}
