<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_templates".
 *
 * @property int $id
 * @property string $key Ключ
 * @property string $key_ru Ключ на русском
 * @property string $body Тело шаблона
 * @property int $deletable Удаляема сущность или нет
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['body', 'key_ru'], 'required'],
            [['key', 'key_ru'], 'string', 'max' => 255],
            [['deletable'], 'integer', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'key_ru' => 'Наименование',
            'body' => 'Тело',
            'deletable' => 'Deletable',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if($this->deletable == 0)
            return false;

        return parent::beforeDelete();
    }

    /**
     * @param $key
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByKey($key)
    {
        return self::find()->where(['key' => $key])->one();
    }

    /**
     * @param $data array
     * @return string
     */
    public function applyTags($data)
    {
        $body = preg_replace_callback('/{[a-z.]+}/', function($matches) use ($data) {
          foreach ($matches as $match)
          {
              $match = trim($match, '\{\}');
              return ArrayHelper::getValue($data, $match);
          }
        }, $this->body);

        return $body;
    }
}
