<?php

namespace app\models;

use Yii;
use app\models\Available;
use app\models\Cash;
use app\models\manual\Parts;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "shop".
 *
 * @property integer $id
 * @property integer $store_by
 * @property integer $price
 * @property integer $count
 * @property integer $client_by
 * @property integer $komment
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_by_cr', 'user_by_up', 'date_cr', 'date_up'], 'required'],
            [['story_by', 'price', 'count', 'client_by',  'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['komment','status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'story_by' => 'Товар',
            'price' => 'Сумма',
            'count' => 'Колич.',
            'client_by' => 'Клиент',
            'komment' => 'Коментарий',
            'user_by_cr' => 'Создатель',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
	

			public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {//если заказ завершен то ставим дату изменения на дату фактическую
			if ($this->status == 'finish'){
				
				
						//echo "<pre>".print_r($this,true)."</pre>";
						
				$cash = new Cash();
				$cash->type_cash_by = 6;
				$cash->summa = $this->price;
				$cash->type = 'income';
				$cash->user_by_cr = $this->user_by_cr;
				$cash->user_by_up = $this->user_by_up;
				$cash->date_cr = $this->date_cr;
				$cash->date_up = $this->date_up;
				$cash->comment = '';
				
					if($cash->save())
					{
						
						return true;	
					}
					else
					{
						echo "<pre>".print_r($cash,true)."</pre>";
					}
				
			}
			else
			{
				return true;
			
			}
		} else {
			return false;
		}
	}
	
	public function getClient()
	{
		return $this->hasOne(Client::className(), ['id' => 'client_by']);
	}
	
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by_cr']);
	}
	

}
