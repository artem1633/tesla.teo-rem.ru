<?php

namespace app\models;

use Yii;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "sending_sms".
 *
 * @property integer $id
 * @property string $name
 * @property string $tekst
 * @property integer $count
 * @property string $created_date
 * @property string $update_date
 */
class SendingSms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $otp;
    public $user_id;
    public static function tableName()
    {
        return 'sending_sms';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['tekst'], 'string'],
            [['count', 'company_id'], 'integer'],
            [['created_date', 'update_date'], 'safe'],
            [['name','status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название рассылки',
            'tekst' => 'Текст',
            'count' => 'Кол-во',
            'created_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
            'otp' => 'Отправка',
            'user_id' => "Клиенты",
            'status' => "Статус",
        ];
    }
}
