<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_object".
 *
 * @property integer $id
 * @property integer $sms_id
 * @property integer $user_id
 */
class SmsObject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sms_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_id' => 'Sms ID',
            'user_id' => 'User ID',
        ];
    }
}
