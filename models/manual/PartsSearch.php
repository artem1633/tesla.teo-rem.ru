<?php

namespace app\models\manual;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\manual\Parts;

/**
 * PartsSearch represents the model behind the search form about `app\models\manual\Parts`.
 */
class PartsSearch extends Parts
{
	 public $group_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'date_cr', 'date_up', 'company_id'], 'integer'],
			[['group_name', 'group_by'], 'string'],
            [['name', 'supplier', 'phone','group_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Parts::find();
	
			

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);
		
		

		
        // add conditions that should always apply here
        $this->load($params);
		
		
		$query->joinWith(['group'])->andFilterWhere(['like', 'type_device.name', $this->group_name]);
        $query->joinWith(['client'])->andFilterWhere(['like', 'client.name', $this->supplier]);


		
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		//Сортировка для нового столбца
		$dataProvider->sort->attributes['parts.group_name'] = [
        'asc' => ['name' => SORT_ASC],
        'desc' => ['name' => SORT_DESC],
			];

        // grid filtering conditions
        $query->andFilterWhere([
            'parts.id' => $this->id,
            'parts.group_by' => $this->group_by,
            'parts.date_cr' => $this->date_cr,
            'parts.date_up' => $this->date_up,
        ]);
	
		
		
        $query->andFilterWhere(['like', 'parts.name', $this->name])
           // ->andFilterWhere(['like', 'client.supplier', $this->supplier])
            ->andFilterWhere(['like', 'parts.phone', $this->phone]);

        return $dataProvider;
    }
}
