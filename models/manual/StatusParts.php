<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "status_parts".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 */
class StatusParts extends \yii\db\ActiveRecord
{
    const DEFAULT_STATUSES = [
        2 => 'Ожидание',
        3 => 'Необходимо заказать',
        4 => 'Поступили на склад'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status_parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_cr', 'date_up'], 'required'],
            [['date_cr', 'date_up'], 'integer'],
			[['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
}
