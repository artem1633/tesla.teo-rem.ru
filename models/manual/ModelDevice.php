<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "model_device".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 */
class ModelDevice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model_device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_cr', 'date_up'], 'required'],
            [['date_cr', 'date_up'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
}
