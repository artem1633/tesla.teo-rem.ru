<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "marketing".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 * @property boolean $show_in_indicators
 * @property integer $plan_month
 */
class Marketing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marketing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['show_in_indicators', 'boolean'],
            [['name', 'date_up'], 'required'],
            [['date_cr', 'date_up', 'plan_month'], 'integer'],
			[['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'show_in_indicators' => 'Отображать в показателях',
            'plan_month' => 'План в месяц',
        ];
    }
}
