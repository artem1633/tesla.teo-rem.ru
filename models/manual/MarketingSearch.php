<?php

namespace app\models\manual;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\manual\Marketing;

/**
 * MarketingSearch represents the model behind the search form about `app\models\manual\Marketing`.
 */
class MarketingSearch extends Marketing
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date_cr', 'date_up'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Marketing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
$query->orderBy('id DESC');
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
            'show_in_indicators' => $this->show_in_indicators,
            'plan_month' => $this->plan_month,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
