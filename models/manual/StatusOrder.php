<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "status_order".
 *
 * @property integer $id
 * @property string $name
 * @property integer $push
 * @property integer $date_cr
 * @property integer $date_up
 */
class StatusOrder extends \yii\db\ActiveRecord
{

    const DEFAULT_STATUSES = [
        1 => 'Готов',
        2 => 'В работе',
        3 => 'Принят на ремонт',
        4 => 'На диагностике',
        5 => 'На согласовании',
        6 => 'Ждет запчасть',
        7 => 'Выдан',
    ];
    /**
     * @inheritdoc
     */
    //public $shablon;
    public static function tableName()
    {
        return 'status_order';
    }

    /**
     * @inheritdoc
     */
    public $status;
    public function rules()
    {
        return [
            [['name', 'push', 'date_cr', 'date_up', 'tekst'/*, 'shablon'*/], 'required'],
            [['push', 'date_cr', 'date_up'], 'integer'],
			[['name'], 'unique'],
            [['name'], 'string', 'max' => 35],
            [['tekst'/*, 'shablon'*/], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',			
            'push' => 'Уведомления',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'tekst' => 'Текст',
            //'shablon' => 'Шаблон смса'
        ];
    }

    public function getStatus()
    {
        if($this->push == 1) return 'Есть';
        else return 'Нет';
    }
}
