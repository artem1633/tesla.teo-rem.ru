<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "group_client".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 */
class GroupClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_cr', 'date_up'], 'required'],
            [['date_cr', 'date_up'], 'integer'],
			[['name'], 'unique'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
}
