<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "type_cash".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 */
class TypeCash extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_cash';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_cr', 'date_up'], 'required'],
            [['date_cr', 'date_up'], 'integer'],
			[['name'], 'unique'],
            [['name'], 'string', 'max' => 35],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
}
