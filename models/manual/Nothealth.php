<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "nothealth".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 */
class Nothealth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nothealth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_cr', 'date_up'], 'required'],
            [['name'], 'string'],
			[['name'], 'unique'],
            [['date_cr', 'date_up'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date_cr' => 'Дата создания',
            'date_up' => 'Дата изменения',
        ];
    }
}
