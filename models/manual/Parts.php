<?php

namespace app\models\manual;

use Yii;
use app\models\Client;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "parts".
 *
 * @property integer $id
 * @property string $name
 * @property string $supplier
 * @property string $phone
 * @property integer $group_by
 * @property integer $date_cr
 * @property integer $date_up
 */
class Parts extends \yii\db\ActiveRecord
{
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','group_by', 'date_cr', 'date_up', 'supplier', 'price', 'price_shop'], 'required'],
            [['group_by', 'price', 'price_shop', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
			[['name'], 'unique'],
            [['phone'], 'string', 'max' => 20],
            ['supplier', 'validateSupplier'],	
			
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'price' => 'Цена продажи',
            'price_shop' => 'Цена закупа',
            'supplier' => 'Поставщик',
            'phone' => 'Тел.',
            'group_by' => 'Группа',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
	
	
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) 
		{
					
			return true;
		} else {
			return false;
		}
	}
	
	//Новый клиент 
	public function validateSupplier($attribute, $params)
	{
		$supplier = Client::find()->where(['id'=>$this->supplier])->one();
		if (!isset($supplier)){
			$supplier = new Client();
			
			//$this->client_by = "ффф";
			$supplier->name = $this->supplier;
			$supplier->group_by = 6;
			$supplier->type = "person";
			$supplier->discount_order = 0;
			$supplier->discount_parts = 0;
			$supplier->user_by_cr = Yii::$app->user->identity->id;
			$supplier->user_by_up = Yii::$app->user->identity->id;
			$supplier->date_cr = time();
			$supplier->date_up = time();
			
			if ($supplier->save()){
				$this->supplier = $supplier->id;
			}else{
				$this->addError($attribute,"Не создан новый Поставщик");
			//	echo "<pre>".print_r($supplier,true)."</pre>";
			}
		}
	}
	
	 public function getGroup()
    {
        return $this->hasOne(TypeDevice::className(), ['id' => 'group_by']);
    }
	
		public function getClient()
	{
		return $this->hasOne(Client::className(), ['id' => 'supplier']);
	}
}
