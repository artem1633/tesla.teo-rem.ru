<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $price
 * @property integer $date_cr
 * @property integer $date_up
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'date_cr', 'date_up'], 'required'],
            [['price', 'date_cr', 'date_up'], 'integer'],
            [['name'], 'unique'],
			[['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'price' => 'Цена',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
}
