<?php

namespace app\models\manual;

use Yii;

/**
 * This is the model class for table "type_order".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_cr
 * @property integer $date_up
 */
class TypeOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_cr', 'date_up'], 'required'],
            [['date_cr', 'date_up'], 'integer'],
			[['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
}
