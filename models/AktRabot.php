<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akt_rabot".
 *
 * @property integer $id
 * @property string $top
 * @property string $shablon_name
 * @property string $klient
 * @property string $device
 * @property string $repair
 * @property string $discount
 * @property string $prepay
 * @property string $warranty_period
 * @property string $client_signature
 * @property string $employee_signature
 */
class AktRabot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akt_rabot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['top'], 'string'],
            [['shablon_name', 'klient', 'device', 'repair', 'discount', 'prepay', 'warranty_period', 'client_signature', 'employee_signature'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'top' => 'Верхный часть',
            'shablon_name' => 'Названия шаблона',
            'klient' => 'Клиент',
            'device' => 'Устройство',
            'repair' => 'Не исправность',
            'discount' => 'Скидка',
            'prepay' => 'Предоплата',
            'warranty_period' => 'Гарантийный период',
            'client_signature' => 'Подпись клиента',
            'employee_signature' => 'Подпись сотрудника',
        ];
    }
}
