<?php

namespace app\models;

use Yii;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $phone
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $permission
 * @property integer $percent_sale
 * @property integer $percent_service
 * @property integer $percent_finish
 * @property integer $status
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 * @property integer $company
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    /*const ROLE_ADMIN = 999;
    const ROLE_MANAGER = 888;
    const ROLE_USER = 10;*/
    const USER_TYPE_ADMIN = 'admin';
    const USER_TYPE_MANAGER = 'maneger';
    const USER_TYPE_SUPER_ADMIN = 'super_admin';

    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {        
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    if (isset(Yii::$app->user->identity->id)) return Yii::$app->user->identity->company_id;
                    else return null;
                },
            ],
        ];
    }

    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'phone', 'auth_key',  'email', 'permission', 'percent_sale', 'percent_service', 'percent_finish', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['percent_sale', 'percent_service', 'percent_finish', 'status', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up','company', 'move'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [[ 'phone','auth_key', 'permission'], 'string', 'max' => 32],
            [['phone','username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'auth_key' => 'Пароль',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'permission' => 'Должность',
            'percent_sale' => 'За продажу(%)',
            'percent_service' => 'За обслуживание(%)',
            'percent_finish' => 'За выполнение(%)',
            'status' => 'Статус',
            'user_by_cr' => 'Создатель',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'move' => 'Доступ для перемешения',
        ];
    }
	
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
            
            if (isset(Yii::$app->user->identity->id)) {
                $log = new Logs([
                    'user_id' => Yii::$app->user->identity->id,
                    'event_datetime' => date('Y-m-d H:i:s'),
                    'event' => Logs::EVENT_USER_ADDED,
                ]);
                $log->description = $log->generateEventDescription();
                $log->save();
            }
            else {
                $log = new Logs([
                    'user_id' => null,
                    'event_datetime' => date('Y-m-d H:i:s'),
                    'event' => Logs::EVENT_USER_ADDED,
                ]);
                $log->description = $log->generateEventDescription();
                $log->save();
            }

			$this->password_hash =md5($this->auth_key);
			$this->company = \Yii::$app->user->getIdentity()->company;
			return true;
		} else {
			return false;
		}
	}
	
	public function afterSave($insert, $changedAttributes)
	{
			if ($this->permission == "admin"){
				$auth = Yii::$app->authManager;
				$admin = $auth->getRole('admin'); // Получаем роль admin
				$auth->assign($admin, $this->id);
			}elseif ($this->permission == "maneger"){
				$auth = Yii::$app->authManager;
				$maneger = $auth->getRole('maneger'); // Получаем роль meneger
				$auth->assign($maneger, $this->id);
			}elseif ($this->permission == "master"){
				$auth = Yii::$app->authManager;
				$master = $auth->getRole('master'); // Получаем роль master
				$auth->assign($master, $this->id);
			}else{
				$auth = Yii::$app->authManager;
				$guest = $auth->getRole('guest'); // Получаем роль guest
				$auth->assign($guest, $this->id);
			}
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['admin_id' => 'id']);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

}
