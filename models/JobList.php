<?php

namespace app\models;

use Yii;
use app\models\manual\Service;
use app\models\manual\Parts;

/**
 * This is the model class for table "job_list".
 *
 * @property integer $id
 * @property integer $order_by
 * @property integer $job_by
 * @property integer $count
 * @property integer $price
 * @property integer $date_cr
 * @property integer $date_up
 */
class JobList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_by', 'job_by', 'count', 'price', 'date_cr', 'date_up'], 'required'],
            [['order_by', 'job_by', 'count', 'price', 'date_cr', 'date_up'], 'integer'],
            [['type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_by' => 'Заказ',
            'job_by' => '',
            'type' => 'Тип',
            'count' => 'Кол.',
            'price' => 'Цена',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
        ];
    }
	
	
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {//если заказ завершен то ставим дату изменения на дату фактическую
			if ($this->type == "parts"){
				$order = Order::findOne($this->order_by);
				//$available = Available::find()->where(['parts_by'=>$this->job_by, 'store_by' => $order->user_by])->one();
				$resource = Resource::find()->where(['id'=>$this->job_by/*, 'store_by' => $order->user_by*/])->one();
				if($resource != null){
					$resource->count = $resource->count  - $this->count;		
					$resource->save();
				}
			}	
			return true;
		} else {
			return false;
		}
	}
	
	public function getJob()
	{	
		if ($this->type == 'parts')
		{
			$job = $this->hasOne(Parts::className(), ['id' => 'job_by']);	
		}
		else
		{
			$job = $this->hasOne(Service::className(), ['id' => 'job_by']);
		}
		
		return $job;
	}
	
	//отключение  кнопки удаления строки работ 
	public function getStatus_order($id)
	{
		$order = Order::find()->where(['id'=>$id])->one();
		return $order->status_order;
	}
}
