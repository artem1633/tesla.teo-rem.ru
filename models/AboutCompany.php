<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about_company".
 *
 * @property integer $id
 * @property string $name
 * @property string $adress
 * @property string $bank
 * @property string $director
 * @property string $email
 * @property string $phone
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 */
class AboutCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $login;
    public $parol;
    public static function tableName()
    {
        return 'about_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'adress', 'bank', 'director', 'email', 'phone', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up'], 'required'],
            [['user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['name', 'adress', 'smslog','smspas','bank', 'director', 'email', 'site_name', 'login', 'parol'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'adress' => 'Адрес',
            'bank' => 'Bank',
            'director' => 'Руководитель',
            'smslog' => 'Логин смс сервиса',
            'smspas' => 'Пароль смс сервиса',
            'email' => 'Email',
            'phone' => 'Телефон',
            'user_by_cr' => 'Создал',
            'user_by_up' => 'Редактировал',
            'date_cr' => 'Дата создания',
            'date_up' => 'Дата редактирования',
            'site_name' => 'Названия сайта',
            'login'=> 'Новый логин смс сервиса',
            'parol' => 'Новый пароль смс сервиса'
        ];
    }
}
