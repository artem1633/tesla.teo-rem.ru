<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Available;

/**
 * AvailableSearch represents the model behind the search form about `app\models\Available`.
 */
class AvailableSearch extends Available
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_parts_by', 'parts_by','supplier', 'store_by', 'status_parts_by', 'count','price','price_shop', 'type_parts_by', 'number', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'storage_id', 'company_id'], 'integer'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Available::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 20,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group_parts_by' => $this->group_parts_by,
            'parts_by' => $this->parts_by,
            'store_by' => $this->store_by,
            'status_parts_by' => $this->status_parts_by,
            'count' => $this->count,
            'type_parts_by' => $this->type_parts_by,
            'user_by_cr' => $this->user_by_cr,
            'user_by_up' => $this->user_by_up,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
