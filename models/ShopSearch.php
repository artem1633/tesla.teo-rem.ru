<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Shop;

/**
 * ShopSearch represents the model behind the search form about `app\models\Shop`.
 */
class ShopSearch extends Shop
{
	public $client_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_by', 'price', 'count', 'client_by', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'company_id'], 'integer'],
            [['komment','client_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],

        ]);

        $this->load($params);

	//	echo "<br/>".strtotime($this->date_cr);
	//	echo "<br/>".$this->date_cr;
	//	echo "<br/>".date('Y.m.d H:i', strtotime($this->date_cr));
	//	$a=date('Y.m.d H:i', strtotime($this->date_cr));
		$query->joinWith(['client'])->andFilterWhere(['like', Client::tableName().'.name', $this->client_name]);
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
				//Сортировка для нового столбца
		$dataProvider->sort->attributes['client_name'] = [
        'asc' => ['name' => SORT_ASC],
        'desc' => ['name' => SORT_DESC],
			];

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_by' => $this->store_by,
            'price' => $this->price,
            'count' => $this->count,
            'client_by' => $this->client_by,
            'komment' => $this->komment,
            'user_by_cr' => $this->user_by_cr,
            'user_by_up' => $this->user_by_up,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
        ]);

        return $dataProvider;
    }
}
