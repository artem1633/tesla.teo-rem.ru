<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rates".
 *
 * @property int $id
 * @property string $name Наименование
 * @property double $price Стоимость тарифа
 * @property double $time Период действия лицензии с момента ее получения (в секундах)
 * @property double $sort Сортировка
 *
 * @property Companies[] $companies
 */
class Rates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'time'], 'required'],
            [['price', 'time', 'sort'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'price' => 'Стоимость',
            'time' => 'Время',
            'sort' => 'Сортировка',
        ];
    }

    public static function getTimeList()
    {
        return [
            '604800' => 'Неделя',
            '1209600' => '2 недели',
            '1814400' => '3 недели',
            '2419200' => '1 месяц',
            '4838400' => '2 месяца',
            '7257600' => '3 месяца',
            '14515200' => '6 месяцов',
            '29030400' => '1 год',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['rate_id' => 'id']);
    }
}
