<?php

namespace app\models;

use Yii;
use app\models\manual\TypeOrder;
use app\models\manual\Nothealth;
use app\models\manual\TypeDevice;
use app\models\User;
use app\models\Client;
use app\models\manual\Marketing;
use app\models\manual\StatusOrder;
use app\models\manual\Made;
use app\models\manual\ModelDevice;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $return_waid
 * @property integer $return_date
 * @property integer $type_order_by
 * @property integer $client_by
 * @property integer $type_device_by
 * @property integer $made_by
 * @property integer $model_device_by
 * @property string $nothealth_by
 * @property string $view
 * @property string $configuration
 * @property string $comment
 * @property integer $price
 * @property integer $prepay
 * @property integer $user_by
 * @property integer $marketing_by
 * @property integer $discount_order
 * @property integer $status_order
 * @property integer $quick
 * @property integer $user_by_cr
 * @property integer $user_by_up
 * @property integer $date_cr
 * @property integer $date_up
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $search_status;
    public $search_days;
    public $search_id;
    public $search_client;
    public $search_master;
    public $search_texnika;
    public $link_time;
    public $search_address;
    public $search_tel;
    public $search_vidat_from;
    public $search_vidat_to;
    public $search_serial_number;
    public static function tableName()
    {
        return 'order';
    }

      public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if (isset(Yii::$app->user->identity->id)){
            $companyId = Yii::$app->user->identity->company_id;
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public $akt;
    public function rules()
    {
        return [
            [[ 'type_order_by', 'client_by', 'made_by','type_device_by','marketing_by', 'nothealth_by', 'discount_order',  'status_order','phone'/*, 'client_address'*/], 'required'],
            [['type_order_by', 'type_device_by', 'price', 'prepay', 'user_by', 'marketing_by', 'discount_order', 'status_order', 'quick', 'user_by_cr', 'user_by_up', 'date_cr', 'date_up', 'return_date', 'company_id'], 'integer'],
            [['view', 'configuration', 'comment','client_address', 'serial_number'], 'string', 'max' => 255],
			[['return_waid'/*, 'return_date'*/, 'phone', 'akt'],'string'],
			['made_by', 'validateMade'],
			['nothealth_by', 'validateNothealth'],
			['client_by', 'validateClient'],
			['model_device_by', 'validateModeldevice'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
           return [
            'id' => 'Код',
            'return_waid' => 'Дата создания',//'Выдать',
            'return_date' => 'Фактическая дата',
            'type_order_by' => 'Тип заказа',
            'client_by' => 'Клиент',
            'phone' => 'Телефон',
            'type_device_by' => 'Устройство',
            'made_by' => 'Производитель',
            'model_device_by' => 'Модель',
            'nothealth_by' => 'Не исправность',
            'view' => 'Внешний вид',
            'configuration' => 'Комплектация',
            'comment' => 'Коментарий',
            'price' => 'Цена',
            'prepay' => 'Предоплата',
            'user_by' => 'Мастер',
            'marketing_by' => 'Реклама',
            'discount_order' => 'Скидка',
            'status_order' => 'Статус',
            'quick' => 'Срочный',
            'user_by_cr' => 'Принял',
            'user_by_up' => 'Изменял', 
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'client_address' => 'Адрес клиента',
            'serial_number' => 'Серийный номер',
        ];
    }
	
	public function beforeSave($insert)
	{
		$tel = $this->phone; $f="";
        $strlen = strlen( $tel );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $tel, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $f .= $char;
        }
        $this->phone = $f;

		if (parent::beforeSave($insert)) {//если заказ завершен то ставим дату изменения на дату фактическую
			if($this->status_order == 7){
				
				$cash = new Cash();
				$cash->type_cash_by = 7;
				$cash->summa = $this->price;
				$cash->type = 'income';
				$cash->user_by_cr = $this->user_by_cr;
				$cash->user_by_up = $this->user_by_up;
				$cash->date_cr = $this->date_cr;
				$cash->date_up = $this->date_up;
				$cash->comment = "";
				$cash->save();
				
				//$this->return_date =$this->date_up;
				
				}		
				
			$this->return_waid =strtotime($this->return_waid);
		
			return true;
		} else {
			return false;
		}
	}
	
	//Новый производитель
	public function validateMade($attribute, $params)
	{
		$made = Made::find()->where(['id'=>$this->made_by])->one();
		if (!isset($made)){
			
			$made = new Made();
			$made->name = $this->made_by;
			$made->date_cr = time();
			$made->date_up = time();
			
			if ($made->save()){//находим новый производитель
				$this->made_by = $made->id;
			}else{
				$this->addError($attribute,"Не создана новый производитель");
				//echo "<pre>".print_r($nothealth,true)."</pre>";
			}
		}
	}	
	
	//Новая неисправность 
	public function validateNothealth($attribute, $params)
	{
		$nothealth = Nothealth::find()->where(['id'=>$this->nothealth_by])->one();
		if (!isset($nothealth)){
			$nothealth = new Nothealth();
			$nothealth->name = $this->nothealth_by;
			$nothealth->date_cr = time();
			$nothealth->date_up = time();
			
			if ($nothealth->save()){//находим новую модель
				$this->nothealth_by = $nothealth->id;
			}else{
				$this->addError($attribute,"Не создана новая неисправность");
				//echo "<pre>".print_r($nothealth,true)."</pre>";
			}
		}
	}
	
	//Новая модель 
	public function validateModeldevice()
	{
		$modelDevice = ModelDevice::find()->where(['id'=>$this->model_device_by])->one();
		if (!isset($modelDevice)){
			$modelDevice = new ModelDevice();
			$modelDevice->name = $this->model_device_by;
			$modelDevice->date_cr = time();
			$modelDevice->date_up = time();
			
			if ($modelDevice->save()){//находим новую модель
				$this->model_device_by = $modelDevice->id;
			}else{
				$this->addError($attribute,"Не создана новая модель");
			//	echo "<pre>".print_r($client,true)."</pre>";
			}
		}
	}
	
	//Новый клиент 
	public function validateClient($attribute, $params)
	{
		$client = Client::find()->where(['id'=>$this->client_by])->one();
		if (!isset($client)){
			$client = new Client();
			
			//$this->client_by = "ффф";
			$client->name = $this->client_by;
			$client->group_by = 7;
			$client->ogrn = '';
			$client->expense = '';
			$client->email = '';
			$client->type = "person";
			$client->phone = $this->phone;
			$client->discount_order = $this->discount_order;			
			if($this->client_address == null) $client->adress = "1";
			else $client->adress = $this->client_address;
			$client->discount_parts = 0;
			$client->user_by_cr = Yii::$app->user->identity->id;
			$client->user_by_up = Yii::$app->user->identity->id;
			$client->date_cr = time();
			$client->date_up = time();
			
			if ($client->save()){
				$this->client_by = $client->id;
			}else{
				$this->addError($attribute,"Не создан новый клиент");
				echo "<pre>".print_r($client,true)."</pre>";
			}
		}
	}
	//клиент

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
	{
		return $this->hasOne(Client::className(), ['id' => 'client_by']);
	}
	//Создатель

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsercr()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by_cr']);
	}


    /**
     * @return \yii\db\ActiveQuery
     */
	public function getUserup()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by_up']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
	//Мастер
	public function getUsermas()
	{
		return $this->hasOne(User::className(), ['id' => 'user_by']);
	}

	public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	//статус заказа
	public function getStatus()
	{
		return $this->hasOne(StatusOrder::className(), ['id' => 'status_order']);
	}


    /**
     * @return \yii\db\ActiveQuery
     */
	//тип заказа
	public function getTypeorder()
	{
		return $this->hasOne(TypeOrder::className(), ['id' => 'type_order_by']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
		//статус заказа
	public function getNothealth()
	{
		return $this->hasOne(Nothealth::className(), ['id' => 'nothealth_by']);
	}


    /**
     * @return \yii\db\ActiveQuery
     */
	public function getDevice()
	{
		return $this->hasOne(TypeDevice::className(), ['id' => 'type_device_by']);
	}

	public function getType_device()
	{
		return $this->hasOne(TypeDevice::className(), ['id' => 'type_device_by']);
	}


    /**
     * @return \yii\db\ActiveQuery
     */
	public function getMade()
	{
		return $this->hasOne(Made::className(), ['id' => 'made_by']);
	}


    /**
     * @return \yii\db\ActiveQuery
     */
	public function getModeldevice()
	{
		return $this->hasOne(ModelDevice::className(), ['id' => 'model_device_by']);
	}


    /**
     * @return \yii\db\ActiveQuery
     */
	public function getMarketing()
	{
		$value = $this->hasOne(Marketing::className(), ['id' => 'marketing_by']);
		if (!isset($value)){$value = $this->hasOne(Marketing::className(), ['id' => 18]);}
		return $value;
	}
	
}
