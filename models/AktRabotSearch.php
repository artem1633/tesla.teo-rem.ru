<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AktRabot;

/**
 * AktRabotSearch represents the model behind the search form about `app\models\AktRabot`.
 */
class AktRabotSearch extends AktRabot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['top', 'shablon_name', 'klient', 'device', 'repair', 'discount', 'prepay', 'warranty_period', 'client_signature', 'employee_signature'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AktRabot::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'top', $this->top])
            ->andFilterWhere(['like', 'shablon_name', $this->shablon_name])
            ->andFilterWhere(['like', 'klient', $this->klient])
            ->andFilterWhere(['like', 'device', $this->device])
            ->andFilterWhere(['like', 'repair', $this->repair])
            ->andFilterWhere(['like', 'discount', $this->discount])
            ->andFilterWhere(['like', 'prepay', $this->prepay])
            ->andFilterWhere(['like', 'warranty_period', $this->warranty_period])
            ->andFilterWhere(['like', 'client_signature', $this->client_signature])
            ->andFilterWhere(['like', 'employee_signature', $this->employee_signature]);

        return $dataProvider;
    }
}
