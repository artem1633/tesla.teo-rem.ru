<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...
        
        // Создадим роли админа и редактора новостей
        $admin 	= $auth->createRole('admin');
        $maneger = $auth->createRole('maneger');
        $master = $auth->createRole('master');
        $guest = $auth->createRole('guest');
        
        // запишем их в БД
        $auth->add($admin);
        $auth->add($maneger);
        $auth->add($master);
        $auth->add($guest);
        
        // Создаем разрешения. Например, просмотр админки viewAdminPage и редактирование новости updateNews
        $listOrder = $auth->createPermission('listOrder');
        $listOrder->description = 'Просмотр списка заказов';
        
        $createNews = $auth->createPermission('createOrder');
        $createNews->description = 'Создавать заказ';
        
        // Запишем эти разрешения в БД
        $auth->add($listOrder);
        $auth->add($createNews);
        
        // Теперь добавим наследования. Для роли editor мы добавим разрешение updateNews,
        // а для админа добавим наследование от роли editor и еще добавим собственное разрешение listOrder
        
        // Роли «Редактор новостей» присваиваем разрешение «Редактирование новости»
        $auth->addChild($maneger,$createNews);
		
        $auth->addChild($maneger,$listOrder);

        // админ наследует роль редактора новостей. Он же админ, должен уметь всё! :D
        $auth->addChild($admin, $maneger);
        
        // Еще админ имеет собственное разрешение - «Просмотр админки»
        $auth->addChild($admin, $listOrder);

        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 5); 
        
        // Назначаем роль editor пользователю с ID 2
        $auth->assign($maneger, 12);
    }
}