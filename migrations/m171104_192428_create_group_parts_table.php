<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group_parts`.
 */
class m171104_192428_create_group_parts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_parts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('group_parts');
    }
}
