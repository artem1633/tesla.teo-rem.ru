<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage`.
 */
class m180123_034548_create_storage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('storage', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'user_id' => $this->integer(),
            'is_main' => $this->boolean(),
        ]);

        $this->createIndex('idx-storage-user_id', 'storage', 'user_id', false);
        $this->addForeignKey("fk-storage-user_id", "storage", "user_id", "user", "id");

        $this->insert('storage',array(
            'user_id' => null,
            'name' => 'Основной склад',
            'is_main' => 1,
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-storage-user_id','storage');
        $this->dropIndex('idx-storage-user_id','storage');

        $this->dropTable('storage');
    }
}
