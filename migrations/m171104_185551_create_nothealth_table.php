<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nothealth`.
 */
class m171104_185551_create_nothealth_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nothealth', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nothealth');
    }
}
