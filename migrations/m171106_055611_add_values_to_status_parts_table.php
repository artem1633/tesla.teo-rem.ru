<?php

use yii\db\Migration;

class m171106_055611_add_values_to_status_parts_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_055611_add_values_to_status_parts_table cannot be reverted.\n";

        return false;
    }
    public function up()
    {
         Yii::$app->db->createCommand()->batchInsert('status_parts', ['id', 'name', 'date_cr', 'date_up'], [
            [2, 'Ожидание', time(), time()],
            [3, 'Необходимо заказать', time(), time()],
            [4, 'Поступили на склад', time(), time()],
        ])->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_055611_add_values_to_status_parts_table cannot be reverted.\n";

        return false;
    }
    */
}
