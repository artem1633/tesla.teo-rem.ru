<?php

use yii\db\Migration;

class m171106_061422_add_values_to_model_device_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_061422_add_values_to_model_device_table cannot be reverted.\n";

        return false;
    }
    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('model_device', ['id', 'name', 'date_cr', 'date_up'], [
            [8, 'Не указана', time(), time()],
            [9, 'D821 Nexus 5', time(), time()],
            [10, 'TL-20S1W', time(), time()],
            [11, 'ST 27i', time(), time()],
            [12, 'mini 2 L', time(), time()],
            [13, 'TM-4772', time(), time()],
            [14, '1112', time(), time()],
            [15, '3310', time(), time()],
            [16, 'X13-12056', time(), time()],
            [17, 'F2C-00201', time(), time()],
            [18, 'HD 6850', time(), time()],
            [19, 'CTH-680', time(), time()],
            [20, 'Aspire 5741G-353G25Misk', time(), time()],
            [21, 'VA-4SD', time(), time()],
            [22, 'Новая модель', time(), time()],
            [23, 'Informer 701', time(), time()],
            [24, 'Т7001В', time(), time()],
            [25, 'Aspire 5710Z', time(), time()],
            [26, 'CM-2', time(), time()],
            [27, '101', time(), time()],
            [28, '584029-251', time(), time()],
            [29, 'AirTab P72w', time(), time()],
            [30, 'M727G', time(), time()],
            [31, 'G570', time(), time()],
            [32, '9410Z', time(), time()],
            [33, 'PN-975', time(), time()],
            [34, '5541', time(), time()],
            [35, 'P780', time(), time()],
            [36, 'GT-I8552', time(), time()],
            [37, 'MS2384', time(), time()],
            [38, 'XL', time(), time()],
            [39, 'S110', time(), time()],
            [40, 'LC0804B', time(), time()],
            [41, 'Tab 4', time(), time()],
            [42, 'X55VD-SN264H', time(), time()],
            [43, 'PAP4500', time(), time()],
            [44, 'W253BZQ', time(), time()],
            [45, 'Aspire 7735ZG', time(), time()],
            [46, 'S4501M', time(), time()],
            [47, 'sm-t211', time(), time()],
            [48, 'pavilion dv7', time(), time()],
            [49, 'C90S', time(), time()],
            [50, 'K012', time(), time()],
            [51, 'VG70', time(), time()],
            [52, 'aspire one', time(), time()],
            [53, '6-1254er', time(), time()],
            [54, 'R203', time(), time()],
            [55, 'X16-96092', time(), time()],
            [56, 'HTC Desire 310', time(), time()],
            [57, 'one S', time(), time()],
            [58, 'Iphone 4', time(), time()],
            [59, 'Iphone 4s', time(), time()],
            [60, 'Iphone 5', time(), time()],
            [61, 'Iphone 5c', time(), time()],
            [62, 'Iphone 5s', time(), time()],
            [63, 'Iphone 6', time(), time()],
            [64, 'Iphone 6 plus', time(), time()],
            [65, 'Iphone 6s', time(), time()],
            [66, 'Iphone 6s plus', time(), time()],
            [67, 'Ipad 3', time(), time()],
            [68, 'Ipad 2', time(), time()],
            [69, 'X53B', time(), time()],
            [70, 'V3-551G', time(), time()],
            [71, 'RM-1090', time(), time()],
            [72, 'Паша', time(), time()],
            [73, '6010X', time(), time()],
            [74, '4S', time(), time()],
            [75, 'TM-8041', time(), time()],
            [76, 'M100', time(), time()],
            [77, 'А3', time(), time()],
            [78, 'K10', time(), time()],
            [79, 'N8', time(), time()],
            [80, '5S', time(), time()],
            [81, 'star tv', time(), time()],
            [82, '571G', time(), time()],
            [83, 'GT-P5200', time(), time()],
            [84, 'l850', time(), time()],
            [85, 'FS452', time(), time()],
            [86, 'Desire 500', time(), time()],
            [87, 'Vega', time(), time()],
            [88, '5', time(), time()],
            [89, 'T100TAF', time(), time()],
            [90, '940VW', time(), time()],
            [91, '1200', time(), time()],
            [92, 'C850', time(), time()],
            [93, 'Zera F', time(), time()],
            [94, 'GT-i9100', time(), time()],
            [95, 'ZR C5503', time(), time()],
            [96, 'spark 2', time(), time()],
            [97, 'ot-4032d', time(), time()],
            [98, '6555', time(), time()],
            [99, 'W33', time(), time()],
            [100, 'np535', time(), time()],
            [101, '825 ft', time(), time()],
            [102, 'gs 8305', time(), time()],
            [103, 'U8950-1', time(), time()],
            [104, 'F15', time(), time()],
            [105, 'RF-9300', time(), time()],
            [106, 'MediaPad 10', time(), time()],
            [107, 'air', time(), time()],
            [108, 'wildfire s', time(), time()],
            [109, 'iPad mini 2', time(), time()],
            [110, 'HP mini', time(), time()],
            [111, '4570', time(), time()],
            [112, 'multipad 4', time(), time()],
            [113, 'dv6', time(), time()],
            [114, '7070MG', time(), time()],
            [115, 'N5010', time(), time()],
            [116, '5220', time(), time()],
            [117, 'MFLogin3T', time(), time()],
            [118, 'Fonepad 7 LTE', time(), time()],
            [119, 'LT25i', time(), time()],
            [120, 'А2010', time(), time()],
            [121, 'x553m', time(), time()],
            [122, 'ZE7', time(), time()],
            [123, 'N15W4', time(), time()],
            [124, 'a6010', time(), time()],
        ])->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_061422_add_values_to_model_device_table cannot be reverted.\n";

        return false;
    }
    */
}
