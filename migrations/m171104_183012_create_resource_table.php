<?php

use yii\db\Migration;

/**
 * Handles the creation of table `resource`.
 */
class m171104_183012_create_resource_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('resource', [
            'id' => $this->primaryKey(),
            'parts_by' => $this->integer()->notNull()->comment('Parts By'),
            'store_by' => $this->integer()->notNull()->comment('Store By'),
            'status_parts_by' => $this->integer()->notNull()->comment('Status Parts By'),
            'count' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'type_parts_by' => $this->integer()->notNull()->comment('Type Parts By'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('resource');
    }
}
