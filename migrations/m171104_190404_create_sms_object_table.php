<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms_object`.
 */
class m171104_190404_create_sms_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sms_object', [
            'id' => $this->primaryKey(),
            'sms_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sms_object');
    }
}
