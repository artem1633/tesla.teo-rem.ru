<?php

use yii\db\Migration;

/**
 * Handles adding storage_id to table `resource`.
 */
class m180123_085839_add_storage_id_column_to_resource_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('resource', 'storage_id', $this->integer());
        $this->createIndex('idx-resource-storage_id', 'resource', 'storage_id', false);
        $this->addForeignKey("fk-resource-storage_id", "resource", "storage_id", "storage", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-resource-storage_id','resource');
        $this->dropIndex('idx-resource-storage_id','resource');
        $this->dropColumn('resource', 'storage_id');
    }
}
