<?php

use yii\db\Migration;
use app\models\Storage;
use app\models\Companies;

/**
 * Class m180607_113947_add_default_value_to_storage_table
 */
class m180607_113947_add_default_value_to_storage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \Yii::$app->db->createCommand()->update('about_company', ['company_id' => 1], [ 'id' => 1 ])->execute();
        
        $storages = Storage::find()->all();
        foreach ($storages as $value) {
            \Yii::$app->db->createCommand()->update('storage', ['company_id' => 1], [ 'id' => $value->id ])->execute();
        }
    }

    public function safeDown()
    {

    }
}
