<?php

use yii\db\Migration;

class m171106_060932_add_values_to_made_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_060932_add_values_to_made_table cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('made', ['id', 'name', 'date_cr', 'date_up'], [
            [1, 'Китай', time(), time()],
            [2, 'Fly', time(), time()],
            [3, 'Samsung', time(), time()],
            [4, 'Lg', time(), time()],
            [5, 'Apple', time(), time()],
            [6, 'Huawei', time(), time()],
            [7, 'HP', time(), time()],
            [8, 'Lenovo', time(), time()],
            [9, 'Asus', time(), time()],
            [10, 'Acer', time(), time()],
            [11, 'Dell', time(), time()],
            [12, 'Sony', time(), time()],
            [13, 'Texet', time(), time()],
            [14, 'Wexler', time(), time()],
            [15, 'Alcatel', time(), time()],
            [16, 'Haier', time(), time()],
            [17, 'HTC', time(), time()],
            [18, 'DEXP', time(), time()],
            [19, 'Highscreen', time(), time()],
            [20, 'Irbis', time(), time()],
            [21, 'Lexand', time(), time()],
            [22, 'Meizu', time(), time()],
            [23, 'Micromax', time(), time()],
            [24, 'Microsoft', time(), time()],
            [25, 'Motorola', time(), time()],
            [26, 'One+', time(), time()],
            [27, 'Philips', time(), time()],
            [28, 'Prestigio', time(), time()],
            [29, 'ZTE', time(), time()],
            [30, 'MSI', time(), time()],
            [31, 'Packard Bell', time(), time()],
            [32, 'Panasonic', time(), time()],
            [33, 'ARCHOS', time(), time()],
            [34, 'BB-mobile', time(), time()],
            [35, 'Digma', time(), time()],
            [36, 'GiNZZU', time(), time()],
            [37, 'Oysters', time(), time()],
            [38, 'Rover', time(), time()],
            [39, 'Supra', time(), time()],
            [40, 'Tesla', time(), time()],
            [41, 'TECKTON', time(), time()],
            [42, 'Radeon', time(), time()],
            [43, 'Wacom', time(), time()],
            [44, 'HUMAX', time(), time()],
            [46, 'Mistery', time(), time()],
            [49, 'Expley', time(), time()],
            [50, 'Crown', time(), time()],
            [51, 'Nokia', time(), time()],
            [52, 'DNS', time(), time()],
            [53, 'g-pad', time(), time()],
            [54, 'microlab', time(), time()],
            [55, 'H.D.D.', time(), time()],
            [56, 'Gigabyte', time(), time()],
            [57, 'Toshiba', time(), time()],
            [58, 'Depo', time(), time()],
            [59, 'Xiaomi', time(), time()],
            [60, 'Parkcity', time(), time()],
            [61, 'Dr.HD', time(), time()],
            [62, 'Ritmix', time(), time()],
            [63, 'view sonic', time(), time()],
            [64, 'Zotac', time(), time()],
            [65, 'BQ', time(), time()],
            [66, 'Мегафон', time(), time()],
        ])->execute();
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_060932_add_values_to_made_table cannot be reverted.\n";

        return false;
    }
    */
}
