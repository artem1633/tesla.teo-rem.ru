<?php

use yii\db\Migration;

/**
 * Handles the creation of table `available`.
 */
class m171104_182504_create_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('available', [
            'id' => $this->primaryKey(),
            'group_parts_by' => $this->integer()->comment('Група'),
            'parts_by' => $this->integer()->notNull()->comment('Товар'),
            'supplier' => $this->integer()->notNull()->comment('Поставщик'),
            'store_by' => $this->integer()->notNull()->comment('Склад'),
            'status_parts_by' => $this->integer()->notNull()->comment('Статус'),
            'count' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'price_shop' => $this->integer()->notNull()->comment('Цена закупа'),
            'type_parts_by' => $this->integer()->notNull()->comment('Тип'),
            'comment' => $this->string(255),
            'number' => $this->integer()->notNull(),
            'user_by_cr' => $this->integer()->notNull(),
            'user_by_up' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('available');
    }
}
