<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180522_042634_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);

        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings', [
            'key' => 'vk_access_token',
            'value' => 'c63737affb195896217c0a1fec77646b030cbe434c0629e79c8bfea94a3daad8265b07be8b5c8030591ac',
            'label' => 'Токен для бота VK',
        ]);

        $this->insert('settings',array(
            'key' => 'telegram_access_token',
            'value' => '508288308:AAFjfqet_ndbpgUbxPFm2b9gycn0XAy4mNU',
            'label' => 'Токен для бота телеграм',
        ));

        $this->insert('settings',array(
            'key' => 'proxy_server',
            'value' => '148.251.238.124:1080',
            'label' => 'Proxy',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
