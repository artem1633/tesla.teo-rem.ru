<?php

use yii\db\Migration;

class m171106_054900_add_values_to_type_cash_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_054900_add_values_to_type_cash_table cannot be reverted.\n";

        return false;
    }
    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('type_cash', ['id', 'name', 'date_cr', 'date_up'], [
            [5, 'Выдача ЗП', time(), time()],
            [6, 'Продажа товара', time(), time()],
            [7, 'Оказание услуг', time(), time()],
        ])->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_054900_add_values_to_type_cash_table cannot be reverted.\n";

        return false;
    }
    */
}
