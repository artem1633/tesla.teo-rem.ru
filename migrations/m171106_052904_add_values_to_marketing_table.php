<?php

use yii\db\Migration;

class m171106_052904_add_values_to_marketing_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_052904_add_values_to_marketing_table cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('marketing', ['id', 'name', 'date_cr', 'date_up'], [
            [ 11, 'Газета', time(), time()],
            [ 12, 'Группа в ВК', time(), time()],
            [ 13, 'Друзья', time(), time()],
            [ 14, 'Визитка', time(), time()],
            [ 15, 'Реклама в ВК', time(), time()],
            [ 16, 'Листовки на подъездах', time(), time()],
            [ 17, 'Реклама в лифте', time(), time()],
            [ 18, 'Не указано', time(), time()],
        ])->execute();

        \Yii::$app->db->createCommand()->delete('marketing', ['id' => 1])->execute();
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_052904_add_values_to_marketing_table cannot be reverted.\n";

        return false;
    }
    */
}
