<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_company`.
 */
class m171104_171427_create_about_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('about_company', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Название'),
            'adress' => $this->string(255)->notNull(),
            'bank' => $this->string(255)->notNull(),
            'director' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'phone' => $this->string(60)->notNull(),
            'user_by_cr' => $this->integer()->notNull(),
            'user_by_up' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
            'smslog' => $this->string(100)->notNull()->comment('Логин смс сервиса'),
            'smspas' => $this->string(100)->notNull()->comment('Пароль смс сервиса'),
            'site_name' => $this->string(255)->comment('Названия сайта'),
        ]);

        $this->insert('about_company',array(
            'name' => 'Компания "Цифровые Технологии" (ИП Боровиков А.В. ИНН 182908894783)',
            'adress' => 'г. Глазов, ул. Молодой гвардии, д. 9',
            'bank' => 'Ижкомбанк',
            'director' => 'Боровиков Андрей Васильевич',
            'email' => 'tv-glazov@mail.ru',
            'phone' => '(34141) 66-001, 8-912-022-82-22',
            'user_by_cr' => 1,
            'user_by_up' => 1,
            'date_cr' => time(),
            'date_up' => time(),
            'smslog' => 'teo',
            'smspas' => 'vRe4Og0t08',
            'site_name' => 'shop-crm.ru',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('about_company');
    }
}
