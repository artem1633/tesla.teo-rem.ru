<?php

use yii\db\Migration;

class m171105_162704_add_values_to_order_status extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171105_162704_add_values_to_order_status cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->insert('status_order', 
                array(
                'id' => '1',
                'name' => 'Готов',
                'push' => 1,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            )
        );
        $this->insert('status_order', 
            array(
                'id' => '2',
                'name' => 'В работе',
                'push' => 1,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            ));
        $this->insert('status_order',         
            array(
                'id' => '3',
                'name' => 'Принят на ремонт',
                'push' => 0,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            ));
        $this->insert('status_order',         
            array(
                'id' => '4',
                'name' => 'На диагностике',
                'push' => 0,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            ));
        $this->insert('status_order', 
            array(
                'id' => '5',
                'name' => 'На согласовании',
                'push' => 0,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            ));
        $this->insert('status_order', 
            array(
                'id' => '6',
                'name' => 'Ждет запчасть',
                'push' => 1,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            ));
        $this->insert('status_order', 
            array(
                'id' => '7',
                'name' => 'Выдан',
                'push' => 0,
                'date_cr' => time(),
                'date_up' => time(),
                'tekst' => 'Ваш заказ № {order_id} {status_name}, сумма ремонта {summa} руб. Компания TEO-CRM',
            ));
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171105_162704_add_values_to_order_status cannot be reverted.\n";

        return false;
    }
    */
}
