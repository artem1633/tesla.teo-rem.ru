<?php

use yii\db\Migration;

class m171106_060024_add_values_to_type_parts_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_060024_add_values_to_type_parts_table cannot be reverted.\n";

        return false;
    }

    public function up()
    {
         Yii::$app->db->createCommand()->batchInsert('type_parts', ['id', 'name', 'date_cr', 'date_up'], [
            [2, 'Новый', time(), time()],
            [3, 'Б/У', time(), time()],
        ])->execute();

    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_060024_add_values_to_type_parts_table cannot be reverted.\n";

        return false;
    }
    */
}
