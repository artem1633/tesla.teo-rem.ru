<?php

use yii\db\Migration;

/**
 * Handles the creation of table `template`.
 */
class m171104_184459_create_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('template', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'tekst' => $this->text()->notNull(),
            'dostup' => $this->boolean(),
        ]);

        $this->insert('template',array(
            'name' => 'Акт приемки на ремонт',
            'tekst' =>"
            <html>

            <head>
            <meta http-equiv=Content-Type content=\"text/html; charset=windows-1251\">
            <meta name=Generator content=\"Microsoft Word 14 (filtered)\">
            <style>
             @font-face
                {font-family:Calibri;
                panose-1:2 15 5 2 2 2 4 3 2 4;}
             p.MsoNormal, li.MsoNormal, div.MsoNormal
                {margin-top:0cm;
                margin-right:0cm;
                margin-bottom:10.0pt;
                margin-left:0cm;
                line-height:115%;
                font-size:11.0pt;
                font-family:\"Calibri\",\"sans-serif\";}
            .MsoChpDefault
                {font-family:\"Calibri\",\"sans-serif\";}
            .MsoPapDefault
                {margin-bottom:10.0pt;
                line-height:115%;}
            @page WordSection1
                {size:595.3pt 841.9pt;
                margin:35.45pt 28.3pt 2.0cm 49.65pt;}
            div.WordSection1
                {page:WordSection1;}
            </style>
            </head>
            <body lang=RU>
            <div class=WordSection1>
            <p class=MsoNormal  style='font-size:8.00pt;'>

            &quot; {company_name} &quot; <br>
            {company_adress} <br>
            тел.: {company_phone} <br>
            {mode_of_operation}  Пн-Пт  9­-18, Сб,Вс - выходной  </p>

            <br/>
            <p class=MsoNormal align=center style='text-align:center'><b><span
            style='font-size:14.0pt;line-height:115%'>{template_name} № {akt_number} от {akt_date}</span></b></p>
            <br/>
            <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
             style='margin-left:5.4pt;border-collapse:collapse;border:none'>
             <tr>
              <td width=319 valign=top style='width:239.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
              normal'>Клиент: {client_name} <br>
              Тел.: {client_phone}<br>
              Устройство: {device} <br>
              Комплектация: {configuration} <br>
              Внешний вид: {appearance}<br>
              Дефект: {defect}</p>
              </td>
              <td width=371 valign=top style='width:278.15pt;padding:0cm 5.4pt 0cm 5.4pt'>
              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
              normal'>Ориентировочная дата готовности: {approx_data} <br>
              Ориентировочная стоимость: {approx_cost} руб <br>
              Предоплата: {preorder} руб <br>
              Заметки приемщика: {notes}</p>
              </td>
             </tr>
            </table>

            <p class=MsoNormal>&nbsp;</p>

            <p class=MsoNormal style='font-size:8.00pt;'>1. Технический центр не несет ответственности за возможную
            потерю данных в памяти устройства, а так же за оставленные SIM и FLASH карты.
            Заблаговременно примите меры по резервированию информации. <br>
            2. Заказчик принимает на себя риск возможной полной или частичной утраты
            работоспособности устройства в процессе ремонта, в случае грубых нарушений
            пользователем условий эксплуатации , наличие следов попадания токопроводящей
            жидкости (коррозии), либо механических повреждений. <br>
            3. На восстановленные после попадания жидкости на устройство гарантия не
            распространяется и не продлевается. <br>
            4. Срок хранения аппарата 30 дней с ориентировочной даты готовности. После
            данного срока аппарат утилизируется и претензии по нему не принимаются. <br>
            5. В случае утери квитанции, устройство выдается по предъявлению паспорта на
            имя заказчика. </p>
            <br>
            <br>
            <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
             style='border-collapse:collapse;border:none'>
             <tr style='height:28.05pt'>
              <td width=352 valign=top style='width:264.05pt;padding:0cm 5.4pt 0cm 5.4pt;
              height:28.05pt'>
              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
              normal'>Приемщик: ________________({usercr_name})</p>
              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
              normal'>                      М.П.</p>
              </td>
              <td width=352 valign=top style='width:264.1pt;padding:0cm 5.4pt 0cm 5.4pt;
              height:28.05pt'>
              <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
              text-align:right;line-height:normal'>_____________________ ({client_name})</p>
              <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
              text-align:right;line-height:normal'>с условиями ремонта ознакомлен и
              согласен</p>
              </td>
             </tr>
            </table>

            <p class=MsoNormal>&nbsp;</p>

            <p class=MsoNormal>______________________________________________________________________________________________</p>

            <p class=MsoNormal align=center style='text-align:center'><b><span
            style='font-size:14.0pt;line-height:115%'>Информация для мастера № {akt_number} от {akt_date}</span></b></p>

             <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
              normal'>Клиент: {client_name},{client_phone}<br>
              Устройство: {device} <br>
              Комплектация: {configuration} <br>
              Внешний вид: {appearance} <br>
              Дефект: {defect}<br>
            Заметки приемщика: {notes}</p>

            </div>

            </body>

            </html>

           
         ",
        ));

$this->insert('template',array(
            'name' => 'Акт выполненных работ',
            'tekst' =>"
                <div class=Section1>
                <p class=MsoNormal>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                normal'><p class=MsoNormal  style='font-size:8.00pt;'>

                &quot; {company_name} &quot; <br>
                {company_adress} <br>
                тел.: {company_phone} <br>
                {mode_of_operation}</p>
                </p>
                <br/>
                <p class=MsoNormal align=center style='text-align:center'>
                <u><span
                style='font-size:12.0pt;font-family:\"Arial\",\"sans-serif\"'>{template_name}  <b style='mso-bidi-font-weight:
                normal'>№ </b></u><b style='mso-bidi-font-weight:normal'><u>
                {akt_number}

                </u></b><b
                style='mso-bidi-font-weight:normal'><u></u></b><i
                style='mso-bidi-font-style:normal'> от {akt_date}<o:p></o:p></span></i>

                </p>

                <br/><br/>

                              
                <br/>

                <b
                style='mso-bidi-font-weight:normal'>Клиент:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'> {client_name},  {client_phone} </span><o:p></o:p></span>

                <br/>
                
                
                <b
                style='mso-bidi-font-weight:normal'>Устройство:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{device} {device_made_name} {device_modeldevice_name} </span><o:p></o:p></span>

                <br/>

                <b
                style='mso-bidi-font-weight:normal'>Не исправность:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{nothealth_name} </span><o:p></o:p></span>

                <br/>
                <b
                style='mso-bidi-font-weight:normal'>Скидка:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{discount_order} </span><o:p></o:p></span>

                <br/>
                <b
                style='mso-bidi-font-weight:normal'>Предоплата:
                </b><span class=SpellE style='font-size:9.0pt;mso-bidi-font-size:
                11.0pt;line-height:101%;mso-ansi-language:RU'>{prepay} </span><o:p></o:p></span>

                </p>

                <br/>
                <b style='mso-bidi-font-weight:normal'>
                {warranty_period} : 30 дней
                </b>
                <p class=MsoNormal>&nbsp;</p>
                
                <br><br>
                {table}
                <br>
                <br>

                <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
                style='font-size:8.0pt;line-height:115%;font-family:\"Arial\",\"sans-serif\";
                color:black;background:white'>{client_signature}: ____________________
                (________________)<span style='mso-spacerun:yes'>     </span>{employee_signature}: __________________({order_usermas_name})</span></b><b style='mso-bidi-font-weight:
                normal'><span style='font-size:8.0pt;line-height:115%'><o:p></o:p></span></b></p>

                <p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
                \"Arial\",\"sans-serif\";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

                <p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
                \"Arial\",\"sans-serif\";color:black;background:white'><o:p>&nbsp;</o:p></span></p>

                </div>

           
         ",
        ));

        $this->insert('template',array(
            'name' => 'Смс шаблон',
            'tekst' => "Ваш заказ № {order_id} {status_name} {status_tekst}, сумма ремонта {summa} руб. Компания Цифровые технологии",
            ));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('template');
    }
}
