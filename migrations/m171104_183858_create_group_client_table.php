<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group_client`.
 */
class m171104_183858_create_group_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group_client', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('group_client');
    }
}
