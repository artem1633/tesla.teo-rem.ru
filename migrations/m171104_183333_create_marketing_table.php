<?php

use yii\db\Migration;

/**
 * Handles the creation of table `marketing`.
 */
class m171104_183333_create_marketing_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('marketing', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_cr' => $this->integer(),
            'date_up' => $this->integer()->notNull(),
        ]);

         $this->insert('marketing',array(
            'name' => 'Газета',
            'date_cr' => time(),
            'date_up' => time(),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('marketing');
    }
}
