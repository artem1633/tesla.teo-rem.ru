<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_list`.
 */
class m171104_192854_create_shop_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shop_list', [
            'id' => $this->primaryKey(),
            'parts_by' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull(),
            'shop_by' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shop_list');
    }
}
