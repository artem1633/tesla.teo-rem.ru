<?php

use yii\db\Migration;

/**
 * Class m180615_121332_change_tamplate_table
 */
class m180615_121332_change_tamplate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $template = '<div class="WordSection1">
<p>&quot; {company_name} &quot;<br />
{company_adress}<br />
тел.: {company_phone}<br />
{mode_of_operation} Пн-Пт&nbsp; 9&shy;-18, Сб,Вс - выходной</p>
&nbsp;

<p style="text-align:center"><strong><span style="font-size:14.0pt">{template_name} № {akt_number} от {akt_date}</span></strong></p>
&nbsp;

<table border="0" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none; margin-left:5.4pt">
    <tbody>
        <tr>
            <td style="width:239.25pt">
            <p>Клиент: {client_name}<br />
            Тел.: {client_phone}<br />
            Устройство: {device}<br />
            Комплектация: {configuration}<br />
            Внешний вид: {appearance}<br />
            Дефект: {defect}</p>
            </td>
            <td style="width:278.15pt">
            <p>Ориентировочная дата готовности: {approx_data}<br />
            Ориентировочная стоимость: {approx_cost} руб<br />
            Предоплата: {preorder} руб<br />
            Заметки приемщика: {notes}</p>
            </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>1. Технический центр не несет ответственности за возможную потерю данных в памяти устройства, а так же за оставленные SIM и FLASH карты. Заблаговременно примите меры по резервированию информации.<br />
2. Заказчик принимает на себя риск возможной полной или частичной утраты работоспособности устройства в процессе ремонта, в случае грубых нарушений пользователем условий эксплуатации , наличие следов попадания токопроводящей жидкости (коррозии), либо механических повреждений.<br />
3. На восстановленные после попадания жидкости на устройство гарантия не распространяется и не продлевается.<br />
4. Срок хранения аппарата 30 дней с ориентировочной даты готовности. После данного срока аппарат утилизируется и претензии по нему не принимаются.<br />
5. В случае утери квитанции, устройство выдается по предъявлению паспорта на имя заказчика.</p>
&nbsp;

<table border="0" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none">
    <tbody>
        <tr>
            <td style="height:28.05pt; width:264.05pt">
            <p>Приемщик: ________________({usercr_name})</p>

            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; М.П.</p>
            </td>
            <td style="height:28.05pt; width:264.1pt">
            <p style="text-align:right">_____________________ ({client_name})</p>

            <p style="text-align:right">с условиями ремонта ознакомлен и согласен</p>
            </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>______________________________________________________________________________________________</p>

<p style="text-align:center"><strong><span style="font-size:14.0pt">Информация для мастера № {akt_number} от {akt_date}</span></strong></p>

<p>Клиент: {client_name},{client_phone}<br />
Устройство: {device}<br />
Комплектация: {configuration}<br />
Внешний вид: {appearance}<br />
Дефект: {defect}<br />
Заметки приемщика: {notes}</p>
</div>
';
        \Yii::$app->db->createCommand()->update('template', ['tekst' => $template ], [ 'id' => 1 ])->execute();
    }

    public function down()
    {

    }
}
