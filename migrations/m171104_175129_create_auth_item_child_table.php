<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth_item_child`.
 */
class m171104_175129_create_auth_item_child_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auth_item_child', [
            'id' => $this->primaryKey(),
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
        ]);

        $this->insert('auth_item_child',array(
            'parent' => 'maneger',
            'child' => 'createOrder',
        ));

        $this->insert('auth_item_child',array(
            'parent' => 'admin',
            'child' => 'listOrder',
        ));

        $this->insert('auth_item_child',array(
            'parent' => 'maneger',
            'child' => 'listOrder',
        ));

        $this->insert('auth_item_child',array(
            'parent' => 'admin',
            'child' => 'maneger',
        ));
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auth_item_child');
    }
}
