<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m180606_041337_add_default_value_to_companies_table
 */
class m180606_041337_add_default_value_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \Yii::$app->db->createCommand()->update('user', ['company_id' => 1, 'permission' => 'super_admin'], [ 'id' => 2 ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
