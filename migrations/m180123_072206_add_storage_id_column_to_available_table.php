<?php

use yii\db\Migration;

/**
 * Handles adding storage_id to table `available`.
 */
class m180123_072206_add_storage_id_column_to_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('available', 'storage_id', $this->integer());
        $this->createIndex('idx-available-storage_id', 'available', 'storage_id', false);
        $this->addForeignKey("fk-available-storage_id", "available", "storage_id", "storage", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-available-user_id','available');
        $this->dropIndex('idx-available-user_id','available');
        $this->dropColumn('available', 'storage_id');
    }
}
