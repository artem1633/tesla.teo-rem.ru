<?php

use yii\db\Migration;

/**
 * Class m180519_125815_add_column_to_user_table
 */
class m180519_125815_add_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('user', 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));
        $this->addColumn('user', 'last_activity_datetime', $this->dateTime()->comment('Дата и время последней активности'));
        $this->addColumn('user', 'vk_href', $this->string()->comment('Ссылка на страницу ВКонтакте'));

        /*$this->createIndex('idx-user-company_id', 'user', 'company_id', false);
        $this->addForeignKey("fk-user-company_id", "user", "company_id", "storage", "id");*/
    }

    public function down()
    {
        /*$this->dropForeignKey('fk-user-company_id','user');
        $this->dropIndex('idx-user-company_id','user');*/
        
        $this->dropColumn('user', 'company_id');
        $this->dropColumn('user', 'last_activity_datetime');
        $this->dropColumn('user', 'vk_id');
    }
}
