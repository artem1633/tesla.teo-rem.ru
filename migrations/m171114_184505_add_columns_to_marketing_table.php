<?php

use yii\db\Migration;

class m171114_184505_add_columns_to_marketing_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('marketing','show_in_indicators', $this->boolean()->comment('Отображать в показателях')->defaultValue(false));
        $this->addColumn('marketing','plan_month', $this->integer()->comment('План в месяц'));
    }

    public function safeDown()
    {
        $this->dropColumn('marketing','show_in_indicators');
        $this->dropColumn('marketing','plan_month');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171114_184505_add_columns_to_marketing_table cannot be reverted.\n";

        return false;
    }
    */
}
