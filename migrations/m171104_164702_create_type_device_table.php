<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_device`.
 */
class m171104_164702_create_type_device_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('type_device', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Наименование'),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('type_device');
    }
}
