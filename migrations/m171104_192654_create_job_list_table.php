<?php

use yii\db\Migration;

/**
 * Handles the creation of table `job_list`.
 */
class m171104_192654_create_job_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job_list', [
            'id' => $this->primaryKey(),
            'order_by' => $this->integer()->notNull(),
            'job_by' => $this->integer()->notNull(),
            'type' => $this->string(11)->notNull(),
            'count' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('job_list');
    }
}
