<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_order`.
 */
class m171104_185415_create_type_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('type_order', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('type_order');
    }
}
