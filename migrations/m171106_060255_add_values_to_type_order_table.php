<?php

use yii\db\Migration;

class m171106_060255_add_values_to_type_order_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_060255_add_values_to_type_order_table cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('type_order', ['id', 'name', 'date_cr', 'date_up'], [
            [2, 'По гарантии', time(), time()],
            [6, 'Платный ремонт', time(), time()],
        ])->execute();
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_060255_add_values_to_type_order_table cannot be reverted.\n";

        return false;
    }
    */
}
