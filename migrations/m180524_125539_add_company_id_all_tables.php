<?php

use yii\db\Migration;

/**
 * Class m180524_125539_add_company_id_all_tables
 */
class m180524_125539_add_company_id_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addCompanyIdField('client');//
        $this->addCompanyIdField('parts');//
        $this->addCompanyIdField('storage');//
        $this->addCompanyIdField('available');//
        $this->addCompanyIdField('sending_sms');//
        $this->addCompanyIdField('about_company');//
        $this->addCompanyIdField('shop');//
        $this->addCompanyIdField('cash');//
        $this->addCompanyIdField('order');//
        $this->addCompanyIdField('resource');//
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCompanyIdField('client');//
        $this->dropCompanyIdField('parts');//
        $this->dropCompanyIdField('storage');//
        $this->dropCompanyIdField('available');//
        $this->dropCompanyIdField('sending_sms');//
        $this->dropCompanyIdField('about_company');//
        $this->dropCompanyIdField('shop');//
        $this->dropCompanyIdField('cash');//
        $this->dropCompanyIdField('order');//
        $this->dropCompanyIdField('resource');//
    }


    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "CASCADE"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
