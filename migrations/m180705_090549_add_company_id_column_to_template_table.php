<?php

use yii\db\Migration;
use app\models\Template;

/**
 * Handles adding company_id to table `template`.
 */
class m180705_090549_add_company_id_column_to_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('template', 'company_id', $this->integer());
        $this->addColumn('template', 'key', $this->string(255));

        $this->createIndex('idx-template-company_id', 'template', 'company_id', false);
        $this->addForeignKey("fk-template-company_id", "template", "company_id", "companies", "id");

        $template = Template::findOne(1);
        if($template != null){
            $template->key = 'acceptance';
            $template->company_id = 1;
            $template->save();
        }

        $template = Template::findOne(2);
        if($template != null){
            $template->key = 'done';
            $template->company_id = 1;
            $template->save();
        }

        $template = Template::findOne(3);
        if($template != null){
            $template->key = 'sms';
            $template->company_id = 1;
            $template->save();
        }

        $template = Template::findOne(4);
        if($template != null){
            $template->key = 'order_form';
            $template->company_id = 1;
            $template->save();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-template-company_id','template');
        $this->dropIndex('idx-template-company_id','template');

        $this->dropColumn('template', 'company_id');
        $this->dropColumn('template', 'key');
    }
}
