<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash`.
 */
class m171104_175800_create_cash_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cash', [
            'id' => $this->primaryKey(),
            'type_cash_by' => $this->integer()->notNull()->comment('Статья'),
            'summa' => $this->integer()->notNull()->comment('Сумма'),
            'comment' => $this->string(255)->notNull(),
            'user_by_cr' => $this->integer()->notNull(),
            'user_by_up' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
            'type' => $this->string(20)->notNull()->comment('Тип'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cash');
    }
}
