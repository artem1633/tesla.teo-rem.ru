<?php

use yii\db\Migration;

class m171106_055253_add_values_to_group_client_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_055253_add_values_to_group_client_table cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('group_client', ['id', 'name', 'date_cr', 'date_up'], [
            [2, 'Черный список', time(), time()],
            [4, 'Физ. лица', time(), time()],
            [5, 'Юр. лица', time(), time()],
            [6, 'Поставщики', time(), time()],
            [7, 'Созданы автоматически', time(), time()],
        ])->execute();

    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_055253_add_values_to_group_client_table cannot be reverted.\n";

        return false;
    }
    */
}
