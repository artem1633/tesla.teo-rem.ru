<?php

use yii\db\Migration;

/**
 * Handles the creation of table `parts`.
 */
class m171104_190057_create_parts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('parts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'price' => $this->integer()->notNull(),
            'price_shop' => $this->integer()->notNull(),
            'supplier' => $this->integer()->notNull(),
            'phone' => $this->string(20)->notNull(),
            'group_by' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('parts');
    }
}
