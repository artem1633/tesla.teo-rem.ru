<?php

use yii\db\Migration;

/**
 * Handles the creation of table `made`.
 */
class m171104_185642_create_made_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('made', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('made');
    }
}
