<?php

use yii\db\Migration;

class m171105_060329_change_values_about_company extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171105_060329_change_values_about_company cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->update('about_company', array(
            'name' => 'Компания "TEO-CRM" (ИНН 123123123123)',
            'adress' => 'г. Новосибирск, ул. Красный пр. , д. 76',
            'bank' => 'Сбербанк',
            'director' => 'Ковальский Артем Александрович',
            'email' => 'artem.kovalskiy.93@mail.ru',
            'phone' => '(961) 220-66-59',
            'user_by_cr' => 1,
            'user_by_up' => 1,
            'date_cr' => time(),
            'date_up' => time(),
            'smslog' => 'teo',
            'smspas' => 'vRe4Og0t08',
            'site_name' => 'shop-crm.ru',
          ), 
              'id=1'
        );
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171105_060329_change_values_about_company cannot be reverted.\n";

        return false;
    }
    */
}
