<?php

use yii\db\Migration;
use app\models\User;

/**
 * Handles the creation of table `companies`.
 */
class m180519_130019_create_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(255)->comment('Название компании'),
            'created_date' => $this->date()->comment('Дата регистрации'),
            'admin_id' => $this->integer()->comment('Id администратора компании'),
            'rate_id' => $this->integer()->comment('Тариф'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'access_end_datetime' => $this->datetime()->comment('Дата и время потери доступа к системе'),
            'is_super' => $this->boolean()->defaultValue(0)->comment('Супер компания'),
        ]);

        $this->addCommentOnTable('companies', 'Компании');

        $this->createIndex(
            'idx-companies-admin_id',
            'companies',
            'admin_id'
        );

        $this->addForeignKey(
            'fk-companies-admin_id',
            'companies',
            'admin_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-companies-rate_id',
            'companies',
            'rate_id'
        );

        $this->addForeignKey(
            'fk-companies-rate_id',
            'companies',
            'rate_id',
            'rates',
            'id',
            'SET NULL'
        );

        $this->insert('companies', [
            'admin_id' => 2,
            'rate_id' => null,
            'is_super' => 1,
        ]);

        /*$admin = User::findOne(1);
        $company = \app\models\Companies::findOne(['is_super' => 1]);
        $admin->company_id = $company->id;
        $admin->role = 0;
        $admin->save();*/
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-companies-admin_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-admin_id',
            'companies'
        );

        $this->dropForeignKey(
            'fk-companies-rate_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-rate_id',
            'companies'
        );

        $this->dropTable('companies');
    }
}
