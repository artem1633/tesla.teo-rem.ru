<?php

use yii\db\Migration;

/**
 * Handles adding part_id to table `move`.
 */
class m180125_084311_add_part_id_column_to_move_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('move', 'part_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('move', 'part_id');
    }
}
