<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_order`.
 */
class m171104_170613_create_status_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status_order', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'push' => $this->boolean()->notNull()->comment('Уведомления'),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
            'tekst' => $this->text()->comment('Шаблон'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status_order');
    }
}
