<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sending_sms`.
 */
class m171104_185110_create_sending_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sending_sms', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'tekst' => $this->text(),
            'count' => $this->integer(),
            'created_date' => $this->date(),
            'update_date' => $this->date(),
            'status' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sending_sms');
    }
}
