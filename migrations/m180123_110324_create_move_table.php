<?php

use yii\db\Migration;

/**
 * Handles the creation of table `move`.
 */
class m180123_110324_create_move_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('move', [
            'id' => $this->primaryKey(),
            'table' => $this->string(255),
            'storage_form' => $this->integer(),
            'storage_to' => $this->integer(),
            'old_count' => $this->integer(),
            'sending_count' => $this->integer(),
            'data' => $this->datetime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('move');
    }
}
