<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m171117_130527_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'return_waid' => $this->integer()->comment('Планируемая дата сдачи'),
            'return_date' => $this->integer()->comment('Фактическая дата сдачи'),
            'type_order_by' => $this->integer()->notNull()->comment('Тип'),
            'client_by' => $this->integer()->notNull(),
            'phone' => $this->string(20)->notNull(),
            'type_device_by' => $this->integer()->notNull()->comment('Устройство'),
            'made_by' => $this->integer()->comment('Производитель'),
            'model_device_by' => $this->integer()->comment('Модель'),
            'nothealth_by' => $this->integer()->notNull()->comment('Не исправность'),
            'view' => $this->string(255)->comment('Внешний вид'),
            'configuration' => $this->string(255)->comment('Комплектация'),
            'comment' => $this->string(255),
            'price' => $this->integer()->comment('Цена'),
            'prepay' => $this->integer()->comment('Предоплата'),
            'marketing_by' => $this->integer()->comment('Реклама'),
            'discount_order' => $this->integer()->comment('Скидка'),
            'status_order' => $this->integer()->notNull()->comment('Статус'),
            'quick' => $this->boolean()->notNull()->comment('Срочный заказ'),
            'user_by_cr' => $this->integer()->notNull()->comment('Принял'),
            'user_by_up' => $this->integer()->notNull()->comment('Изменял'),
            'date_cr' => $this->integer()->notNull()->comment('Создано'),
            'date_up' => $this->integer()->notNull()->comment('Изменено'),
            'user_by' => $this->integer()->comment('Мастер'),
            'client_address' => $this->string(255)->comment('Адрес клиента'),
        ]);

        $this->createIndex(
            'ix_order_status_order',
            'order',
            'status_order'
        );

        $this->addForeignKey(
            'fk_order_status_order',
            'order',
            'status_order',
            'status_order',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_order_type_order_by',
            'order',
            'type_order_by'
        );
        $this->addForeignKey(
            'fk_order_type_order_by',
            'order',
            'type_order_by',
            'type_order',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_order_client_by',
            'order',
            'client_by'
        );
        $this->addForeignKey(
            'fk_order_client_by',
            'order',
            'client_by',
            'client',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_type_device',
            'order',
            'type_device_by'
        );
        $this->addForeignKey(
            'fk_type_device',
            'order',
            'type_device_by',
            'type_device',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_made_by',
            'order',
            'made_by'
        );
        $this->addForeignKey(
            'fk_made_by',
            'order',
            'made_by',
            'made',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_model_device_by',
            'order',
            'model_device_by'
        );
        $this->addForeignKey(
            'fk_model_device_by',
            'order',
            'model_device_by',
            'model_device',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_nothealth_by',
            'order',
            'nothealth_by'
        );
        $this->addForeignKey(
            'fk_nothealth_by',
            'order',
            'nothealth_by',
            'nothealth',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_marketing_by',
            'order',
            'marketing_by'
        );
        $this->addForeignKey(
            'fk_marketing_by',
            'order',
            'marketing_by',
            'marketing',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_user_by_cr',
            'order',
            'user_by_cr'
        );
        $this->addForeignKey(
            'fk_user_by_cr',
            'order',
            'user_by_cr',
            'user',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'ix_user_by_up',
            'order',
            'user_by_up'
        );
        $this->addForeignKey(
            'fk_user_by_up',
            'order',
            'user_by_up',
            'user',
            'id',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');

//        $this->dropForeignKey('fk_order_status_order','order');
//        $this->dropIndex('ix_order_status_order','order');

//        $this->dropForeignKey('fk_order_type_order_by','order');
//        $this->dropIndex('ix_order_type_order_by','order');
//
//        $this->dropForeignKey('fk_order_client_by','order');
//        $this->dropIndex('ix_order_client_by','order');
//
//        $this->dropForeignKey('fk_type_device','order');
//        $this->dropIndex('ix_type_device','order');
//
//        $this->dropForeignKey('fk_made_by','order');
//        $this->dropIndex('ix_made_by','order');
//
//        $this->dropForeignKey('fk_model_device_by','order');
//        $this->dropIndex('ix_model_device_by','order');
//
//        $this->dropForeignKey('fk_nothealth_by','order');
//        $this->dropIndex('ix_nothealth_by','order');
//
//        $this->dropForeignKey('fk_marketing_by','order');
//        $this->dropIndex('ix_marketing_by','order');
//
//        $this->dropForeignKey('fk_user_by_cr','order');
//        $this->dropIndex('ix_user_by_cr','order');
//
//        $this->dropForeignKey('fk_user_by_up','order');
//        $this->dropIndex('ix_user_by_up','order');

    }
}
