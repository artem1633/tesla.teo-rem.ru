<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171104_163226_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull()->comment('Логин'),
            'name' => $this->text()->notNull()->comment('ФИО'),
            'phone' => $this->string(32)->notNull(),
            'auth_key' => $this->string(32)->notNull()->comment('Пароль'),
            'password_hash' => $this->string(255)->notNull()->comment('Hash'),
            'password_reset_token' => $this->string(255)->comment('Reset Token'),
            'email' => $this->string(255)->notNull(),
            'permission' => $this->string(32)->notNull()->comment('Должность'),
            'percent_sale' => $this->integer()->notNull()->comment('За продаж'),
            'percent_service' => $this->integer()->notNull()->comment('За обслуживание'),
            'percent_finish' => $this->integer()->notNull()->comment('За вполнение'),
            'status' => $this->string(10)->notNull()->comment('Статус'),
            'user_by_cr' => $this->integer()->notNull()->comment('Создатель'),
            'user_by_up' => $this->integer()->notNull()->comment('Изменял'),
            'date_cr' => $this->integer()->notNull()->comment('Создано'),
            'date_up' => $this->integer()->notNull()->comment('Изменено'),
        ]);

        $this->insert('user',array(
            'username' => 'admin',
            'name' => 'Ковальский',
            'phone' => '89612206659',
            'auth_key' => 'admin',
            'password_hash' => md5('admin'),
            'password_reset_token' => '',
            'email' => 'admin@gmail.com',
            'permission' => 'admin',
            'percent_sale' => '50',
            'percent_service' => '50',
            'percent_finish' => '50',
            'status' => '1',
            'user_by_cr' => '1',
            'user_by_up' => '1',
            'date_cr' => time(),
            'date_up' => time(),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
