<?php

use yii\db\Migration;

/**
 * Handles adding company to table `user`.
 */
class m171115_152553_add_company_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->delete('user');
        $this->addColumn('user','company', $this->integer()->comment('Компания')->defaultValue(false));

        $this->createIndex(
            'ix_user_company',
            'user',
            'company'
        );

        $this->addForeignKey(
            'fk_user_company',
            'user',
            'company',
            'about_company',
            'id',
            'RESTRICT'
        );


        $this->insert('user',array(
            'username' => 'admin',
            'name' => 'Ковальский',
            'phone' => '89612206659',
            'auth_key' => 'admin',
            'password_hash' => md5('admin'),
            'password_reset_token' => '',
            'email' => 'admin@gmail.com',
            'permission' => 'admin',
            'percent_sale' => '50',
            'percent_service' => '50',
            'percent_finish' => '50',
            'status' => '1',
            'user_by_cr' => '1',
            'user_by_up' => '1',
            'date_cr' => time(),
            'date_up' => time(),
            'company' => 1
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_user_company','user');
        $this->dropIndex('ix_user_company','user');
        $this->dropColumn('user','company');
    }
}
