<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop`.
 */
class m171104_173041_create_shop_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shop', [
            'id' => $this->primaryKey(),
            'story_by' => $this->integer()->comment('Товар'),
            'price' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull(),
            'client_by' => $this->integer()->notNull(),
            'komment' => $this->string(255)->notNull(),
            'status' => $this->string(11)->notNull(),
            'user_by_cr' => $this->integer()->notNull(),
            'user_by_up' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shop');
    }
}
