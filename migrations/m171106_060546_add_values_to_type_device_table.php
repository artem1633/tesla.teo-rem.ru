<?php

use yii\db\Migration;

class m171106_060546_add_values_to_type_device_table extends Migration
{
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171106_060546_add_values_to_type_device_table cannot be reverted.\n";

        return false;
    }
    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('type_device', ['id', 'name', 'date_cr', 'date_up'], [
            [3, 'Телефон', time(), time()],
            [4, 'Планшет', time(), time()],
            [5, 'Ноутбук', time(), time()],
            [6, 'Системный блок', time(), time()],
            [7, 'Монитор', time(), time()],
            [8, 'Винчестер', time(), time()],
            [9, 'Телевизор', time(), time()],
            [10, 'Видеорегистратор', time(), time()],
            [11, 'Навигатор', time(), time()],
            [12, 'Фотоаппарат', time(), time()],
            [13, 'DVD плеер', time(), time()],
            [14, 'MP3 плеер', time(), time()],
            [15, 'Видеокарта', time(), time()],
            [16, 'Графический планшет', time(), time()],
            [17, 'ТВ приставка', time(), time()],
            [18, 'Электронная книга', time(), time()],
            [19, 'Наушники', time(), time()],
            [20, 'Роутер', time(), time()],
            [21, 'Модем', time(), time()],
            [22, 'Ресивер', time(), time()],
            [23, 'Стиральная машина', time(), time()],
        ])->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171106_060546_add_values_to_type_device_table cannot be reverted.\n";

        return false;
    }
    */
}
