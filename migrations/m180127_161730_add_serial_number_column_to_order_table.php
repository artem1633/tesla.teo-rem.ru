<?php

use yii\db\Migration;

/**
 * Handles adding serial_number to table `order`.
 */
class m180127_161730_add_serial_number_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'serial_number', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'serial_number');
    }
}
