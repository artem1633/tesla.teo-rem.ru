<?php

use yii\db\Migration;

class m171105_175503_add_nothealth_values extends Migration
{
    public function safeUp()
    {

        //$this->execute(file_get_contents('/'.__DIR__ .'queries/nothealth.sql'));
    }

    public function safeDown()
    {
        echo "m171105_175503_add_nothealth_values cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        Yii::$app->db->createCommand()->batchInsert('nothealth', ['id', 'name', 'date_cr', 'date_up'], [
            [ 18, 'Не включается', time(), time()],
            [ 19, 'Не запускается ОС', time(), time()],
            [ 20, 'Не работает экран', time(), time()],
            [ 21, 'Нет звука', time(), time()],
            [ 22, 'Не работает тачскрин', time(), time()],
            [ 23, 'Греется', time(), time()],
            [ 27, 'Виснет на заставке', time(), time()],
            [ 28, 'Виснет при работе', time(), time()],
            [ 29, 'Выключается', time(), time()],
            [ 30, 'Заблокирован', time(), time()],
            [ 31, 'Не заряжается', time(), time()],
            [ 32, 'Не работает кнопка включения \\ выключения', time(), time()],
            [ 33, 'Не работает микрофон', time(), time()],
            [ 34, 'Не работают кнопки', time(), time()],
            [ 35, 'Нет звука мелодии', time(), time()],
            [ 36, 'Нет звука разговора', time(), time()],
            [ 37, 'Нет изображения', time(), time()],
            [ 38, 'Нет подсветки', time(), time()],
            [ 39, 'Нет сети', time(), time()],
            [ 40, 'Перезагружается', time(), time()],
            [ 41, 'Пропадает изображение', time(), time()],
            [ 42, 'Разбито сенсорное стекло', time(), time()],
            [ 43, 'Разбит экран', time(), time()],
            [ 46, 'зависает', time(), time()],
            [ 47, 'Новая не исправность', time(), time()],
            [ 48, 'включается только в безопастном режиме', time(), time()],
            [ 49, 'медленно работает', time(), time()],
            [ 50, 'не работают порты', time(), time()],
            [ 51, 'сломан шарнир', time(), time()],
            [ 52, 'при включении просит установить дату  и время', time(), time()],
            [ 53, 'не работает клавиатура', time(), time()],
            [ 54, 'замена АКБ', time(), time()],
            [ 55, 'разбит дисплей', time(), time()],
            [ 56, 'не сливает', time(), time()],
        ])->execute();
    }
    /*
    // Use up()/down() to run migration code without a transaction.

    public function down()
    {
        echo "m171105_175503_add_nothealth_values cannot be reverted.\n";

        return false;
    }
    */
}
