<?php

use yii\db\Migration;

/**
 * Handles adding move to table `user`.
 */
class m180123_055155_add_move_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'move', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'move');
    }
}
