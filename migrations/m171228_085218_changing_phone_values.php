<?php

use yii\db\Migration;

/**
 * Class m171228_085218_changing_phone_values
 */
class m171228_085218_changing_phone_values extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $orders = \app\models\Order::find()->all();
        foreach ($orders as $value) {
            $tel = $value->phone; $f="";
            $strlen = strlen( $tel );
            $numeric = 0; 
            for( $i = 0; $i <= $strlen; $i++ ) 
            {
                $char = substr( $tel, $i, 1 );             
                if(ord($char) > 47 && ord($char) < 58) $f .= $char;
            }
            Yii::$app->db->createCommand()->update('order', ['phone' => $f], [ 'id' => $value->id ])->execute();
        }
    }

    public function down()
    {
        
    }
    
}
