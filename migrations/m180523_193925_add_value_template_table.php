<?php

use yii\db\Migration;

/**
 * Class m180523_193925_add_value_template_table
 */
class m180523_193925_add_value_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('template',array(
          'name' => 'Форма заказа',
          'tekst' =>"<div class=Section1>
 <br><br><br><br><br>
<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 style='margin-left:.25pt;border-collapse:collapse;border:none'> 
 
<tr style='height:19.85pt'>
    <td width=200 height=70 colspan='6'>
        <p class=MsoNormal style='font-size:26.0pt'><center><b>Форма заказа</b></center></p>
    </td>
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Выдать</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{akt_date}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Тип заказа</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{tipzakaza}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Мастер</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{order_usermas_name}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Клиент</b></p>
        </td>
        <td width=300 height=70 colspan='3'>
            <p class=MsoNormal style='font-size:16.0pt'><center>{client_name}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Телефон клиента</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{client_phone}</center></p>
        </td>
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Не исправность</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{nothealth_name}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Устройство</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{device}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Производитель</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{device_made_name}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Модель</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{model}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Внешний вид</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{appearance}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Комплектация</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{configuration}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Цена</b></p>
        </td>
        <td width=300 height=70>
            <p class=MsoNormal style='font-size:16.0t'><center>{price}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Предоплата</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{prepay}</center></p>
        </td>

        <td width=150 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Скидка</b></p>
        </td>
        <td width=400 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{discount_order}</center></p>
        </td>  
</tr>
<tr style='height:19.85pt'>
        <td width=200 height=70 >
            <p class=MsoNormal style='font-size:16.0pt'><b>Коментарий</b></p>
        </td>
        <td width=300 height=70 colspan='3'>
            <p class=MsoNormal style='font-size:16.0pt'><center>{comment}</center></p>
        </td>
        <td width=200 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><b>Реклама</b></p>
        </td>
        <td width=350 height=70>
            <p class=MsoNormal style='font-size:16.0pt'><center>{reklama}</center></p>
        </td>
</tr>
 
</table>
</div>

"
));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180523_193925_add_value_template_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_193925_add_value_template_table cannot be reverted.\n";

        return false;
    }
    */
}
