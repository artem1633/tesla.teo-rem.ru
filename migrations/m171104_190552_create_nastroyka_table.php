<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nastroyka`.
 */
class m171104_190552_create_nastroyka_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nastroyka', [
            'id' => $this->primaryKey(),
            'password' => $this->string(255),
            'sms_parol' => $this->string(255),
        ]);

         $this->insert('nastroyka',array(
            'password' => 'teo-crm',
            'sms_parol' => 'teo-crm',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nastroyka');
    }
}
