<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth_assignment`.
 */
class m171104_173443_create_auth_assignment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auth_assignment', [
            'id' => $this->primaryKey(),
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->insert('auth_assignment',array(
            'item_name' => 'admin',
            'user_id' => 1,
            'created_at' => time(),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auth_assignment');
    }
}
