<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store`.
 */
class m171104_184228_create_store_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'adress' => $this->string(255)->notNull(),
            'status' => $this->boolean()->notNull(),
            'user_by_cr' => $this->integer()->notNull(),
            'user_by_up' => $this->integer()->notNull(),
            'date_cr' => $this->integer()->notNull(),
            'date_up' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('store');
    }
}
