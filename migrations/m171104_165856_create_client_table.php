<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client`.
 */
class m171104_165856_create_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('ФИО'),
            'type' => $this->string(100)->notNull()->comment('Тип'),
            'ogrn' => $this->string(30)->notNull()->comment('ОГРН'),
            'expense' => $this->string(50)->notNull()->comment('р/с'),
            'group_by' => $this->integer()->notNull()->comment('Группа'),
            'adress' => $this->string(50)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'email' => $this->string(50)->notNull(),
            'discount_order' => $this->integer()->notNull()->comment('На заказ'),
            'discount_parts' => $this->integer()->notNull()->comment('На работу'),
            'user_by_cr' => $this->integer()->notNull()->comment('Создатель'),
            'user_by_up' => $this->integer()->notNull()->comment('Изменял'),
            'date_cr' => $this->integer()->notNull()->comment('Создано'),
            'date_up' => $this->integer()->notNull()->comment('Изменено'),
            'iin' => $this->string(30)->comment('ИИН'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('client');
    }
}
