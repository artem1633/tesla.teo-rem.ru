<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth_item`.
 */
class m171104_174332_create_auth_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auth_item', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->insert('auth_item',array(
            'name' => 'admin',
            'type' => 1,
            'description' => null,
            'rule_name' => null,
            'data' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert('auth_item',array(
            'name' => 'createOrder',
            'type' => 2,
            'description' => 'Создавать заказ',
            'rule_name' => null,
            'data' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert('auth_item',array(
            'name' => 'guest',
            'type' => 1,
            'description' => null,
            'rule_name' => null,
            'data' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert('auth_item',array(
            'name' => 'listOrder',
            'type' => 2,
            'description' => 'Просмотр списка заказов',
            'rule_name' => null,
            'data' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert('auth_item',array(
            'name' => 'maneger',
            'type' => 1,
            'description' => null,
            'rule_name' => null,
            'data' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert('auth_item',array(
            'name' => 'master',
            'type' => 1,
            'description' => null,
            'rule_name' => null,
            'data' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auth_item');
    }
}
