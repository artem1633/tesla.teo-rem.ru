<?php

namespace app\event_handlers;

use app\models\LoginForm;
use app\models\Logs;
use yii\base\Behavior;

/**
 * Class LoginFormEventHandler
 * @package app\event_handlers
 * @see \app\models\LoginForm
 *
 * Обработчик событий класса LoginForm
 */
class LoginFormEventHandler extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            LoginForm::EVENT_AUTHORIZED => 'authorized',
        ];
    }

    /**
     * @param $event
     */
    public function authorized($event)
    {
        $user = $event->sender->_user;

        $log = new Logs([
            'user_id' => $user->id,
            'event_datetime' => date('Y-m-d H:i:s'),
            'event' => Logs::EVENT_USER_AUTHORIZED,
        ]);

        $log->description = $log->generateEventDescription();
        $log->save();
    }
}